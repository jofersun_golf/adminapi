﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.IO;

namespace CashMarket_Entities.Utilities
{
    public static class HashMethod
    {
        public static string GenerateMd5Hash(string input)
        {
            using (MD5 md5Hash = MD5.Create())
            {
                // Convert the input string to a byte array and compute the hash.
                byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

                // Create a new Stringbuilder to collect the bytes
                // and create a string.
                StringBuilder sBuilder = new StringBuilder();

                // Loop through each byte of the hashed data 
                // and format each one as a hexadecimal string.
                for (int i = 0; i < data.Length; i++)
                {
                    sBuilder.Append(data[i].ToString("x2"));
                }

                // Return the hexadecimal string.
                return sBuilder.ToString();
            }
        }

        public static bool VerifyMd5Hash(string input, string hash)
        {
            // Hash the input.
            string hashOfInput = GenerateMd5Hash(input);

            // Create a StringComparer an compare the hashes.
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(hashOfInput, hash))
            {
                return true;
            }
            return false;
        }

        public static string HmacSha256Digest(string message, string secret)
        {
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] keyBytes = encoding.GetBytes(secret);
            byte[] messageBytes = encoding.GetBytes(message);
           HMACSHA256 cryptographer = new HMACSHA256(keyBytes);

            byte[] bytes = cryptographer.ComputeHash(messageBytes);

            return BitConverter.ToString(bytes).Replace("-", "").ToLower();
        }

        public static string GetUnixTimestampString() 
        { 
            return ((long)DateTime.UtcNow.Subtract(UnixEpoch).TotalSeconds).ToString(); 
        }

        private static readonly DateTime UnixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public static byte[] Encrypt(string key, string base64Iv, string value) 
        { 
            return Encrypt(key, Convert.FromBase64String(base64Iv), value); 
        } 
        
        public static byte[] Encrypt(string key, byte[] iv, string value) 
        { 
            byte[] useKey = Convert.FromBase64String(key);
            return Encrypt(useKey, iv, value); 
        } 
        
        public static byte[] Encrypt(byte[] key, byte[] iv, string value) 
        { 
            var bytes = Encoding.UTF8.GetBytes(value);
            return Encrypt(key, iv, bytes); 
        } 
        
        public static byte[] Encrypt(byte[] key, byte[] iv, byte[] bytes) 
        { 
            using (var aes = new AesManaged()) 
            { 
                aes.Key = key; aes.IV = iv; 
                using (var enc = aes.CreateEncryptor()) 
                { 
                    using (var strm = new MemoryStream()) 
                    { 
                        using (var cs = new CryptoStream(strm, enc, CryptoStreamMode.Write)) 
                        { 
                            cs.Write(bytes, 0, bytes.Length); 
                        } 
                        return strm.ToArray(); 
                    } 
                } 
            } 
        }

        public static string GetEncryptedValue(byte[] data)
        {
            return Convert.ToBase64String(data);
        }
    }
}
