﻿namespace CashMarket_Entities.Enums
{
    public enum TransactionType
    {
        Debit = 1,
        Credit = 2
    }
}