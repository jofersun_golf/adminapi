﻿using System.ComponentModel;

namespace CashMarket_Entities.Enums
{
    public enum Approval
    {
        [Description("approval status not available")]
        Na = 0,
        [Description("approval status pending")]
        Pending = 1,
        [Description("approval status approve")]
        Approve = 2,
        [Description("approval status reject")]
        Reject = 3
    }
}