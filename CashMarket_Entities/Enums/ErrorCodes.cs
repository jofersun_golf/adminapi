﻿using System.ComponentModel;

namespace CashMarket_Entities.Enums
{
    public enum ErrorCodes
    {
        /// <summary>
        /// Completed successfully
        /// </summary>
        [Description("Completed successfully")]
        Success = 0,

        /// <summary>
        /// General error
        /// </summary>
        [Description("General error")]
        GeneralError = 1,

        /// <summary>
        /// Invalid parameter
        /// </summary>
        [Description("Invalid parameter")]
        InvalidParameter = 2,

        /// <summary>
        /// Invalid data
        /// </summary>
        [Description("Invalid data")]
        InvalidData = 3,

        /// <summary>
        /// Unable to save data
        /// </summary>
        [Description("Unable to save data")]
        UnableSave = 4,

        /// <summary>
        /// Already processed
        /// </summary>
        [Description("Already processed")]
        AlreadyProcessed = 5,

        /// <summary>
        /// Invalid user name or pass word
        /// </summary>
        [Description("Invalid user name or pass word")]
        InvalidUserNameOrPassWord = 10,

        /// <summary>
        /// Invalid session
        /// </summary>
        [Description("Invalid session")]
        InvalidSession = 11,

        /// <summary>
        /// Invalid white label code or user name
        /// </summary>
        [Description("Invalid white label code or user name")]
        InvalidWlCodeOrUserName = 12,

        /// <summary>
        /// Not found
        /// </summary>
        [Description("Not found")]
        NotFound = 20,

        /// <summary>
        /// Already exist
        /// </summary>
        [Description("Already exist")]
        AlreadyExist = 21,

        /// <summary>
        /// Insufficient fund
        /// </summary>
        [Description("Insufficient fund")]
        InsufficientFund = 31,

        /// <summary>
        /// Internal server error
        /// </summary>
        [Description("Internal server error")]
        InternalError = 9999
    }
}