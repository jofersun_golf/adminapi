﻿using System.ComponentModel;

namespace CashMarket_Entities.Enums
{
    public enum BetStatus
    {
        /// <summary>
        /// Not available bet status
        /// </summary>
        [Description("Not available bet status")]
        Na = 0,

        /// <summary>
        /// In progress or pending bet status
        /// </summary>
        [Description("In progress or pending bet status")]
        InProgress = 1,

        /// <summary>
        /// Bet status winning
        /// </summary>
        [Description("Bet status winning")]
        Win = 2,

        /// <summary>
        /// Bet status lose
        /// </summary>
        [Description("Bet status lose")]
        Lose = 3,

        /// <summary>
        /// Bet status draw
        /// </summary>
        [Description("Bet status draw")]
        Draw = 4,

        /// <summary>
        /// Bet status cancel
        /// </summary>
        [Description("Bet status cancel")]
        Cancelled = 5,

        /// <summary>
        /// Bet status refund
        /// </summary>
        [Description("Bet status refund")]
        Refunded = 6,

        /// <summary>
        /// Bet status void
        /// </summary>
        [Description("Bet status void")]
        Voided = 7,

        /// <summary>
        /// Bet status half win
        /// </summary>
        [Description("Bet status half win")]
        HalfWin = 8,

        /// <summary>
        /// Bet status half lose
        /// </summary>
        [Description("Bet status half lose")]
        HalfLose = 0,
    }
}