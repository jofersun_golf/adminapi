﻿namespace CashMarket_Entities.Enums
{
    public enum CashMarketDb
    {
        Admin = 0,
        WhiteLabels = 1,
        Master = 2,
        Members = 3,
        Provider = 4,
        Report = 5
    }
}