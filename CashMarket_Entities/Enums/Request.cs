﻿using System.ComponentModel;

namespace CashMarket_Entities.Enums
{
    public enum Request
    {
        /// <summary>
        /// Request type not available.
        /// </summary>
        [Description("request type not available")]
        Na = 0,

        /// <summary>
        /// Request type deposit.
        /// </summary>
        [Description("request type deposit")]
        Deposit = 1,

        /// <summary>
        /// Request type withdraw.
        /// </summary>
        [Description("request type withdraw")]
        Withdraw = 2,

        /// <summary>
        /// Request type addition adjustment.
        /// </summary>
        [Description("request type addition adjustment")]
        Addition = 3,

        /// <summary>
        /// Request type subtract adjustment.
        /// </summary>
        [Description("request type subtract adjustment")]
        Subs = 4
    }
}