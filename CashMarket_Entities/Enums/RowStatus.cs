﻿namespace CashMarket_Entities.Enums
{
    public enum RowStatus
    {
        InActive = 0,
        Active = 1
    }
}