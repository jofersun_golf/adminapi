﻿namespace CashMarket_Entities.Enums
{
    public enum PersonStatus
    {
        InReview, Deactivated, Activated, Suspended, Blacklist, Blocked
    }
}