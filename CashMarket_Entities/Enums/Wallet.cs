﻿using System.ComponentModel;

namespace CashMarket_Entities.Enums
{
    public enum Wallet
    {
        [Description("Wallet not available")]
        Na = 0,
        [Description("Main wallet")]
        Main = 1,
        [Description("Hold wallet")]
        Hold = 2
    }
}