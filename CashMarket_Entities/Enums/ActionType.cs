﻿using System.ComponentModel;

namespace CashMarket_Entities.Enums
{
    public enum ActionType
    {
        /// <summary>
        /// Not available action type
        /// </summary>
        [Description("Not available action type")]
        Na = 0,

        /// <summary>
        /// Bet action type
        /// </summary>
        [Description("Bet action type")]
        Bet = 1,

        /// <summary>
        /// Settlement action type
        /// </summary>
        [Description("Settlement action type")]
        Set = 2,

        /// <summary>
        /// Canceled action type
        /// </summary>
        [Description("Canceled action type")]
        Can = 3,

        /// <summary>
        /// Rollback action type
        /// </summary>
        [Description("Rollback action type")]
        Rol = 4,

        /// <summary>
        /// Void action type
        /// </summary>
        [Description("Void action type")]
        Voi = 5,

        /// <summary>
        /// Bonus action type
        /// </summary>
        [Description("Bonus action type")]
        Bon = 6,

        /// <summary>
        /// Tip action type
        /// </summary>
        [Description("Tip action type")]
        Tip = 7,

        /// <summary>
        /// Unsettle action type
        /// </summary>
        [Description("Unsettle action type")]
        Unset = 8,

        /// <summary>
        /// Re-settle action type
        /// </summary>
        [Description("Re-settle action type")]
        Reset = 9,

        /// <summary>
        /// Reserve action type
        /// </summary>
        [Description("Reserve action type")]
        Res = 10,

        /// <summary>
        /// Debit reserve action type
        /// </summary>
        [Description("Debit reserve action type")]
        Dbr = 11,

        /// <summary>
        /// Commit reserve action type
        /// </summary>
        [Description("Commit reserve action type")]
        Cmr = 12,

        /// <summary>
        /// Cancel reserve action type
        /// </summary>
        [Description("Cancel reserve action type")]
        Ccr = 13
    }
}