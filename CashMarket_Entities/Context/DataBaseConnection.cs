﻿using System.Data.Common;
using Npgsql;

namespace CashMarket_Entities.Context
{
    public class DataBaseConnection
    {
        public string Name { get; set; }

        public string ConnectionString
        {
            get => Connection?.ConnectionString;
            set => Connection = new NpgsqlConnection(value);
        }

        public bool IsProvider { get; set; }

        public DbConnection Connection { get; private set; }
    }
}
