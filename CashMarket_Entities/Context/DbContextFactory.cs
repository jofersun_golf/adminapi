﻿using System;
using System.Collections.Generic;
using System.Linq;
using CashMarket_Entities.Enums;
using CashMarket_Entities.Models.Admin;
using CashMarket_Entities.Models.Master;
using CashMarket_Entities.Models.Members;
using CashMarket_Entities.Models.Providers;
using CashMarket_Entities.Models.WhiteLabels;
using Microsoft.EntityFrameworkCore;

namespace CashMarket_Entities.Context
{
    public static class DbContextFactory
    {
        public static void Initialize(List<DataBaseConnection> dbConnections)
        {
            Connections = new List<DataBaseConnection>();
            Connections.AddRange(dbConnections);
        }

        public static List<DataBaseConnection> Connections { get; private set; }

        public static string GetCashMarketConnectionString(CashMarketDb cashMarket)
        {
            var connection = Connections?.FirstOrDefault(a => a.Name.Equals(cashMarket.ToString(), StringComparison.CurrentCultureIgnoreCase) && a.IsProvider == false);
            return connection?.ConnectionString;
        }

        public static DbContext GetCashMarketContext(CashMarketDb cashMarket)
        {
            var connection = Connections?.FirstOrDefault(a => a.Name.Equals(cashMarket.ToString(), StringComparison.CurrentCultureIgnoreCase) && a.IsProvider == false);
            if (string.IsNullOrEmpty(connection?.ConnectionString)) throw new NullReferenceException("Initialize database connection first!");

            DbContextOptionsBuilder optionsBuilder;
            switch (cashMarket)
            {
                case CashMarketDb.Admin:
                    optionsBuilder = new DbContextOptionsBuilder<cashmarket_adminContext>();
                    optionsBuilder.UseNpgsql(connection.Connection);
                    return new cashmarket_adminContext((DbContextOptions<cashmarket_adminContext>) optionsBuilder.Options);
                case CashMarketDb.Master:
                    optionsBuilder = new DbContextOptionsBuilder<cashmarket_masterContext>();
                    optionsBuilder.UseNpgsql(connection.Connection);
                    return new cashmarket_masterContext((DbContextOptions<cashmarket_masterContext>)optionsBuilder.Options);
                case CashMarketDb.Members:
                    optionsBuilder = new DbContextOptionsBuilder<cashmarket_membersContext>();
                    optionsBuilder.UseNpgsql(connection.Connection);
                    return new cashmarket_membersContext((DbContextOptions<cashmarket_membersContext>)optionsBuilder.Options);
                case CashMarketDb.Provider:
                    throw new NotImplementedException();
                case CashMarketDb.WhiteLabels:
                    optionsBuilder = new DbContextOptionsBuilder<cashmarket_whitelabelsContext>();
                    optionsBuilder.UseNpgsql(connection.Connection);
                    return new cashmarket_whitelabelsContext((DbContextOptions<cashmarket_whitelabelsContext>)optionsBuilder.Options);
            }
            throw new NullReferenceException("Invalid CashMarket");
        }

        public static cashmarket_provider_aContext GetProviderContext(string providerName)
        {
            var connection = Connections?.FirstOrDefault(a => a.Name.Equals(providerName, StringComparison.CurrentCultureIgnoreCase) && a.IsProvider);
            if (string.IsNullOrEmpty(connection?.ConnectionString)) throw new NullReferenceException("Initialize database connection first!");
            DbContextOptionsBuilder optionsBuilder = new DbContextOptionsBuilder<cashmarket_provider_aContext>();
            optionsBuilder.UseNpgsql(connection.Connection);
            return new cashmarket_provider_aContext((DbContextOptions<cashmarket_provider_aContext>)optionsBuilder.Options);
        }

        public static cashmarket_provider_aContext[] GetProviderContexts()
        {
            List<cashmarket_provider_aContext> providerContexts = new List<cashmarket_provider_aContext>();
            if (Connections != null)
            {
                foreach (DataBaseConnection connection in Connections)
                {
                    if (!connection.IsProvider) continue;
                    DbContextOptionsBuilder optionsBuilder = new DbContextOptionsBuilder<cashmarket_provider_aContext>();
                    optionsBuilder.UseNpgsql(connection.Connection);
                    providerContexts.Add(new cashmarket_provider_aContext((DbContextOptions<cashmarket_provider_aContext>) optionsBuilder.Options));
                }
            }
            return providerContexts.ToArray();
        }

        public static string[] GetProviderConnectionStrings()
        {
            List<string> connStrings = new List<string>();
            if (Connections != null)
            {
                connStrings.AddRange(Connections.Where(a=>a.IsProvider).Select(a=>a.ConnectionString));
            }
            return connStrings.ToArray();
        }
    }
}
