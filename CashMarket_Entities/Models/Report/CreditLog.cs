﻿using System;
using System.Collections.Generic;
using System.Linq;
using X.PagedList;

#nullable disable

namespace CashMarket_Entities.Models.Report
{
    public partial class CreditLog
    {
        public long Id { get; set; }
        public string WhiteLabelCode { get; set; }
        public string WhiteLabelName { get; set; }
        public string WhiteLabelDesc { get; set; }
        public string ProviderCode { get; set; }
        public string ProviderName { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string CategoryCode { get; set; }
        public string CategoryName { get; set; }
        public string PrefixUserName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string FullName { get; set; }
        public short? Gender { get; set; }
        public string Referral { get; set; }
        public string ReferralReference { get; set; }
        public string SerialId { get; set; }
        public decimal Amount { get; set; }
        public decimal StartBalance { get; set; }
        public decimal EndBalance { get; set; }
        public string BetData { get; set; }
        public DateTime? CreateTime { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateTime { get; set; }
        public string UpdateBy { get; set; }
        public short Status { get; set; }
        public short ActionType { get; set; }
        public string ActionTypeCode { get; set; }
        public string ActionTypeDesc { get; set; }
        public short WalletTypeId { get; set; }
        public string WalletTypeCode { get; set; }
        public string WalletTypeDesc { get; set; }
        public int Period { get; set; }

        public static IEnumerable<CreditLog> GetCreditLogs(out decimal grandTotalAmount, int page = 1, int size = 20, DateTime? from = null, DateTime? to = null,
            string keyWord = "", string[] providerCodes = null, string[] categoryCodes = null, string[] productCodes = null, string sort = "")
        {
            cashmarket_reportContext context = new cashmarket_reportContext();
            IQueryable<CreditLog> query = context.CreditLogs;

            if (from.HasValue)
            {
                query = query.Where(a =>
                    (a.CreateTime.HasValue && a.CreateTime.Value.Date >= from.Value) ||
                    (a.UpdateTime.HasValue && a.UpdateTime.Value.Date >= from.Value));
            }
            if (to.HasValue)
            {
                query = query.Where(a =>
                    (a.CreateTime.HasValue && a.CreateTime.Value.Date <= to.Value) ||
                    (a.UpdateTime.HasValue && a.UpdateTime.Value.Date <= to.Value));
            }

            if (providerCodes != null && providerCodes.Any())
            {
                var providerCodes2 = providerCodes.Select(a => a.ToLower()).ToList();
                query = query.Where(a => providerCodes2.Contains(a.ProviderCode.ToLower()));
            }

            if (categoryCodes != null && categoryCodes.Any())
            {
                var categoryCodes2 = categoryCodes.Select(a => a.ToLower()).ToList();
                query = query.Where(a => categoryCodes2.Contains(a.CategoryCode.ToLower()));
            }

            if (productCodes != null && productCodes.Any())
            {
                var productCodes2 = productCodes.Select(a => a.ToLower()).ToList();
                query = query.Where(a => productCodes2.Contains(a.ProductCode.ToLower()));
            }

            if (!string.IsNullOrEmpty(sort))
            {
                switch (sort.ToLower())
                {
                    case "username":
                        query = query.OrderBy(a => a.UserName);
                        break;
                    case "username_desc":
                        query = query.OrderByDescending(a => a.UserName);
                        break;
                    case "fullname":
                        query = query.OrderBy(a => a.FullName);
                        break;
                    case "fullname_desc":
                        query = query.OrderByDescending(a => a.FullName);
                        break;
                    case "status":
                        query = query.OrderBy(a => a.Status);
                        break;
                    case "status_desc":
                        query = query.OrderByDescending(a => a.Status);
                        break;
                    default:
                        query = query.OrderByDescending(a => a.CreateTime);
                        break;
                }
            }

            grandTotalAmount = query.Sum(a => a.Amount);

            return query.ToPagedList(page, size == 0 ? int.MaxValue : size);
        }
    }
}
