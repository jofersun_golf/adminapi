﻿using System;
using CashMarket_Entities.Context;
using CashMarket_Entities.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace CashMarket_Entities.Models.Report
{
    public partial class cashmarket_reportContext : DbContext
    {
        public cashmarket_reportContext()
        {
            var connection = DbContextFactory.GetCashMarketConnectionString(CashMarketDb.Report);
            base.Database.SetConnectionString(connection);
        }

        public cashmarket_reportContext(DbContextOptions<cashmarket_reportContext> options)
            : base(options)
        {
        }

        public virtual DbSet<CreditLog> CreditLogs { get; set; }
        public virtual DbSet<Wager> Wagers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseNpgsql("Host=13.212.37.122;Database=cashmarket_report;Username=postgres;Password=tristan123");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "C.UTF-8");

            modelBuilder.Entity<CreditLog>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("credit_logs");

                entity.Property(e => e.ActionType).HasColumnName("action_type");

                entity.Property(e => e.ActionTypeCode)
                    .HasMaxLength(25)
                    .HasColumnName("action_type_code");

                entity.Property(e => e.ActionTypeDesc)
                    .HasMaxLength(100)
                    .HasColumnName("action_type_desc");

                entity.Property(e => e.Amount)
                    .HasPrecision(22, 4)
                    .HasColumnName("amount");

                entity.Property(e => e.BetData)
                    .HasColumnType("jsonb")
                    .HasColumnName("bet_data");

                entity.Property(e => e.CategoryCode)
                    .HasMaxLength(25)
                    .HasColumnName("category_code");

                entity.Property(e => e.CategoryName)
                    .HasMaxLength(100)
                    .HasColumnName("category_name");

                entity.Property(e => e.CreateBy)
                    .HasMaxLength(50)
                    .HasColumnName("create_by");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("create_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.DateOfBirth)
                    .HasColumnType("date")
                    .HasColumnName("date_of_birth");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(150)
                    .HasColumnName("email");

                entity.Property(e => e.EndBalance)
                    .HasPrecision(22, 4)
                    .HasColumnName("end_balance");

                entity.Property(e => e.FullName)
                    .HasMaxLength(150)
                    .HasColumnName("full_name");

                entity.Property(e => e.Gender).HasColumnName("gender");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Period).HasColumnName("_period");

                entity.Property(e => e.PrefixUserName)
                    .IsRequired()
                    .HasMaxLength(65)
                    .HasColumnName("prefix_user_name");

                entity.Property(e => e.ProductCode)
                    .HasMaxLength(15)
                    .HasColumnName("product_code");

                entity.Property(e => e.ProductName)
                    .HasMaxLength(50)
                    .HasColumnName("product_name");

                entity.Property(e => e.ProviderCode)
                    .HasMaxLength(25)
                    .HasColumnName("provider_code");

                entity.Property(e => e.ProviderName)
                    .HasMaxLength(50)
                    .HasColumnName("provider_name");

                entity.Property(e => e.Referral)
                    .HasMaxLength(50)
                    .HasColumnName("referral");

                entity.Property(e => e.ReferralReference)
                    .HasMaxLength(50)
                    .HasColumnName("referral_reference");

                entity.Property(e => e.SerialId)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("serial_id");

                entity.Property(e => e.StartBalance)
                    .HasPrecision(22, 4)
                    .HasColumnName("start_balance");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.UpdateBy)
                    .HasMaxLength(50)
                    .HasColumnName("update_by");

                entity.Property(e => e.UpdateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("update_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("user_name");

                entity.Property(e => e.WalletTypeCode)
                    .HasMaxLength(25)
                    .HasColumnName("wallet_type_code");

                entity.Property(e => e.WalletTypeDesc)
                    .HasMaxLength(100)
                    .HasColumnName("wallet_type_desc");

                entity.Property(e => e.WalletTypeId).HasColumnName("wallet_type_id");

                entity.Property(e => e.WhiteLabelCode)
                    .HasMaxLength(10)
                    .HasColumnName("white_label_code");

                entity.Property(e => e.WhiteLabelDesc)
                    .HasMaxLength(100)
                    .HasColumnName("white_label_desc");

                entity.Property(e => e.WhiteLabelName)
                    .HasMaxLength(25)
                    .HasColumnName("white_label_name");
            });
            
            modelBuilder.Entity<Wager>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("wagers");

                entity.HasIndex(e => e.Period, "wagers__period_idx")
                    .HasNullSortOrder(new[] { NullSortOrder.NullsLast })
                    .HasSortOrder(new[] { SortOrder.Descending });

                entity.HasIndex(e => e.WhiteLabelCode, "wagers_white_label_code_idx");

                entity.Property(e => e.ActualRealBet)
                    .HasPrecision(22, 4)
                    .HasColumnName("actual_real_bet");

                entity.Property(e => e.BetData)
                    .HasColumnType("jsonb")
                    .HasColumnName("bet_data");

                entity.Property(e => e.BetStatus).HasColumnName("bet_status");

                entity.Property(e => e.CategoryCode)
                    .IsRequired()
                    .HasMaxLength(25)
                    .HasColumnName("category_code");

                entity.Property(e => e.CategoryName)
                    .HasMaxLength(100)
                    .HasColumnName("category_name");

                entity.Property(e => e.CreateBy)
                    .HasMaxLength(50)
                    .HasColumnName("create_by");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("create_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.DateOfBirth)
                    .HasColumnType("date")
                    .HasColumnName("date_of_birth");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(150)
                    .HasColumnName("email");

                entity.Property(e => e.FullName)
                    .HasMaxLength(150)
                    .HasColumnName("full_name");

                entity.Property(e => e.GameLastUpdateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("game_last_update_time");

                entity.Property(e => e.Gender).HasColumnName("gender");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.IsSynch).HasColumnName("is_synch");

                entity.Property(e => e.PayoutAmount)
                    .HasPrecision(22, 4)
                    .HasColumnName("payout_amount");

                entity.Property(e => e.Period).HasColumnName("_period");

                entity.Property(e => e.PlaceTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("place_time");

                entity.Property(e => e.PrefixUserName)
                    .IsRequired()
                    .HasMaxLength(65)
                    .HasColumnName("prefix_user_name");

                entity.Property(e => e.ProductCode)
                    .IsRequired()
                    .HasMaxLength(15)
                    .HasColumnName("product_code");

                entity.Property(e => e.ProductName)
                    .HasMaxLength(50)
                    .HasColumnName("product_name");

                entity.Property(e => e.ProviderCode)
                    .IsRequired()
                    .HasMaxLength(25)
                    .HasColumnName("provider_code");

                entity.Property(e => e.ProviderName)
                    .HasMaxLength(50)
                    .HasColumnName("provider_name");

                entity.Property(e => e.RefKey)
                    .HasMaxLength(100)
                    .HasColumnName("ref_key");

                entity.Property(e => e.Referral)
                    .HasMaxLength(50)
                    .HasColumnName("referral");

                entity.Property(e => e.ReferralReference)
                    .HasMaxLength(50)
                    .HasColumnName("referral_reference");

                entity.Property(e => e.SerialId)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("serial_id");

                entity.Property(e => e.StakeAmount)
                    .HasPrecision(22, 4)
                    .HasColumnName("stake_amount");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.UpdateBy)
                    .HasMaxLength(50)
                    .HasColumnName("update_by");

                entity.Property(e => e.UpdateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("update_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("user_name");

                entity.Property(e => e.WhiteLabelCode)
                    .IsRequired()
                    .HasMaxLength(10)
                    .HasColumnName("white_label_code");

                entity.Property(e => e.WhiteLabelDesc)
                    .HasMaxLength(100)
                    .HasColumnName("white_label_desc");

                entity.Property(e => e.WhiteLabelName)
                    .IsRequired()
                    .HasMaxLength(25)
                    .HasColumnName("white_label_name");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
