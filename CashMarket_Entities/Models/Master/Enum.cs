﻿using System;

#nullable disable

namespace CashMarket_Entities.Models.Master
{
    public partial class Enum
    {
        public int Id { get; set; }
        public string EnumType { get; set; }
        public short EnumValue { get; set; }
        public string EnumCode { get; set; }
        public string EnumDesc { get; set; }
        public DateTime? CreateTime { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateTime { get; set; }
        public string UpdateBy { get; set; }
        public short Status { get; set; }
    }
}
