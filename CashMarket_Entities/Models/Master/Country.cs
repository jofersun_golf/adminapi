﻿using System;

#nullable disable

namespace CashMarket_Entities.Models.Master
{
    public partial class Country
    {
        public int Id { get; set; }
        public string CountryCode { get; set; }
        public string CountrySymbol { get; set; }
        public string CountryName { get; set; }
        public DateTime? CreateTime { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateTime { get; set; }
        public string UpdateBy { get; set; }
        public short Status { get; set; }
        public string PhoneCode { get; set; }
    }
}
