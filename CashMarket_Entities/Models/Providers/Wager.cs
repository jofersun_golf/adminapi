﻿using System;

#nullable disable

namespace CashMarket_Entities.Models.Providers
{
    public partial class Wager
    {
        public long Id { get; set; }
        public string PrefixUserName { get; set; }
        public string ProviderCode { get; set; }
        public string ProductCode { get; set; }
        public string SerialId { get; set; }
        public decimal StakeAmount { get; set; }
        public decimal PayoutAmount { get; set; }
        public decimal ActualRealBet { get; set; }
        public string BetData { get; set; }
        public string RefKey { get; set; }
        public bool IsSynch { get; set; }
        public short BetStatus { get; set; }
        public DateTime? CreateTime { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateTime { get; set; }
        public string UpdateBy { get; set; }
        public short Status { get; set; }
        public DateTime? PlaceTime { get; set; }
        public DateTime? GameLastUpdateTime { get; set; }
    }
}
