﻿using Microsoft.EntityFrameworkCore;

#nullable disable

namespace CashMarket_Entities.Models.Providers
{
    public partial class cashmarket_provider_aContext : DbContext
    {
        public cashmarket_provider_aContext()
        {
        }

        public cashmarket_provider_aContext(DbContextOptions<cashmarket_provider_aContext> options)
            : base(options)
        {
        }

        public virtual DbSet<CreditLog> CreditLogs { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<Provider> Providers { get; set; }
        public virtual DbSet<Wager> Wagers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseNpgsql("Host=13.212.37.122;Database=cashmarket_provider_a;Username=postgres;Password=tristan123");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "C.UTF-8");

            modelBuilder.Entity<CreditLog>(entity =>
            {
                entity.ToTable("credit_logs");

                entity.HasIndex(e => e.ProductCode, "fki_fk_product_credit_logs");

                entity.HasIndex(e => e.ProviderCode, "fki_fk_provider_credit_logs");

                entity.HasIndex(e => e.PrefixUserName, "fki_fk_user_credit_logs");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ActionType).HasColumnName("action_type");

                entity.Property(e => e.Amount)
                    .HasPrecision(22, 4)
                    .HasColumnName("amount");

                entity.Property(e => e.BetData)
                    .HasColumnType("jsonb")
                    .HasColumnName("bet_data");

                entity.Property(e => e.CreateBy)
                    .HasMaxLength(50)
                    .HasColumnName("create_by");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("create_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.EndBalance)
                    .HasPrecision(22, 4)
                    .HasColumnName("end_balance");

                entity.Property(e => e.PrefixUserName)
                    .IsRequired()
                    .HasMaxLength(65)
                    .HasColumnName("prefix_user_name");

                entity.Property(e => e.ProductCode)
                    .IsRequired()
                    .HasMaxLength(15)
                    .HasColumnName("product_code");

                entity.Property(e => e.ProviderCode)
                    .IsRequired()
                    .HasMaxLength(25)
                    .HasColumnName("provider_code");

                entity.Property(e => e.SerialId)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("serial_id");

                entity.Property(e => e.StartBalance)
                    .HasPrecision(22, 4)
                    .HasColumnName("start_balance");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.UpdateBy)
                    .HasMaxLength(50)
                    .HasColumnName("update_by");

                entity.Property(e => e.UpdateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("update_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.WalletTypeId).HasColumnName("wallet_type_id");
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.ToTable("products");

                entity.HasIndex(e => e.ProviderId, "fki_fk_provider_products");

                entity.HasIndex(e => e.ProductCode, "uix_product_code")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.CreateBy)
                    .HasMaxLength(50)
                    .HasColumnName("create_by");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("create_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.ProductCode)
                    .IsRequired()
                    .HasMaxLength(15)
                    .HasColumnName("product_code");

                entity.Property(e => e.ProductName)
                    .HasMaxLength(50)
                    .HasColumnName("product_name");

                entity.Property(e => e.ProviderId).HasColumnName("provider_id");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.UpdateBy)
                    .HasMaxLength(50)
                    .HasColumnName("update_by");

                entity.Property(e => e.UpdateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("update_time")
                    .HasDefaultValueSql("now()");

                entity.HasOne(d => d.Provider)
                    .WithMany(p => p.Products)
                    .HasForeignKey(d => d.ProviderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_provider_products");
            });

            modelBuilder.Entity<Provider>(entity =>
            {
                entity.ToTable("providers");

                entity.HasIndex(e => e.CurrencyId, "fki_fk_currency_providers");

                entity.HasIndex(e => e.ProviderCode, "uix_provider_code")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.CreateBy)
                    .HasMaxLength(50)
                    .HasColumnName("create_by");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("create_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.CurrencyId)
                    .IsRequired()
                    .HasMaxLength(5)
                    .HasColumnName("currency_id");

                entity.Property(e => e.IsSeamless)
                    .IsRequired()
                    .HasColumnName("is_seamless")
                    .HasDefaultValueSql("true");

                entity.Property(e => e.ProviderCode)
                    .IsRequired()
                    .HasMaxLength(25)
                    .HasColumnName("provider_code");

                entity.Property(e => e.ProviderName)
                    .HasMaxLength(50)
                    .HasColumnName("provider_name");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.UpdateBy)
                    .HasMaxLength(50)
                    .HasColumnName("update_by");

                entity.Property(e => e.UpdateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("update_time")
                    .HasDefaultValueSql("now()");
            });

            modelBuilder.Entity<Wager>(entity =>
            {
                entity.ToTable("wagers");

                entity.HasIndex(e => e.ProductCode, "fki_fk_product_wagers");

                entity.HasIndex(e => e.ProviderCode, "fki_fk_provider_wagers");

                entity.HasIndex(e => e.PrefixUserName, "fki_fk_user_wagers");

                entity.HasIndex(e => e.SerialId, "uix_serial_id")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ActualRealBet)
                    .HasPrecision(22, 4)
                    .HasColumnName("actual_real_bet");

                entity.Property(e => e.BetData)
                    .HasColumnType("jsonb")
                    .HasColumnName("bet_data");

                entity.Property(e => e.BetStatus).HasColumnName("bet_status");

                entity.Property(e => e.CreateBy)
                    .HasMaxLength(50)
                    .HasColumnName("create_by");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("create_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.GameLastUpdateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("game_last_update_time");

                entity.Property(e => e.IsSynch).HasColumnName("is_synch");

                entity.Property(e => e.PayoutAmount)
                    .HasPrecision(22, 4)
                    .HasColumnName("payout_amount");

                entity.Property(e => e.PlaceTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("place_time");

                entity.Property(e => e.PrefixUserName)
                    .IsRequired()
                    .HasMaxLength(65)
                    .HasColumnName("prefix_user_name");

                entity.Property(e => e.ProductCode)
                    .IsRequired()
                    .HasMaxLength(15)
                    .HasColumnName("product_code");

                entity.Property(e => e.ProviderCode)
                    .IsRequired()
                    .HasMaxLength(25)
                    .HasColumnName("provider_code");

                entity.Property(e => e.RefKey)
                    .HasMaxLength(100)
                    .HasColumnName("ref_key");

                entity.Property(e => e.SerialId)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("serial_id");

                entity.Property(e => e.StakeAmount)
                    .HasPrecision(22, 4)
                    .HasColumnName("stake_amount");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.UpdateBy)
                    .HasMaxLength(50)
                    .HasColumnName("update_by");

                entity.Property(e => e.UpdateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("update_time")
                    .HasDefaultValueSql("now()");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
