﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CashMarket_Entities.Models.Providers
{
    public partial class Provider
    {
        public Provider()
        {
            Products = new HashSet<Product>();
        }

        public int Id { get; set; }
        public string CurrencyId { get; set; }
        public string ProviderCode { get; set; }
        public string ProviderName { get; set; }
        public bool? IsSeamless { get; set; }
        public DateTime? CreateTime { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateTime { get; set; }
        public string UpdateBy { get; set; }
        public short Status { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}
