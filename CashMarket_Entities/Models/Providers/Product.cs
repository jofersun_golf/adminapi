﻿using System;

#nullable disable

namespace CashMarket_Entities.Models.Providers
{
    public partial class Product
    {
        public int Id { get; set; }
        public int ProviderId { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public DateTime? CreateTime { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateTime { get; set; }
        public string UpdateBy { get; set; }
        public short Status { get; set; }

        public virtual Provider Provider { get; set; }
    }
}
