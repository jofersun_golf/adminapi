﻿using System;

#nullable disable

namespace CashMarket_Entities.Models.Providers
{
    public partial class CreditLog
    {
        public long Id { get; set; }
        public string PrefixUserName { get; set; }
        public string ProviderCode { get; set; }
        public string ProductCode { get; set; }
        public string SerialId { get; set; }
        public decimal Amount { get; set; }
        public decimal StartBalance { get; set; }
        public decimal EndBalance { get; set; }
        public string BetData { get; set; }
        public DateTime? CreateTime { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateTime { get; set; }
        public string UpdateBy { get; set; }
        public short Status { get; set; }
        public short ActionType { get; set; }
        public short WalletTypeId { get; set; }
    }
}
