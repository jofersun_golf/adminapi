﻿using System;
using System.Collections.Generic;
using System.Linq;
using X.PagedList;

#nullable disable

namespace CashMarket_Entities.Models.WhiteLabels
{
    public partial class WhiteLabelProduct
    {
        public int Id { get; set; }
        public int WhiteLabelId { get; set; }
        public string ProviderCode { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public DateTime? CreateTime { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateTime { get; set; }
        public string UpdateBy { get; set; }
        public short Status { get; set; }

        public virtual WhiteLabel WhiteLabel { get; set; }

        public static IEnumerable<WhiteLabelProduct> GetWhiteLabelProducts(int page = 1, int size = 20, string sort = "", short? status = null)
        {
            cashmarket_whitelabelsContext context = new cashmarket_whitelabelsContext();
            IQueryable<WhiteLabelProduct> query = context.WhiteLabelProducts;

            if (status.HasValue)
            {
                query = query.Where(a => a.Status == status.Value);
            }

            if (!string.IsNullOrEmpty(sort))
            {
                switch (sort.ToLower())
                {
                    case "providercode":
                        query = query.OrderBy(a => a.ProviderCode);
                        break;
                    case "providercode_desc":
                        query = query.OrderByDescending(a => a.ProviderCode);
                        break;
                    case "productcode":
                        query = query.OrderBy(a => a.ProductCode);
                        break;
                    case "productcode_desc":
                        query = query.OrderByDescending(a => a.ProductCode);
                        break;
                    case "productname":
                        query = query.OrderBy(a => a.ProductName);
                        break;
                    case "productname_desc":
                        query = query.OrderByDescending(a => a.ProductName);
                        break;
                    case "status":
                        query = query.OrderBy(a => a.Status);
                        break;
                    case "status_desc":
                        query = query.OrderByDescending(a => a.Status);
                        break;
                    default:
                        query = query.OrderBy(a => a.ProductName);
                        break;
                }
            }

            return query.ToPagedList(page, size == 0 ? int.MaxValue : size);
        }
    }
}
