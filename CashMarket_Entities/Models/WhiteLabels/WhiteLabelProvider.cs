﻿using System;

#nullable disable

namespace CashMarket_Entities.Models.WhiteLabels
{
    public partial class WhiteLabelProvider
    {
        public int Id { get; set; }
        public string ProviderCode { get; set; }
        public string ProviderName { get; set; }
        public string ApiKey { get; set; }
        public string ApiEndpoint { get; set; }
        public bool? IsSeamless { get; set; }
        public DateTime? CreateTime { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateTime { get; set; }
        public string UpdateBy { get; set; }
        public short Status { get; set; }
        public string Landingpage { get; set; }
        public string Operatorid { get; set; }
        public string Suboperatorid { get; set; }
        public string Playerprefix { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Publickey { get; set; }
        public string Country { get; set; }
        public string WhiteLabelCode { get; set; }
        public string CurrencyCode { get; set; }
        public string Brandname { get; set; }
    }
}
