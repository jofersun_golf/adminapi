﻿using System;

#nullable disable

namespace CashMarket_Entities.Models.WhiteLabels
{
    public partial class WhiteLabelLanguage
    {
        public int Id { get; set; }
        public int WhiteLabelId { get; set; }
        public string WhiteLabelCode { get; set; }
        public string LanguageCode { get; set; }
        public string LanguageSymbol { get; set; }
        public bool? EnableStatus { get; set; }
        public int? SortNumber { get; set; }
        public DateTime? CreateTime { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateTime { get; set; }
        public string UpdateBy { get; set; }
        public short Status { get; set; }
    }
}
