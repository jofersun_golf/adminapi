﻿using CashMarket_Entities.Context;
using CashMarket_Entities.Enums;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace CashMarket_Entities.Models.WhiteLabels
{
    public partial class cashmarket_whitelabelsContext : DbContext
    {
        public cashmarket_whitelabelsContext()
        {
            var connection = DbContextFactory.GetCashMarketConnectionString(CashMarketDb.WhiteLabels);
            base.Database.SetConnectionString(connection);
        }

        public cashmarket_whitelabelsContext(DbContextOptions<cashmarket_whitelabelsContext> options)
            : base(options)
        {
        }

        public virtual DbSet<WhiteLabel> WhiteLabels { get; set; }
        public virtual DbSet<WhiteLabelLanguage> WhiteLabelLanguages { get; set; }
        public virtual DbSet<WhiteLabelProduct> WhiteLabelProducts { get; set; }
        public virtual DbSet<WhiteLabelProvider> WhiteLabelProviders { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseNpgsql("Host=13.212.37.122;Database=cashmarket_whitelabels;Username=postgres;Password=tristan123");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "C.UTF-8");

            modelBuilder.Entity<WhiteLabel>(entity =>
            {
                entity.ToTable("white_labels");

                entity.HasIndex(e => e.WhiteLabelCode, "uix_white_labels_code")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CreateBy)
                    .HasMaxLength(50)
                    .HasColumnName("create_by");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("create_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.PublicKey)
                    .IsRequired()
                    .HasMaxLength(32)
                    .HasColumnName("public_key");

                entity.Property(e => e.SecretKey)
                    .IsRequired()
                    .HasMaxLength(64)
                    .HasColumnName("secret_key");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.UpdateBy)
                    .HasMaxLength(50)
                    .HasColumnName("update_by");

                entity.Property(e => e.UpdateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("update_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.WhiteLabelCode)
                    .IsRequired()
                    .HasMaxLength(10)
                    .HasColumnName("white_label_code");

                entity.Property(e => e.WhiteLabelDesc)
                    .HasMaxLength(100)
                    .HasColumnName("white_label_desc");

                entity.Property(e => e.WhiteLabelName)
                    .IsRequired()
                    .HasMaxLength(25)
                    .HasColumnName("white_label_name");
            });

            modelBuilder.Entity<WhiteLabelLanguage>(entity =>
            {
                entity.ToTable("white_label_languages");

                entity.HasIndex(e => e.LanguageCode, "fki_fk_language_white_label_languages");

                entity.HasIndex(e => e.WhiteLabelId, "fki_fk_wl_white_label_languages");

                entity.HasIndex(e => new { e.WhiteLabelCode, e.LanguageCode }, "uix_wlcode_languages")
                    .IsUnique();

                entity.HasIndex(e => new { e.WhiteLabelId, e.LanguageCode }, "uix_wlid_languages")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CreateBy)
                    .HasMaxLength(50)
                    .HasColumnName("create_by");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("create_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.EnableStatus)
                    .HasColumnName("enable_status")
                    .HasDefaultValueSql("false");

                entity.Property(e => e.LanguageCode)
                    .IsRequired()
                    .HasMaxLength(15)
                    .HasColumnName("language_code");

                entity.Property(e => e.LanguageSymbol)
                    .HasMaxLength(25)
                    .HasColumnName("language_symbol");

                entity.Property(e => e.SortNumber).HasColumnName("sort_number");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.UpdateBy)
                    .HasMaxLength(50)
                    .HasColumnName("update_by");

                entity.Property(e => e.UpdateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("update_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.WhiteLabelCode)
                    .IsRequired()
                    .HasMaxLength(10)
                    .HasColumnName("white_label_code");

                entity.Property(e => e.WhiteLabelId).HasColumnName("white_label_id");
            });

            modelBuilder.Entity<WhiteLabelProduct>(entity =>
            {
                entity.ToTable("white_label_products");

                entity.HasIndex(e => e.ProductCode, "fki_fk_product_white_label_products");

                entity.HasIndex(e => e.ProviderCode, "fki_fk_provider_white_label_products");

                entity.HasIndex(e => e.WhiteLabelId, "fki_fk_wl_white_label_products");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CreateBy)
                    .HasMaxLength(50)
                    .HasColumnName("create_by");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("create_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.ProductCode)
                    .IsRequired()
                    .HasMaxLength(25)
                    .HasColumnName("product_code");

                entity.Property(e => e.ProductName)
                    .HasMaxLength(100)
                    .HasColumnName("product_name");

                entity.Property(e => e.ProviderCode)
                    .IsRequired()
                    .HasMaxLength(25)
                    .HasColumnName("provider_code");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.UpdateBy)
                    .HasMaxLength(50)
                    .HasColumnName("update_by");

                entity.Property(e => e.UpdateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("update_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.WhiteLabelId).HasColumnName("white_label_id");

                entity.HasOne(d => d.WhiteLabel)
                    .WithMany(p => p.WhiteLabelProducts)
                    .HasForeignKey(d => d.WhiteLabelId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_wl_white_label_products");
            });

            modelBuilder.Entity<WhiteLabelProvider>(entity =>
            {
                entity.ToTable("white_label_providers");

                entity.HasIndex(e => e.ProviderCode, "fki_fk_provider_white_label_providers");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ApiEndpoint)
                    .IsRequired()
                    .HasMaxLength(500)
                    .HasColumnName("api_endpoint");

                entity.Property(e => e.ApiKey)
                    .IsRequired()
                    .HasMaxLength(2000)
                    .HasColumnName("api_key");

                entity.Property(e => e.Brandname)
                    .HasMaxLength(50)
                    .HasColumnName("brandname");

                entity.Property(e => e.Country)
                    .HasMaxLength(2000)
                    .HasColumnName("country");

                entity.Property(e => e.CreateBy)
                    .HasMaxLength(50)
                    .HasColumnName("create_by");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("create_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.CurrencyCode)
                    .IsRequired()
                    .HasMaxLength(5)
                    .HasColumnName("currency_code");

                entity.Property(e => e.IsSeamless)
                    .IsRequired()
                    .HasColumnName("is_seamless")
                    .HasDefaultValueSql("true");

                entity.Property(e => e.Landingpage)
                    .HasMaxLength(2000)
                    .HasColumnName("landingpage");

                entity.Property(e => e.Operatorid)
                    .HasMaxLength(2000)
                    .HasColumnName("operatorid");

                entity.Property(e => e.Password)
                    .HasMaxLength(2000)
                    .HasColumnName("password");

                entity.Property(e => e.Playerprefix)
                    .HasMaxLength(2000)
                    .HasColumnName("playerprefix");

                entity.Property(e => e.ProviderCode)
                    .IsRequired()
                    .HasMaxLength(25)
                    .HasColumnName("provider_code");

                entity.Property(e => e.ProviderName)
                    .HasMaxLength(50)
                    .HasColumnName("provider_name");

                entity.Property(e => e.Publickey)
                    .HasMaxLength(2000)
                    .HasColumnName("publickey");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Suboperatorid)
                    .HasMaxLength(2000)
                    .HasColumnName("suboperatorid");

                entity.Property(e => e.UpdateBy)
                    .HasMaxLength(50)
                    .HasColumnName("update_by");

                entity.Property(e => e.UpdateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("update_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.Username)
                    .HasMaxLength(2000)
                    .HasColumnName("username");

                entity.Property(e => e.WhiteLabelCode)
                    .IsRequired()
                    .HasMaxLength(10)
                    .HasColumnName("white_label_code");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
