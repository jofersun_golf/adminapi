﻿using System;
using System.Collections.Generic;
using System.Linq;
using X.PagedList;

#nullable disable

namespace CashMarket_Entities.Models.WhiteLabels
{
    public partial class WhiteLabel
    {
        public WhiteLabel()
        {
            WhiteLabelProducts = new HashSet<WhiteLabelProduct>();
        }

        public int Id { get; set; }
        public string WhiteLabelCode { get; set; }
        public string WhiteLabelName { get; set; }
        public string WhiteLabelDesc { get; set; }
        public string SecretKey { get; set; }
        public string PublicKey { get; set; }
        public DateTime? CreateTime { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateTime { get; set; }
        public string UpdateBy { get; set; }
        public short Status { get; set; }

        public virtual ICollection<WhiteLabelProduct> WhiteLabelProducts { get; set; }

        public static WhiteLabel GetByCode(string code)
        {
            if (string.IsNullOrEmpty(code)) return null;
            var context = new cashmarket_whitelabelsContext();
            return context.WhiteLabels.FirstOrDefault(a => a.WhiteLabelCode.ToLower().Equals(code.ToLower()));
        }

        public static IEnumerable<WhiteLabel> GetWhiteLabels(int page = 1, int size = 20, string sort = "", string keyword = "")
        {
            cashmarket_whitelabelsContext context = new cashmarket_whitelabelsContext();
            IQueryable<WhiteLabel> query = context.WhiteLabels;

            if (!string.IsNullOrEmpty(keyword))
            {
                query = query.Where(a => a.WhiteLabelCode.Contains(keyword)
                                         || a.WhiteLabelName.Contains(keyword)
                                         || a.WhiteLabelDesc.ToLower().Contains(keyword));
            }

            if (!string.IsNullOrEmpty(sort))
            {
                switch (sort.ToLower())
                {
                    case "whitelabelcode":
                        query = query.OrderBy(a => a.WhiteLabelCode);
                        break;
                    case "whitelabelcode_desc":
                        query = query.OrderByDescending(a => a.WhiteLabelCode);
                        break;
                    case "whitelabelname":
                        query = query.OrderBy(a => a.WhiteLabelName);
                        break;
                    case "whitelabelname_desc":
                        query = query.OrderByDescending(a => a.WhiteLabelName);
                        break;
                    case "status":
                        query = query.OrderBy(a => a.Status);
                        break;
                    case "status_desc":
                        query = query.OrderByDescending(a => a.Status);
                        break;
                    default:
                        query = query.OrderBy(a => a.WhiteLabelCode);
                        break;
                }
            }

            return query.ToPagedList(page, size == 0 ? int.MaxValue : size);
        }
    }
}
