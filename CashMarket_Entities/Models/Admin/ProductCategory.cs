﻿using System;
using System.Collections.Generic;
using System.Linq;
using X.PagedList;

#nullable disable

namespace CashMarket_Entities.Models.Admin
{
    public partial class ProductCategory
    {
        public int Id { get; set; }
        public string WhiteLabelCode { get; set; }
        public string ProviderCode { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string CategoryCode { get; set; }
        public string CategoryName { get; set; }
        public int? ItemSort { get; set; }
        public DateTime? CreateTime { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateTime { get; set; }
        public string UpdateBy { get; set; }
        public short Status { get; set; }

        public static IEnumerable<ProductCategory> GetProductCategories(int page = 1, int size = 20, string sort = "", short? status = null)
        {
            cashmarket_adminContext context = new cashmarket_adminContext();
            IQueryable<ProductCategory> query = context.ProductCategories;

            if (status.HasValue)
            {
                query = query.Where(a => a.Status == status.Value);
            }

            if (!string.IsNullOrEmpty(sort))
            {
                switch (sort.ToLower())
                {
                    case "whitelabelcode":
                        query = query.OrderBy(a => a.WhiteLabelCode);
                        break;
                    case "whitelabelcode_desc":
                        query = query.OrderByDescending(a => a.WhiteLabelCode);
                        break;
                    case "providercode":
                        query = query.OrderBy(a => a.ProviderCode);
                        break;
                    case "providercode_desc":
                        query = query.OrderByDescending(a => a.ProviderCode);
                        break;
                    case "productcode":
                        query = query.OrderBy(a => a.ProductCode);
                        break;
                    case "productcode_desc":
                        query = query.OrderByDescending(a => a.ProductCode);
                        break;
                    case "productname":
                        query = query.OrderBy(a => a.ProductName);
                        break;
                    case "productname_desc":
                        query = query.OrderByDescending(a => a.ProductName);
                        break;
                    case "status":
                        query = query.OrderBy(a => a.Status);
                        break;
                    case "status_desc":
                        query = query.OrderByDescending(a => a.Status);
                        break;
                    default:
                        query = query.OrderBy(a => a.ProductName);
                        break;
                }
            }

            return query.ToPagedList(page, size == 0 ? int.MaxValue : size);
        }

        #region Save Method

        public bool Add()
        {
            cashmarket_adminContext context = new cashmarket_adminContext();
            CreateTime = DateTime.Now;
            context.ProductCategories.Add(this);
            return context.SaveChanges() > 0;
        }

        public bool Update()
        {
            cashmarket_adminContext context = new cashmarket_adminContext();
            var item = context.ProductCategories.Find(this.Id);
            if (item == null) return false;
            var properties = this.GetType().GetProperties();
            foreach (var property in properties)
            {
                if (!property.GetGetMethod().IsVirtual)
                {
                    property.SetValue(item, property.GetValue(this));
                }
            }
            item.UpdateTime = DateTime.Now;
            return context.SaveChanges() > 0;
        }

        public bool Remove()
        {
            cashmarket_adminContext context = new cashmarket_adminContext();
            var item = context.ProductCategories.Find(this.Id);
            if (!context.ProductCategories.Any(a => a.Id.Equals(this.Id) && a.CategoryCode.ToLower().Equals(this.CategoryCode.ToLower())
                && a.ProductCode.ToLower().Equals(this.ProductCode.ToLower()))) return true;
            context.ProductCategories.Remove(item);
            return context.SaveChanges() > 0;
        }

        #endregion

        public static ProductCategory GetById(int id)
        {
            var context = new cashmarket_adminContext();
            return context.ProductCategories.Find(id);
        }

    }
}
