﻿using System;
using System.Collections.Generic;
using System.Linq;
using CashMarket_Entities.Enums;
using CashMarket_Entities.Utilities;
using Microsoft.EntityFrameworkCore;
using X.PagedList;

#nullable disable

namespace CashMarket_Entities.Models.Admin
{
    public partial class User
    {
        public User()
        {
            LoginHistories = new HashSet<LoginHistory>();
            UserRoles = new HashSet<UserRole>();
            UserWhiteLabels = new HashSet<UserWhiteLabel>();
        }

        public int Id { get; set; }
        public string UserName { get; set; }
        public string PasswordHash { get; set; }
        public string Salt { get; set; }
        public string EmailAddress { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public bool Verified { get; set; }
        public DateTime? VerifiedTime { get; set; }
        public int WrongCount { get; set; }
        public DateTime? CreateTime { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateTime { get; set; }
        public string UpdateBy { get; set; }
        public short Status { get; set; }

        public virtual ICollection<LoginHistory> LoginHistories { get; set; }
        public virtual ICollection<UserRole> UserRoles { get; set; }
        public virtual ICollection<UserWhiteLabel> UserWhiteLabels { get; set; }

        #region Save Method

        public bool Add()
        {
            cashmarket_adminContext context = new cashmarket_adminContext();
            //if (context.Users.Any(a => a.Id.Equals(this.Id) && a.UserName.ToLower().Equals(this.UserName.ToLower()))) return true;
            CreateTime = DateTime.Now;
            context.Users.Add(this);
            return context.SaveChanges() > 0;
        }

        public bool Update()
        {
            cashmarket_adminContext context = new cashmarket_adminContext();
            var item = context.Users.Find(this.Id);
            if (item == null) return false;
            var properties = this.GetType().GetProperties();
            foreach (var property in properties)
            {
                if (!property.GetGetMethod().IsVirtual)
                {
                    property.SetValue(item, property.GetValue(this));
                }
            }
            item.UpdateTime = DateTime.Now;
            return context.SaveChanges() > 0;
        }

        public bool Remove()
        {
            cashmarket_adminContext context = new cashmarket_adminContext();
            var item = context.Users.Find(this.Id);
            if (!context.Users.Any(a => a.Id.Equals(this.Id) && a.UserName.ToLower().Equals(this.UserName.ToLower()))) return true;
            context.Users.Remove(item);
            return context.SaveChanges() > 0;
        }

        #endregion

        public new static User LogIn(string userName, string passWord, string ipAddress, out string session)
        {
            session = string.Empty;
            if (string.IsNullOrEmpty(userName)) return null;
            cashmarket_adminContext context = new cashmarket_adminContext();
            User user = context.Users.Include(a => a.UserRoles).ThenInclude(a => a.Role)
                .FirstOrDefault(a => a.UserName.ToLower().Equals(userName.Trim().ToLower().Trim()));
            if (user == null) return null;

            string hashedPassword = HashMethod.HmacSha256Digest(passWord, user.Salt);
            if (user.Status != (int)Enums.PersonStatus.Activated) return user;
            if (!hashedPassword.Equals(user.PasswordHash))
            {
                user.WrongCount += 1;
                if (user.WrongCount >= 3) user.Status = (int)Enums.PersonStatus.Blocked;
            }
            else
            {
                user.WrongCount = 0;
                session = Guid.NewGuid().ToString();
                LoginHistory history = new LoginHistory()
                {
                    User = user,
                    UserId = user.Id,
                    UserName = userName,
                    SessionCode = session,
                    LoginTime = DateTime.Now,
                    IpAddress = ipAddress
                };
                context.LoginHistories.Add(history);
            }
            context.SaveChanges();
            return user;
        }

        public new static bool LogOut(string userName)
        {
            if (string.IsNullOrEmpty(userName)) return false;
            cashmarket_adminContext context = new cashmarket_adminContext();
            var userSession = context.LoginHistories.OrderByDescending(a => a.LoginTime)
                .FirstOrDefault(a => a.User.UserName.ToLower().Equals(userName.Trim().ToLower()));
            if (userSession == null) return false;
            if (userSession.LogoutTime != null) return true;
            userSession.LogoutTime = DateTime.Now;
            return context.SaveChanges() > 0;
        }

        public static User GetById(int id)
        {
            var context = new cashmarket_adminContext();
            return context.Users.Find(id);
        }

        public static IEnumerable<User> GetUsers(int page = 1, int size = 20, string sort = null, string keyword = null, PersonStatus? status = null)
        {
            cashmarket_adminContext context = new cashmarket_adminContext();
            IQueryable<User> query = context.Users;

            if (!string.IsNullOrEmpty(keyword))
            {
                query = query.Where(a => a.UserName.Contains(keyword)
                                         || a.EmailAddress.Contains(keyword)
                                         || a.FullName.ToLower().Contains(keyword));
            }

            if (status.HasValue) query = query.Where(a => a.Status == (short)status);

            if (!string.IsNullOrEmpty(sort))
            {
                switch (sort.ToLower())
                {
                    case "username":
                        query = query.OrderBy(a => a.UserName);
                        break;
                    case "username_desc":
                        query = query.OrderByDescending(a => a.UserName);
                        break;
                    case "fullname":
                        query = query.OrderBy(a => a.FullName);
                        break;
                    case "fullname_desc":
                        query = query.OrderByDescending(a => a.FullName);
                        break;
                    case "status":
                        query = query.OrderBy(a => a.Status);
                        break;
                    case "status_desc":
                        query = query.OrderByDescending(a => a.Status);
                        break;
                    default:
                        query = query.OrderBy(a => a.UserName);
                        break;
                }
            }

            return query.Include(a => a.UserRoles).ThenInclude(a => a.Role).ToPagedList(page, size < 1 ? int.MaxValue : size);
        }

        public new static bool ValidateUserSession(string userName, string sessionCode)
        {
            if (string.IsNullOrEmpty(userName)) return false;
            cashmarket_adminContext context = new cashmarket_adminContext();
            var userSession = context.LoginHistories.OrderByDescending(a => a.LoginTime)
                .FirstOrDefault(a => a.User.UserName.ToLower().Equals(userName.Trim().ToLower()));
            return userSession != null && userSession.SessionCode.Equals(sessionCode) && userSession.LogoutTime == null;
        }
    }
}
