﻿using CashMarket_Entities.Context;
using CashMarket_Entities.Enums;
using Microsoft.EntityFrameworkCore;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace CashMarket_Entities.Models.Admin
{
    public partial class cashmarket_adminContext : DbContext
    {
        public cashmarket_adminContext()
        {
            var connection = DbContextFactory.GetCashMarketConnectionString(CashMarketDb.Admin);
            base.Database.SetConnectionString(connection);
        }

        public cashmarket_adminContext(DbContextOptions<cashmarket_adminContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Announcement> Announcements { get; set; }
        public virtual DbSet<AnnouncementLanguage> AnnouncementLanguages { get; set; }
        public virtual DbSet<BankAccountBalance> BankAccountBalances { get; set; }
        public virtual DbSet<BankActionLog> BankActionLogs { get; set; }
        public virtual DbSet<Banner> Banners { get; set; }
        public virtual DbSet<BannerLanguage> BannerLanguages { get; set; }
        public virtual DbSet<LoginHistory> LoginHistories { get; set; }
        public virtual DbSet<MemberStatusTag> MemberStatusTags { get; set; }
        public virtual DbSet<Permission> Permissions { get; set; }
        public virtual DbSet<ProductCategory> ProductCategories { get; set; }
        public virtual DbSet<Promotion> Promotions { get; set; }
        public virtual DbSet<PromotionLanguage> PromotionLanguages { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<RolePermission> RolePermissions { get; set; }
        public virtual DbSet<StatusTag> StatusTags { get; set; }
        public virtual DbSet<StatusTagLanguage> StatusTagLanguages { get; set; }

        public virtual DbSet<TableLogs> TableLogs { get; set; }
        public virtual DbSet<Transaction> Transactions { get; set; }
        public virtual DbSet<TransactionRequest> TransactionRequests { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserRole> UserRoles { get; set; }
        public virtual DbSet<UserWhiteLabel> UserWhiteLabels { get; set; }
        public virtual DbSet<WhiteLabelBank> WhiteLabelBanks { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseNpgsql("Host=13.212.37.122;Database=cashmarket_admin;Username=postgres;Password=tristan123");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasPostgresExtension("uuid-ossp")
                .HasAnnotation("Relational:Collation", "C.UTF-8");

            modelBuilder.Entity<Announcement>(entity =>
            {
                entity.ToTable("announcements");

                entity.HasIndex(e => e.WhiteLabelCode, "fki_fk_wl_code_announcements");

                entity.HasIndex(e => new { e.WhiteLabelCode, e.AnnouncementCode }, "uix_code_announcements")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AnnouncementCode)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("announcement_code");

                entity.Property(e => e.AnnouncementName)
                    .HasMaxLength(250)
                    .HasColumnName("announcement_name");

                entity.Property(e => e.CreateBy)
                    .HasMaxLength(50)
                    .HasColumnName("create_by");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("create_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.EnableStatus).HasColumnName("enable_status");

                entity.Property(e => e.ItemSort).HasColumnName("item_sort");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.UpdateBy)
                    .HasMaxLength(50)
                    .HasColumnName("update_by");

                entity.Property(e => e.UpdateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("update_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.WhiteLabelCode)
                    .IsRequired()
                    .HasMaxLength(10)
                    .HasColumnName("white_label_code");
            });

            modelBuilder.Entity<AnnouncementLanguage>(entity =>
            {
                entity.ToTable("announcement_languages");

                entity.HasIndex(e => e.AnnouncementId, "fki_fk_announcement_announcement_languages");

                entity.HasIndex(e => e.LanguageCode, "fki_fk_language_code_announcement_languages");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ActionLink)
                    .HasMaxLength(1000)
                    .HasColumnName("action_link");

                entity.Property(e => e.AnnouncementId).HasColumnName("announcement_id");

                entity.Property(e => e.Contents)
                    .HasMaxLength(1000)
                    .HasColumnName("contents");

                entity.Property(e => e.CopyLanguage)
                    .HasMaxLength(1000)
                    .HasColumnName("copy_language");

                entity.Property(e => e.CreateBy)
                    .HasMaxLength(50)
                    .HasColumnName("create_by");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("create_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.LanguageCode)
                    .IsRequired()
                    .HasMaxLength(15)
                    .HasColumnName("language_code");

                entity.Property(e => e.PromotionDesc)
                    .HasMaxLength(100)
                    .HasColumnName("promotion_desc");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.UpdateBy)
                    .HasMaxLength(50)
                    .HasColumnName("update_by");

                entity.Property(e => e.UpdateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("update_time")
                    .HasDefaultValueSql("now()");

                entity.HasOne(d => d.Announcement)
                    .WithMany(p => p.AnnouncementLanguages)
                    .HasForeignKey(d => d.AnnouncementId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_announcement_announcement_languages");
            });

            modelBuilder.Entity<BankAccountBalance>(entity =>
            {
                entity.ToTable("bank_account_balance");

                entity.HasIndex(e => e.BankName, "uix_bank_name_bank_account_balance");

                entity.HasIndex(e => e.BankNumber, "uix_bank_number_bank_account_balance");

                entity.HasIndex(e => e.WhiteLabelCode, "uix_wlcode_bank_account_balance");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Balance)
                    .HasPrecision(22, 4)
                    .HasColumnName("balance");

                entity.Property(e => e.BankName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("bank_name");

                entity.Property(e => e.BankNumber)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("bank_number");

                entity.Property(e => e.CreateBy)
                    .HasMaxLength(50)
                    .HasColumnName("create_by");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("create_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.UpdateBy)
                    .HasMaxLength(50)
                    .HasColumnName("update_by");

                entity.Property(e => e.UpdateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("update_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.WhiteLabelCode)
                    .IsRequired()
                    .HasMaxLength(10)
                    .HasColumnName("white_label_code");
            });

            modelBuilder.Entity<BankActionLog>(entity =>
            {
                entity.ToTable("bank_action_logs");

                entity.HasIndex(e => e.ActionType, "uix_action_id_bank_action_logs");

                entity.HasIndex(e => e.BankNumber, "uix_bank_number_bank_action_logs");

                entity.HasIndex(e => e.WhiteLabelCode, "uix_wlcode_bank_action_logs");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("uuid_generate_v4()");

                entity.Property(e => e.ActionType)
                    .HasColumnName("action_type")
                    .HasComment("Debit Credit Flag");

                entity.Property(e => e.Amount)
                    .HasPrecision(22, 4)
                    .HasColumnName("amount");

                entity.Property(e => e.BankName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("bank_name");

                entity.Property(e => e.BankNumber)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("bank_number");

                entity.Property(e => e.CreateBy)
                    .HasMaxLength(50)
                    .HasColumnName("create_by");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("create_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.ReffData)
                    .HasColumnType("jsonb")
                    .HasColumnName("reff_data");

                entity.Property(e => e.ReffId).HasColumnName("reff_id");

                entity.Property(e => e.ReffType).HasColumnName("reff_type");

                entity.Property(e => e.WhiteLabelCode)
                    .IsRequired()
                    .HasMaxLength(10)
                    .HasColumnName("white_label_code");
            });

            modelBuilder.Entity<Banner>(entity =>
            {
                entity.ToTable("banners");

                entity.HasIndex(e => e.WhiteLabelCode, "fki_fk_wl_code_banners");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CreateBy)
                    .HasMaxLength(50)
                    .HasColumnName("create_by");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("create_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.EnableStatus).HasColumnName("enable_status");

                entity.Property(e => e.ItemSort).HasColumnName("item_sort");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.UpdateBy)
                    .HasMaxLength(50)
                    .HasColumnName("update_by");

                entity.Property(e => e.UpdateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("update_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.WhiteLabelCode)
                    .IsRequired()
                    .HasMaxLength(10)
                    .HasColumnName("white_label_code");
            });

            modelBuilder.Entity<BannerLanguage>(entity =>
            {
                entity.ToTable("banner_languages");

                entity.HasIndex(e => e.BannerId, "fki_fk_banner_banner_languages");

                entity.HasIndex(e => e.LanguageCode, "fki_fk_language_code_banner_languages");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ActionLink)
                    .HasMaxLength(1000)
                    .HasColumnName("action_link");

                entity.Property(e => e.BannerId).HasColumnName("banner_id");

                entity.Property(e => e.BannerName)
                    .HasMaxLength(100)
                    .HasColumnName("banner_name");

                entity.Property(e => e.Contents)
                    .HasMaxLength(1000)
                    .HasColumnName("contents");

                entity.Property(e => e.CopyLanguage)
                    .HasMaxLength(1000)
                    .HasColumnName("copy_language");

                entity.Property(e => e.CreateBy)
                    .HasMaxLength(50)
                    .HasColumnName("create_by");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("create_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.LanguageCode)
                    .IsRequired()
                    .HasMaxLength(15)
                    .HasColumnName("language_code");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.UpdateBy)
                    .HasMaxLength(50)
                    .HasColumnName("update_by");

                entity.Property(e => e.UpdateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("update_time")
                    .HasDefaultValueSql("now()");

                entity.HasOne(d => d.Banner)
                    .WithMany(p => p.BannerLanguages)
                    .HasForeignKey(d => d.BannerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_banner_banner_languages");
            });

            modelBuilder.Entity<LoginHistory>(entity =>
            {
                entity.ToTable("login_histories");

                entity.HasIndex(e => e.UserId, "fki_fk_user_login_histories");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.IpAddress)
                    .HasMaxLength(50)
                    .HasColumnName("ip_address");

                entity.Property(e => e.LoginTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("login_time");

                entity.Property(e => e.LogoutTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("logout_time");

                entity.Property(e => e.SessionCode)
                    .HasMaxLength(150)
                    .HasColumnName("session_code");

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("user_name");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.LoginHistories)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_user_login_histories");
            });

            modelBuilder.Entity<MemberStatusTag>(entity =>
            {
                entity.ToTable("member_status_tags");

                entity.HasIndex(e => e.PrefixUserName, "fki_fk_prefix_user_name_member_status_tags");

                entity.HasIndex(e => e.StatusTagId, "fki_fk_status_tag_id_member_status_tags");

                entity.HasIndex(e => e.WhiteLabelCode, "fki_fk_wlcode_member_status_tags");

                entity.HasIndex(e => new { e.WhiteLabelCode, e.UserName }, "idx_wlcode_uname_member_status_tags");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CreateBy)
                    .HasMaxLength(50)
                    .HasColumnName("create_by");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("create_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.PrefixUserName)
                    .IsRequired()
                    .HasMaxLength(65)
                    .HasColumnName("prefix_user_name");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.StatusTagId).HasColumnName("status_tag_id");

                entity.Property(e => e.UpdateBy)
                    .HasMaxLength(50)
                    .HasColumnName("update_by");

                entity.Property(e => e.UpdateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("update_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("user_name");

                entity.Property(e => e.WhiteLabelCode)
                    .IsRequired()
                    .HasMaxLength(10)
                    .HasColumnName("white_label_code");

                entity.HasOne(d => d.StatusTag)
                    .WithMany(p => p.MemberStatusTags)
                    .HasForeignKey(d => d.StatusTagId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_status_tag_id_member_status_tags");
            });

            modelBuilder.Entity<Permission>(entity =>
            {
                entity.ToTable("permissions");

                entity.HasIndex(e => e.PermissionCode, "uix_permission_code")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ActionName)
                    .HasMaxLength(100)
                    .HasColumnName("action_name");

                entity.Property(e => e.AppName)
                    .HasMaxLength(100)
                    .HasColumnName("app_name");

                entity.Property(e => e.ControllerName)
                    .HasMaxLength(100)
                    .HasColumnName("controller_name");

                entity.Property(e => e.CreateBy)
                    .HasMaxLength(50)
                    .HasColumnName("create_by");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("create_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.ItemSort).HasColumnName("item_sort");

                entity.Property(e => e.PermissionCode)
                    .IsRequired()
                    .HasMaxLength(25)
                    .HasColumnName("permission_code");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.UpdateBy)
                    .HasMaxLength(50)
                    .HasColumnName("update_by");

                entity.Property(e => e.UpdateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("update_time")
                    .HasDefaultValueSql("now()");
            });

            modelBuilder.Entity<ProductCategory>(entity =>
            {
                entity.ToTable("product_categories");

                entity.HasIndex(e => e.ProductCode, "fki_fk_product_code_product_categories");

                entity.HasIndex(e => e.ProviderCode, "fki_fk_provider_code_product_categories");

                entity.HasIndex(e => e.WhiteLabelCode, "fki_fk_wl_code_product_categories");

                entity.HasIndex(e => new { e.WhiteLabelCode, e.ProviderCode, e.ProductCode, e.CategoryCode }, "idx_category_code_product_categories");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CategoryCode)
                    .IsRequired()
                    .HasMaxLength(25)
                    .HasColumnName("category_code");

                entity.Property(e => e.CategoryName)
                    .HasMaxLength(100)
                    .HasColumnName("category_name");

                entity.Property(e => e.CreateBy)
                    .HasMaxLength(50)
                    .HasColumnName("create_by");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("create_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.ItemSort).HasColumnName("item_sort");

                entity.Property(e => e.ProductCode)
                    .IsRequired()
                    .HasMaxLength(15)
                    .HasColumnName("product_code");

                entity.Property(e => e.ProductName)
                    .HasMaxLength(50)
                    .HasColumnName("product_name");

                entity.Property(e => e.ProviderCode)
                    .IsRequired()
                    .HasMaxLength(25)
                    .HasColumnName("provider_code");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.UpdateBy)
                    .HasMaxLength(50)
                    .HasColumnName("update_by");

                entity.Property(e => e.UpdateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("update_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.WhiteLabelCode)
                    .IsRequired()
                    .HasMaxLength(10)
                    .HasColumnName("white_label_code");
            });

            modelBuilder.Entity<Promotion>(entity =>
            {
                entity.ToTable("promotions");

                entity.HasIndex(e => e.WhiteLabelCode, "fki_fk_wl_code_promotions");

                entity.HasIndex(e => new { e.WhiteLabelCode, e.PromotionCode }, "uix_procode_promotions")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CreateBy)
                    .HasMaxLength(50)
                    .HasColumnName("create_by");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("create_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.EnableStatus).HasColumnName("enable_status");

                entity.Property(e => e.ItemSort).HasColumnName("item_sort");

                entity.Property(e => e.PromotionCode)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("promotion_code");

                entity.Property(e => e.PromotionName)
                    .HasMaxLength(250)
                    .HasColumnName("promotion_name");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.UpdateBy)
                    .HasMaxLength(50)
                    .HasColumnName("update_by");

                entity.Property(e => e.UpdateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("update_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.WhiteLabelCode)
                    .IsRequired()
                    .HasMaxLength(10)
                    .HasColumnName("white_label_code");
            });

            modelBuilder.Entity<PromotionLanguage>(entity =>
            {
                entity.ToTable("promotion_languages");

                entity.HasIndex(e => e.LanguageCode, "fki_fk_language_code_promotion_languages");

                entity.HasIndex(e => e.PromotionId, "fki_fk_promotion_promotion_languages");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ActionLink)
                    .HasMaxLength(1000)
                    .HasColumnName("action_link");

                entity.Property(e => e.Contents)
                    .HasMaxLength(1000)
                    .HasColumnName("contents");

                entity.Property(e => e.CopyLanguage)
                    .HasMaxLength(1000)
                    .HasColumnName("copy_language");

                entity.Property(e => e.CreateBy)
                    .HasMaxLength(50)
                    .HasColumnName("create_by");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("create_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.LanguageCode)
                    .IsRequired()
                    .HasMaxLength(15)
                    .HasColumnName("language_code");

                entity.Property(e => e.PromotionDesc)
                    .HasMaxLength(100)
                    .HasColumnName("promotion_desc");

                entity.Property(e => e.PromotionId).HasColumnName("promotion_id");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.UpdateBy)
                    .HasMaxLength(50)
                    .HasColumnName("update_by");

                entity.Property(e => e.UpdateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("update_time")
                    .HasDefaultValueSql("now()");

                entity.HasOne(d => d.Promotion)
                    .WithMany(p => p.PromotionLanguages)
                    .HasForeignKey(d => d.PromotionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_promotion_promotion_languages");
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.ToTable("roles");

                entity.HasIndex(e => e.RoleCode, "uix_role_code")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CreateBy)
                    .HasMaxLength(50)
                    .HasColumnName("create_by");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("create_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.RoleCode)
                    .IsRequired()
                    .HasMaxLength(10)
                    .HasColumnName("role_code");

                entity.Property(e => e.RoleDesc)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("role_desc");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.UpdateBy)
                    .HasMaxLength(50)
                    .HasColumnName("update_by");

                entity.Property(e => e.UpdateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("update_time")
                    .HasDefaultValueSql("now()");
            });

            modelBuilder.Entity<RolePermission>(entity =>
            {
                entity.ToTable("role_permissions");

                entity.HasIndex(e => e.PermissionId, "fki_fk_permission_role_permissions");

                entity.HasIndex(e => e.RoleId, "fki_fk_role_role_permissions");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("nextval('roles_id_seq'::regclass)");

                entity.Property(e => e.CreateBy)
                    .HasMaxLength(50)
                    .HasColumnName("create_by");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("create_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.PermissionId).HasColumnName("permission_id");

                entity.Property(e => e.RoleId).HasColumnName("role_id");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.UpdateBy)
                    .HasMaxLength(50)
                    .HasColumnName("update_by");

                entity.Property(e => e.UpdateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("update_time")
                    .HasDefaultValueSql("now()");

                entity.HasOne(d => d.Permission)
                    .WithMany(p => p.RolePermissions)
                    .HasForeignKey(d => d.PermissionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_permission_role_permissions");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.RolePermissions)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_role_role_permissions");
            });

            modelBuilder.Entity<StatusTag>(entity =>
            {
                entity.ToTable("status_tags");

                entity.HasIndex(e => e.WhiteLabelCode, "fki_fk_wl_code_status_tags");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ColorCode)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("color_code");

                entity.Property(e => e.CreateBy)
                    .HasMaxLength(50)
                    .HasColumnName("create_by");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("create_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.TagCode)
                    .IsRequired()
                    .HasMaxLength(25)
                    .HasColumnName("tag_code");

                entity.Property(e => e.TagName)
                    .HasMaxLength(250)
                    .HasColumnName("tag_name");

                entity.Property(e => e.UpdateBy)
                    .HasMaxLength(50)
                    .HasColumnName("update_by");

                entity.Property(e => e.UpdateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("update_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.WhiteLabelCode)
                    .IsRequired()
                    .HasMaxLength(10)
                    .HasColumnName("white_label_code");
            });

            modelBuilder.Entity<StatusTagLanguage>(entity =>
            {
                entity.ToTable("status_tag_languages");

                entity.HasIndex(e => e.LanguageCode, "fki_fk_language_code_status_tag_languages");

                entity.HasIndex(e => e.StatusTagId, "fki_fk_sttags_status_tag_languages");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CopyLanguage)
                    .HasMaxLength(1000)
                    .HasColumnName("copy_language");

                entity.Property(e => e.CreateBy)
                    .HasMaxLength(50)
                    .HasColumnName("create_by");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("create_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.Description)
                    .HasMaxLength(1000)
                    .HasColumnName("description");

                entity.Property(e => e.LanguageCode)
                    .IsRequired()
                    .HasMaxLength(15)
                    .HasColumnName("language_code");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.StatusName)
                    .HasMaxLength(100)
                    .HasColumnName("status_name");

                entity.Property(e => e.StatusTagId).HasColumnName("status_tag_id");

                entity.Property(e => e.UpdateBy)
                    .HasMaxLength(50)
                    .HasColumnName("update_by");

                entity.Property(e => e.UpdateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("update_time")
                    .HasDefaultValueSql("now()");

                entity.HasOne(d => d.StatusTag)
                    .WithMany(p => p.StatusTagLanguages)
                    .HasForeignKey(d => d.StatusTagId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_sttags_status_tag_languages");
            });

            modelBuilder.Entity<TableLogs>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("table_logs");

                entity.HasIndex(e => new { e.TableName, e.ActionName, e.Period }, "table_logs_table_name_action_name__period_idx")
                    .HasNullSortOrder(new[] { NullSortOrder.NullsLast, NullSortOrder.NullsLast, NullSortOrder.NullsLast })
                    .HasSortOrder(new[] { SortOrder.Ascending, SortOrder.Ascending, SortOrder.Descending });

                entity.HasIndex(e => e.Period, "table_logs__period_idx")
                    .HasNullSortOrder(new[] { NullSortOrder.NullsLast })
                    .HasSortOrder(new[] { SortOrder.Descending });

                entity.HasIndex(e => e.TableName, "table_logs_table_name_idx");

                entity.Property(e => e.ActionName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("action_name");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("create_time");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.NewData)
                    .HasColumnType("jsonb")
                    .HasColumnName("new_data");

                entity.Property(e => e.OldData)
                    .HasColumnType("jsonb")
                    .HasColumnName("old_data");

                entity.Property(e => e.Period).HasColumnName("_period");

                entity.Property(e => e.TableName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("table_name");
            });

            modelBuilder.Entity<Transaction>(entity =>
            {
                entity.ToTable("transactions");

                entity.HasIndex(e => e.TransactionRequestId, "fki_fk_tr_transactions");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Amount)
                    .HasPrecision(22, 4)
                    .HasColumnName("amount");

                entity.Property(e => e.CreateBy)
                    .HasMaxLength(50)
                    .HasColumnName("create_by");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("create_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.EndBalance)
                    .HasPrecision(22, 3)
                    .HasColumnName("end_balance");

                entity.Property(e => e.Remark)
                    .HasMaxLength(250)
                    .HasColumnName("remark");

                entity.Property(e => e.StartBalance)
                    .HasPrecision(22, 3)
                    .HasColumnName("start_balance");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.TransactionRequestId).HasColumnName("transaction_request_id");

                entity.Property(e => e.UpdateBy)
                    .HasMaxLength(50)
                    .HasColumnName("update_by");

                entity.Property(e => e.UpdateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("update_time")
                    .HasDefaultValueSql("now()");

                entity.HasOne(d => d.TransactionRequest)
                    .WithMany(p => p.Transactions)
                    .HasForeignKey(d => d.TransactionRequestId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_tr_transactions");
            });

            modelBuilder.Entity<TransactionRequest>(entity =>
            {
                entity.ToTable("transaction_request");

                entity.HasIndex(e => e.MemberUserName, "fki_fk_member_transaction_request");

                entity.HasIndex(e => e.WhiteLabelCode, "fki_fk_wl_transaction_request");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Amount)
                    .HasPrecision(22, 4)
                    .HasColumnName("amount");

                entity.Property(e => e.ApprovalStatus).HasColumnName("approval_status");

                entity.Property(e => e.CreateBy)
                    .HasMaxLength(50)
                    .HasColumnName("create_by");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("create_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.FileUrl)
                    .HasMaxLength(250)
                    .HasColumnName("file_url");

                entity.Property(e => e.MemberUserName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("member_user_name");

                entity.Property(e => e.Mime)
                    .HasMaxLength(50)
                    .HasColumnName("mime");

                entity.Property(e => e.PrefixUserName)
                    .IsRequired()
                    .HasMaxLength(65)
                    .HasColumnName("prefix_user_name");

                entity.Property(e => e.RecipientAccountHolderName)
                    .HasMaxLength(100)
                    .HasColumnName("recipient_account_holder_name");

                entity.Property(e => e.RecipientAccountNumber)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("recipient_account_number")
                    .HasComment("Required for WD request");

                entity.Property(e => e.RequestData)
                    .HasColumnType("jsonb")
                    .HasColumnName("request_data");

                entity.Property(e => e.RequestType)
                    .HasColumnName("request_type")
                    .HasComment("Enum Type request");

                entity.Property(e => e.SenderAccountHolderName)
                    .HasMaxLength(100)
                    .HasColumnName("sender_account_holder_name")
                    .HasComment("Required when Request type Depo");

                entity.Property(e => e.SenderAccountNumber)
                    .HasMaxLength(100)
                    .HasColumnName("sender_account_number")
                    .HasComment("Required when Request type Depo");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.UpdateBy)
                    .HasMaxLength(50)
                    .HasColumnName("update_by");

                entity.Property(e => e.UpdateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("update_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.UrlPath)
                    .HasMaxLength(250)
                    .HasColumnName("url_path");

                entity.Property(e => e.WhiteLabelCode)
                    .IsRequired()
                    .HasMaxLength(10)
                    .HasColumnName("white_label_code");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("users");

                entity.HasIndex(e => e.UserName, "uix_user_name")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CreateBy)
                    .HasMaxLength(50)
                    .HasColumnName("create_by");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("create_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.EmailAddress)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("email_address");

                entity.Property(e => e.FirstName)
                    .HasMaxLength(50)
                    .HasColumnName("first_name");

                entity.Property(e => e.FullName)
                    .HasMaxLength(100)
                    .HasColumnName("full_name");

                entity.Property(e => e.LastName)
                    .HasMaxLength(50)
                    .HasColumnName("last_name");

                entity.Property(e => e.PasswordHash)
                    .IsRequired()
                    .HasMaxLength(150)
                    .HasColumnName("password_hash");

                entity.Property(e => e.Salt)
                    .IsRequired()
                    .HasMaxLength(150)
                    .HasColumnName("salt");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.UpdateBy)
                    .HasMaxLength(50)
                    .HasColumnName("update_by");

                entity.Property(e => e.UpdateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("update_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("user_name");

                entity.Property(e => e.Verified).HasColumnName("verified");

                entity.Property(e => e.VerifiedTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("verified_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.WrongCount).HasColumnName("wrong_count");
            });

            modelBuilder.Entity<UserRole>(entity =>
            {
                entity.ToTable("user_roles");

                entity.HasIndex(e => e.RoleId, "fki_fk_role_user_roles");

                entity.HasIndex(e => e.UserId, "fki_fk_user_user_roles");

                entity.HasIndex(e => new { e.UserId, e.RoleId }, "uix_user_roles")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CreateBy)
                    .HasMaxLength(50)
                    .HasColumnName("create_by");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("create_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.RoleId).HasColumnName("role_id");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.UpdateBy)
                    .HasMaxLength(50)
                    .HasColumnName("update_by");

                entity.Property(e => e.UpdateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("update_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.UserRoles)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_role_user_roles");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserRoles)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_user_user_roles");
            });

            modelBuilder.Entity<UserWhiteLabel>(entity =>
            {
                entity.ToTable("user_white_labels");

                entity.HasIndex(e => e.UserId, "fki_fk_user_user_white_labels");

                entity.HasIndex(e => e.WhiteLabelCode, "fki_fk_wl_user_white_labels");

                entity.HasIndex(e => new { e.UserId, e.WhiteLabelCode }, "uix_user_white_labels")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CreateBy)
                    .HasMaxLength(50)
                    .HasColumnName("create_by");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("create_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.UpdateBy)
                    .HasMaxLength(50)
                    .HasColumnName("update_by");

                entity.Property(e => e.UpdateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("update_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.Property(e => e.WhiteLabelCode)
                    .IsRequired()
                    .HasMaxLength(10)
                    .HasColumnName("white_label_code");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserWhiteLabels)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_user_user_white_labels");
            });

            modelBuilder.Entity<WhiteLabelBank>(entity =>
            {
                entity.ToTable("white_label_banks");

                entity.HasIndex(e => e.WhiteLabelId, "fki_fk_wl_white_label_banks");

                entity.HasIndex(e => new { e.WhiteLabelId, e.BankTypeCode, e.BankNumber }, "uix_white_label_banks")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.BankAccountName)
                    .HasMaxLength(100)
                    .HasColumnName("bank_account_name");

                entity.Property(e => e.BankName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("bank_name");

                entity.Property(e => e.BankNumber)
                    .IsRequired()
                    .HasMaxLength(100)
                    .HasColumnName("bank_number");

                entity.Property(e => e.BankSerial)
                    .HasMaxLength(100)
                    .HasColumnName("bank_serial");

                entity.Property(e => e.BankTypeCode)
                    .IsRequired()
                    .HasMaxLength(10)
                    .HasColumnName("bank_type_code");

                entity.Property(e => e.CreateBy)
                    .HasMaxLength(50)
                    .HasColumnName("create_by");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("create_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.Deposit).HasColumnName("deposit");

                entity.Property(e => e.MaxDeposit)
                    .HasPrecision(22, 4)
                    .HasColumnName("max_deposit");

                entity.Property(e => e.MaxWithdraw)
                    .HasPrecision(22, 4)
                    .HasColumnName("max_withdraw");

                entity.Property(e => e.MinDeposit)
                    .HasPrecision(22, 4)
                    .HasColumnName("min_deposit");

                entity.Property(e => e.MinWithdraw)
                    .HasPrecision(22, 4)
                    .HasColumnName("min_withdraw");

                entity.Property(e => e.SortNumber).HasColumnName("sort_number");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.UpdateBy)
                    .HasMaxLength(50)
                    .HasColumnName("update_by");

                entity.Property(e => e.UpdateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("update_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.WhiteLabelId).HasColumnName("white_label_id");

                entity.Property(e => e.Withdraw).HasColumnName("withdraw");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
