﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CashMarket_Entities.Models.Admin
{
    public partial class Announcement
    {
        public Announcement()
        {
            AnnouncementLanguages = new HashSet<AnnouncementLanguage>();
        }

        public int Id { get; set; }
        public string WhiteLabelCode { get; set; }
        public string AnnouncementCode { get; set; }
        public string AnnouncementName { get; set; }
        public int? ItemSort { get; set; }
        public bool? EnableStatus { get; set; }
        public DateTime? CreateTime { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateTime { get; set; }
        public string UpdateBy { get; set; }
        public short Status { get; set; }

        public virtual ICollection<AnnouncementLanguage> AnnouncementLanguages { get; set; }
    }
}
