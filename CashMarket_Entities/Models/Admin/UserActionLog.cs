﻿using System;

#nullable disable

namespace CashMarket_Entities.Models.Admin
{
    public partial class UserActionLog
    {
        public Guid Id { get; set; }
        public string TableName { get; set; }
        public string ActionName { get; set; }
        public string OldData { get; set; }
        public string NewData { get; set; }
        public DateTime? CreateTime { get; set; }
        public string WhiteLabelCode { get; set; }
    }
}
