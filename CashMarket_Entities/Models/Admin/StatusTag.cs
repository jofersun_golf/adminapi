﻿using System;
using System.Collections.Generic;
using System.Linq;
using X.PagedList;

#nullable disable

namespace CashMarket_Entities.Models.Admin
{
    public partial class StatusTag
    {
        public StatusTag()
        {
            MemberStatusTags = new HashSet<MemberStatusTag>();
            StatusTagLanguages = new HashSet<StatusTagLanguage>();
        }

        public int Id { get; set; }
        public string WhiteLabelCode { get; set; }
        public string ColorCode { get; set; }
        public string TagCode { get; set; }
        public string TagName { get; set; }
        public DateTime? CreateTime { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateTime { get; set; }
        public string UpdateBy { get; set; }
        public short Status { get; set; }

        public virtual ICollection<MemberStatusTag> MemberStatusTags { get; set; }
        public virtual ICollection<StatusTagLanguage> StatusTagLanguages { get; set; }

        public static IEnumerable<StatusTag> GetStatusTags(int page = 1, int size = 20, string sort = null, string keyword = null)
        {
            cashmarket_adminContext context = new cashmarket_adminContext();
            IQueryable<StatusTag> query = context.StatusTags;

            if (!string.IsNullOrEmpty(keyword))
            {
                query = query.Where(a => a.TagName.Contains(keyword)
                                         || a.TagCode.Contains(keyword)
                                         || a.ColorCode.ToLower().Contains(keyword));
            }


            if (!string.IsNullOrEmpty(sort))
            {
                switch (sort.ToLower())
                {
                    case "tagname":
                        query = query.OrderBy(a => a.TagName);
                        break;
                    case "tagname_desc":
                        query = query.OrderByDescending(a => a.TagName);
                        break;
                    case "status":
                        query = query.OrderBy(a => a.Status);
                        break;
                    case "status_desc":
                        query = query.OrderByDescending(a => a.Status);
                        break;
                    default:
                        query = query.OrderBy(a => a.TagName);
                        break;
                }
            }

            return query.ToPagedList(page, size < 1 ? int.MaxValue : size);
        }

        public static IEnumerable<StatusTag> GetMemberStatusTags(string wlCode, string userName)
        {
            cashmarket_adminContext context = new cashmarket_adminContext();
            IQueryable<StatusTag> query = context.StatusTags.AsQueryable();
            query = query.Where(a => a.MemberStatusTags.Any(a => a.WhiteLabelCode.ToLower().Equals(wlCode) && a.UserName.ToLower().Equals(userName)));
            //query = query.Where(a => a.WhiteLabelCode.ToLower().Equals(wlCode) && a.UserName.ToLower().Equals(userName));
            return query.ToList();
        }

        public static StatusTag GetById(int id)
        {
            var context = new cashmarket_adminContext();
            return context.StatusTags.Find(id);
        }

        #region Save Method

        public bool Add()
        {
            cashmarket_adminContext context = new cashmarket_adminContext();
            CreateTime = DateTime.Now;
            context.StatusTags.Add(this);
            return context.SaveChanges() > 0;
        }

        public bool Update()
        {
            cashmarket_adminContext context = new cashmarket_adminContext();
            var item = context.StatusTags.Find(this.Id);
            if (item == null) return false;
            var properties = this.GetType().GetProperties();
            foreach (var property in properties)
            {
                if (!property.GetGetMethod().IsVirtual)
                {
                    property.SetValue(item, property.GetValue(this));
                }
            }
            item.UpdateTime = DateTime.Now;
            return context.SaveChanges() > 0;
        }

        public bool Remove()
        {
            cashmarket_adminContext context = new cashmarket_adminContext();
            var item = context.StatusTags.Find(this.Id);
            if (!context.StatusTags.Any(a => a.Id.Equals(this.Id) && a.ColorCode.ToLower().Equals(this.ColorCode.ToLower()))) return true;
            context.StatusTags.Remove(item);
            return context.SaveChanges() > 0;
        }

        #endregion

    }
}
