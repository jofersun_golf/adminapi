﻿using System;
using System.Collections.Generic;
using System.Linq;
using X.PagedList;

#nullable disable

namespace CashMarket_Entities.Models.Admin
{
    public partial class TableLogs
    {
        public Guid Id { get; set; }
        public string TableName { get; set; }
        public string ActionName { get; set; }
        public string OldData { get; set; }
        public string NewData { get; set; }
        public DateTime? CreateTime { get; set; }
        public int Period { get; set; }

        public static IEnumerable<TableLogs> GetTableLogs(int page = 1, int size = 20, string sort = "", string tableName = "", DateTime? dateFrom = null, DateTime? dateTo = null)
        {
            var context = new cashmarket_adminContext();
            IQueryable<TableLogs> query = context.TableLogs;
            
            if (!string.IsNullOrEmpty(tableName)) query = query.Where(a => a.TableName.ToLower().Equals(tableName.ToLower()));
            if (dateFrom.HasValue) query = query.Where(a => a.CreateTime != null && a.CreateTime >= dateFrom.Value.Date);
            if (dateTo.HasValue) query = query.Where(a => a.CreateTime != null && a.CreateTime < dateTo.Value.Date.AddDays(1));

            if (!string.IsNullOrEmpty(sort))
            {
                switch (sort.ToLower())
                {
                    case "tablename":
                        query = query.OrderBy(a => a.TableName);
                        break;
                    case "tablename_desc":
                        query = query.OrderByDescending(a => a.TableName);
                        break;
                    case "actionname":
                        query = query.OrderBy(a => a.ActionName);
                        break;
                    case "actionname_desc":
                        query = query.OrderByDescending(a => a.ActionName);
                        break;
                    case "createtime":
                        query = query.OrderBy(a => a.CreateTime);
                        break;
                    case "createtime_desc":
                        query = query.OrderByDescending(a => a.CreateTime);
                        break;
                    default:
                        query = query.OrderByDescending(a => a.CreateTime);
                        break;
                }
            }
            else query = query.OrderByDescending(a => a.CreateTime);

            return query.ToPagedList(page, size == 0 ? int.MaxValue : size);
        }

    }
}
