﻿using System;

#nullable disable

namespace CashMarket_Entities.Models.Admin
{
    public partial class BankActionLog
    {
        public Guid Id { get; set; }
        public string WhiteLabelCode { get; set; }
        public string BankNumber { get; set; }
        public short ActionType { get; set; }
        public short ReffType { get; set; }
        public long? ReffId { get; set; }
        public string ReffData { get; set; }
        public decimal Amount { get; set; }
        public DateTime? CreateTime { get; set; }
        public string CreateBy { get; set; }
        public string BankName { get; set; }
    }
}
