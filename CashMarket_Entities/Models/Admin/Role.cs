﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using X.PagedList;

#nullable disable

namespace CashMarket_Entities.Models.Admin
{
    public partial class Role
    {
        public Role()
        {
            RolePermissions = new HashSet<RolePermission>();
            UserRoles = new HashSet<UserRole>();
        }

        public int Id { get; set; }
        public string RoleCode { get; set; }
        public string RoleDesc { get; set; }
        public DateTime? CreateTime { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateTime { get; set; }
        public string UpdateBy { get; set; }
        public short Status { get; set; }

        public virtual ICollection<RolePermission> RolePermissions { get; set; }
        public virtual ICollection<UserRole> UserRoles { get; set; }

        public static IEnumerable<Role> GetRoles(int page = 1, int size = 20, string sort = "", string keyword = "", short? status = null, string roleId = "")
        {
            cashmarket_adminContext context = new cashmarket_adminContext();
            IQueryable<Role> query = context.Roles;

            if (!string.IsNullOrEmpty(keyword))
            {
                query = query.Where(a => a.RoleCode.Contains(keyword)
                                         || a.RoleDesc.Contains(keyword));
            }

            if (status.HasValue)
            {
                query = query.Where(u => u.Status == (int)status);
            }

            if (int.TryParse(roleId, out var id))
            {
                query = query.Where(u => u.Id != id);
            }

            if (!string.IsNullOrEmpty(sort))
            {
                switch (sort.ToLower())
                {
                    case "rolecode":
                        query = query.OrderBy(a => a.RoleCode);
                        break;
                    case "rolecode_desc":
                        query = query.OrderByDescending(a => a.RoleCode);
                        break;
                    case "roledesc":
                        query = query.OrderBy(a => a.RoleDesc);
                        break;
                    case "roledesc_desc":
                        query = query.OrderByDescending(a => a.RoleDesc);
                        break;
                    case "status":
                        query = query.OrderBy(a => a.Status);
                        break;
                    case "status_desc":
                        query = query.OrderByDescending(a => a.Status);
                        break;
                    default:
                        query = query.OrderBy(a => a.RoleCode);
                        break;
                }
            }

            return query.Include(a => a.UserRoles).ThenInclude(a => a.Role).ToPagedList(page, size == 0 ? int.MaxValue : size);
        }
    }
}
