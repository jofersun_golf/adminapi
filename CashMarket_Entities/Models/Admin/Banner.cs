﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CashMarket_Entities.Models.Admin
{
    public partial class Banner
    {
        public Banner()
        {
            BannerLanguages = new HashSet<BannerLanguage>();
        }

        public int Id { get; set; }
        public string WhiteLabelCode { get; set; }
        public int? ItemSort { get; set; }
        public bool? EnableStatus { get; set; }
        public DateTime? CreateTime { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateTime { get; set; }
        public string UpdateBy { get; set; }
        public short Status { get; set; }

        public virtual ICollection<BannerLanguage> BannerLanguages { get; set; }
    }
}
