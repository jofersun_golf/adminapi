﻿using System;
using System.Collections.Generic;
using System.Linq;
using X.PagedList;

#nullable disable

namespace CashMarket_Entities.Models.Admin
{
    public partial class BankAccountBalance
    {
        public int Id { get; set; }
        public string WhiteLabelCode { get; set; }
        public string BankName { get; set; }
        public string BankNumber { get; set; }
        public decimal Balance { get; set; }
        public DateTime? CreateTime { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateTime { get; set; }
        public string UpdateBy { get; set; }
        public short Status { get; set; }

        #region Save Method

        public bool Add()
        {
            cashmarket_adminContext context = new cashmarket_adminContext();
            CreateTime = DateTime.Now;
            context.BankAccountBalances.Add(this);
            return context.SaveChanges() > 0;
        }

        public bool Update()
        {
            cashmarket_adminContext context = new cashmarket_adminContext();
            var item = context.BankAccountBalances.Find(this.Id);
            if (item == null) return false;
            var properties = this.GetType().GetProperties();
            foreach (var property in properties)
            {
                if (!property.GetGetMethod().IsVirtual)
                {
                    property.SetValue(item, property.GetValue(this));
                }
            }
            item.UpdateTime = DateTime.Now;
            return context.SaveChanges() > 0;
        }

        public bool Remove()
        {
            cashmarket_adminContext context = new cashmarket_adminContext();
            var item = context.BankAccountBalances.Find(this.Id);
            if (!context.BankAccountBalances.Any(a => a.Id.Equals(this.Id) && a.BankNumber.ToLower().Equals(this.BankNumber.ToLower()))) return true;
            context.BankAccountBalances.Remove(item);
            return context.SaveChanges() > 0;
        }

        #endregion

        public static BankAccountBalance GetById(int id)
        {
            var context = new cashmarket_adminContext();
            return context.BankAccountBalances.Find(id);
        }

        public static IEnumerable<BankAccountBalance> GetBankAccounts(int page = 1, int size = 20, string sort = "", string keyword = "", short? status = null)
        {
            cashmarket_adminContext context = new cashmarket_adminContext();
            IQueryable<BankAccountBalance> query = context.BankAccountBalances;

            if (!string.IsNullOrEmpty(keyword))
            {
                query = query.Where(a => a.BankName.Contains(keyword)
                                         || a.BankNumber.Contains(keyword));
            }

            if (status.HasValue)
            {
                query = query.Where(a => a.Status == status.Value);
            }

            if (!string.IsNullOrEmpty(sort))
            {
                switch (sort.ToLower())
                {
                    case "bankname":
                        query = query.OrderBy(a => a.BankName);
                        break;
                    case "bankname_desc":
                        query = query.OrderByDescending(a => a.BankName);
                        break;
                    case "banknumber":
                        query = query.OrderBy(a => a.BankNumber);
                        break;
                    case "banknumber_desc":
                        query = query.OrderByDescending(a => a.BankNumber);
                        break;
                    case "status":
                        query = query.OrderBy(a => a.Status);
                        break;
                    case "status_desc":
                        query = query.OrderByDescending(a => a.Status);
                        break;
                    default:
                        query = query.OrderBy(a => a.BankName);
                        break;
                }
            }

            return query.ToPagedList(page, size == 0 ? int.MaxValue : size);
        }
    }
}
