﻿using System;

#nullable disable

namespace CashMarket_Entities.Models.Admin
{
    public partial class LoginHistory
    {
        public long Id { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
        public DateTime? LoginTime { get; set; }
        public DateTime? LogoutTime { get; set; }
        public string SessionCode { get; set; }
        public string IpAddress { get; set; }

        public virtual User User { get; set; }
    }
}
