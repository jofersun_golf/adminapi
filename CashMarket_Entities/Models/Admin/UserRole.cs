﻿using System;

#nullable disable

namespace CashMarket_Entities.Models.Admin
{
    public partial class UserRole
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int RoleId { get; set; }
        public DateTime? CreateTime { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateTime { get; set; }
        public string UpdateBy { get; set; }
        public short Status { get; set; }

        public virtual Role Role { get; set; }
        public virtual User User { get; set; }
    }
}
