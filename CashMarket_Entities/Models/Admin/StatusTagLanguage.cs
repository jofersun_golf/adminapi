﻿using System;

#nullable disable

namespace CashMarket_Entities.Models.Admin
{
    public partial class StatusTagLanguage
    {
        public int Id { get; set; }
        public int StatusTagId { get; set; }
        public string LanguageCode { get; set; }
        public string StatusName { get; set; }
        public string Description { get; set; }
        public string CopyLanguage { get; set; }
        public DateTime? CreateTime { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateTime { get; set; }
        public string UpdateBy { get; set; }
        public short Status { get; set; }

        public virtual StatusTag StatusTag { get; set; }
    }
}
