﻿using System;

#nullable disable

namespace CashMarket_Entities.Models.Admin
{
    public partial class BannerLanguage
    {
        public int Id { get; set; }
        public int BannerId { get; set; }
        public string LanguageCode { get; set; }
        public string Contents { get; set; }
        public string CopyLanguage { get; set; }
        public string BannerName { get; set; }
        public string ActionLink { get; set; }
        public DateTime? CreateTime { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateTime { get; set; }
        public string UpdateBy { get; set; }
        public short Status { get; set; }

        public virtual Banner Banner { get; set; }
    }
}
