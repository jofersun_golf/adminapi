﻿using System;

#nullable disable

namespace CashMarket_Entities.Models.Admin
{
    public partial class UserWhiteLabel
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string WhiteLabelCode { get; set; }
        public DateTime? CreateTime { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateTime { get; set; }
        public string UpdateBy { get; set; }
        public short Status { get; set; }

        public virtual User User { get; set; }
    }
}
