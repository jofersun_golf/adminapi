﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CashMarket_Entities.Models.Admin
{
    public partial class Promotion
    {
        public Promotion()
        {
            PromotionLanguages = new HashSet<PromotionLanguage>();
        }

        public int Id { get; set; }
        public string WhiteLabelCode { get; set; }
        public string PromotionCode { get; set; }
        public string PromotionName { get; set; }
        public int? ItemSort { get; set; }
        public bool? EnableStatus { get; set; }
        public DateTime? CreateTime { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateTime { get; set; }
        public string UpdateBy { get; set; }
        public short Status { get; set; }

        public virtual ICollection<PromotionLanguage> PromotionLanguages { get; set; }
    }
}
