﻿using System;
using System.Collections.Generic;
using System.Linq;
using CashMarket_Entities.Enums;
using Microsoft.EntityFrameworkCore;
using X.PagedList;

#nullable disable

namespace CashMarket_Entities.Models.Admin
{
    public partial class TransactionRequest
    {
        public TransactionRequest()
        {
            Transactions = new HashSet<Transaction>();
        }

        public long Id { get; set; }
        public string WhiteLabelCode { get; set; }
        public string MemberUserName { get; set; }
        public string PrefixUserName { get; set; }
        public decimal Amount { get; set; }
        public string RequestData { get; set; }
        public string FileUrl { get; set; }
        public string UrlPath { get; set; }
        public string Mime { get; set; }
        public short RequestType { get; set; }
        public DateTime? CreateTime { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateTime { get; set; }
        public string UpdateBy { get; set; }
        public short Status { get; set; }
        public short ApprovalStatus { get; set; }
        public string SenderAccountHolderName { get; set; }
        public string SenderAccountNumber { get; set; }
        public string RecipientAccountHolderName { get; set; }
        public string RecipientAccountNumber { get; set; }

        public virtual ICollection<Transaction> Transactions { get; set; }

        public static TransactionRequest GetById(long id)
        {
            var context = new cashmarket_adminContext();
            return context.TransactionRequests.Find(id);

        }

        public static IEnumerable<TransactionRequest> GetTransactionRequests(int page = 1, int size = 20, string sort = "", string[] wlCodes = null, string[] userNames = null,
            Request[] requests = null, Approval[] approvals = null, DateTime? dateFrom = null, DateTime? dateTo = null)
        {
            var context = new cashmarket_adminContext();
            IQueryable<TransactionRequest> query = context.TransactionRequests;

            if (wlCodes != null && wlCodes.Any())
            {
                var wlCodes2 = wlCodes.Select(a => a.ToLower()).ToList();
                query = query.Where(a => wlCodes2.Contains(a.WhiteLabelCode.ToLower()));
            }
            if (userNames != null && userNames.Any())
            {
                var userNames2 = userNames.Select(a => a.ToLower()).ToList();
                query = query.Where(a => userNames2.Contains(a.MemberUserName.ToLower()));
            }
            if (requests != null) query = query.Where(a => requests.Contains((Request)a.RequestType));
            if (approvals != null) query = query.Where(a => approvals.Contains((Approval)a.ApprovalStatus));
            if (dateFrom.HasValue) query = query.Where(a => a.CreateTime != null && a.CreateTime >= dateFrom.Value.Date);
            if (dateTo.HasValue) query = query.Where(a => a.CreateTime != null && a.CreateTime < dateTo.Value.Date.AddDays(1));

            if (!string.IsNullOrEmpty(sort))
            {
                switch (sort.ToLower())
                {
                    case "username":
                        query = query.OrderBy(a => a.MemberUserName);
                        break;
                    case "username_desc":
                        query = query.OrderByDescending(a => a.MemberUserName);
                        break;
                    case "date":
                        query = query.OrderBy(a => a.CreateBy);
                        break;
                    case "fullname_desc":
                        query = query.OrderByDescending(a => a.CreateBy);
                        break;
                    case "status":
                        query = query.OrderBy(a => a.Status);
                        break;
                    case "status_desc":
                        query = query.OrderByDescending(a => a.Status);
                        break;
                    default:
                        query = query.OrderByDescending(a => a.UpdateTime).ThenByDescending(a => a.CreateTime);
                        break;
                }
            }
            else query = query.OrderByDescending(a => a.UpdateTime).ThenByDescending(a => a.CreateTime);

            return query.Include(a => a.Transactions).ToPagedList(page, size == 0 ? int.MaxValue : size);
        }

        #region Save Method

        public bool Add()
        {
            cashmarket_adminContext context = new cashmarket_adminContext();
            //if (context.TransactionRequests.Any(a => a.Id.Equals(this.Id) && a.MemberUserName.ToLower().Equals(this.MemberUserName.ToLower()))) return true;
            CreateTime = DateTime.Now;
            context.TransactionRequests.Add(this);
            return context.SaveChanges() > 0;
        }

        public bool Update()
        {
            cashmarket_adminContext context = new cashmarket_adminContext();
            var item = context.TransactionRequests.Find(this.Id);
            if (item == null) return false;
            var properties = this.GetType().GetProperties();
            foreach (var property in properties)
            {
                if (!property.GetGetMethod().IsVirtual)
                {
                    property.SetValue(item, property.GetValue(this));
                }
            }
            item.UpdateTime = DateTime.Now;
            return context.SaveChanges() > 0;
        }

        public bool Remove()
        {
            cashmarket_adminContext context = new cashmarket_adminContext();
            var item = context.TransactionRequests.Find(this.Id);
            if (!context.TransactionRequests.Any(a => a.Id.Equals(this.Id) && a.MemberUserName.ToLower().Equals(this.MemberUserName.ToLower()))) return true;
            context.TransactionRequests.Remove(item);
            return context.SaveChanges() > 0;
        }

        #endregion

    }
}
