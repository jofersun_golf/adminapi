﻿using System;
using System.Collections.Generic;

#nullable disable

namespace CashMarket_Entities.Models.Admin
{
    public partial class Permission
    {
        public Permission()
        {
            RolePermissions = new HashSet<RolePermission>();
        }

        public int Id { get; set; }
        public string PermissionCode { get; set; }
        public string ActionName { get; set; }
        public string AppName { get; set; }
        public string ControllerName { get; set; }
        public int? ItemSort { get; set; }
        public DateTime? CreateTime { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateTime { get; set; }
        public string UpdateBy { get; set; }
        public short Status { get; set; }

        public virtual ICollection<RolePermission> RolePermissions { get; set; }
    }
}
