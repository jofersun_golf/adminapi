﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using X.PagedList;

#nullable disable

namespace CashMarket_Entities.Models.Admin
{
    public partial class MemberStatusTag
    {
        public int Id { get; set; }
        public string WhiteLabelCode { get; set; }
        public string PrefixUserName { get; set; }
        public string UserName { get; set; }
        public int StatusTagId { get; set; }
        public DateTime? CreateTime { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateTime { get; set; }
        public string UpdateBy { get; set; }
        public short Status { get; set; }

        public virtual StatusTag StatusTag { get; set; }

        public static MemberStatusTag GetById(int id)
        {
            var context = new cashmarket_adminContext();
            return context.MemberStatusTags.Find(id);
        }

        public static IEnumerable<MemberStatusTag> GetMemberStatusTags(int page = 1, int size = 20, string sort = "", string keyword = "")
        {
            cashmarket_adminContext context = new cashmarket_adminContext();
            IQueryable<MemberStatusTag> query = context.MemberStatusTags;

            if (!string.IsNullOrEmpty(keyword))
            {
                query = query.Where(a => a.WhiteLabelCode.Contains(keyword)
                                         || a.PrefixUserName.Contains(keyword)
                                         || a.UserName.ToLower().Contains(keyword));
            }

            if (!string.IsNullOrEmpty(sort))
            {
                switch (sort.ToLower())
                {
                    case "whitelabelcode":
                        query = query.OrderBy(a => a.WhiteLabelCode);
                        break;
                    case "whitelabelcode_desc":
                        query = query.OrderByDescending(a => a.WhiteLabelCode);
                        break;
                    case "prefixusername":
                        query = query.OrderBy(a => a.PrefixUserName);
                        break;
                    case "prefixusername_desc":
                        query = query.OrderByDescending(a => a.PrefixUserName);
                        break;
                    case "status":
                        query = query.OrderBy(a => a.Status);
                        break;
                    case "status_desc":
                        query = query.OrderByDescending(a => a.Status);
                        break;
                    default:
                        query = query.OrderBy(a => a.WhiteLabelCode);
                        break;
                }
            }

            return query.Include(a => a.StatusTag).ToPagedList(page, size == 0 ? int.MaxValue : size);
        }

        #region Save Method

        public bool Add()
        {
            cashmarket_adminContext context = new cashmarket_adminContext();
            CreateTime = DateTime.Now;
            context.MemberStatusTags.Add(this);
            return context.SaveChanges() > 0;
        }

        public bool Update()
        {
            cashmarket_adminContext context = new cashmarket_adminContext();
            var item = context.MemberStatusTags.Find(this.Id);
            if (item == null) return false;
            var properties = this.GetType().GetProperties();
            foreach (var property in properties)
            {
                if (!property.GetGetMethod().IsVirtual)
                {
                    property.SetValue(item, property.GetValue(this));
                }
            }
            item.UpdateTime = DateTime.Now;
            return context.SaveChanges() > 0;
        }

        public bool Remove()
        {
            cashmarket_adminContext context = new cashmarket_adminContext();
            var item = context.MemberStatusTags.Find(this.Id);
            if (!context.MemberStatusTags.Any(a => a.Id.Equals(this.Id) && a.UserName.ToLower().Equals(this.UserName.ToLower()))) return true;
            context.MemberStatusTags.Remove(item);
            return context.SaveChanges() > 0;
        }

        #endregion

    }
}
