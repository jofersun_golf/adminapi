﻿using System;

#nullable disable

namespace CashMarket_Entities.Models.Admin
{
    public partial class Transaction
    {
        public long Id { get; set; }
        public long TransactionRequestId { get; set; }
        public decimal Amount { get; set; }
        public decimal? StartBalance { get; set; }
        public decimal? EndBalance { get; set; }
        public DateTime? CreateTime { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateTime { get; set; }
        public string UpdateBy { get; set; }
        public short Status { get; set; }
        public string Remark { get; set; }

        public virtual TransactionRequest TransactionRequest { get; set; }
    }
}
