﻿using System;
using System.ComponentModel;
using System.Reflection;
using CashMarket_Entities.Enums;
using Newtonsoft.Json;

namespace CashMarket_Entities.Models.Base
{
    /// <summary>
    /// Basic output with response.
    /// </summary>
    public class BaseOutput
    {
        public Response Response { get; set; } = new Response();
    }

    /// <summary>
    /// Basic output with response and data.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class BaseOutput<T> : BaseOutput where T : new()
    {
        /// <summary>
        /// Data type of the response proses.
        /// </summary>
        public T Data { get; set; } = new T();
    }

    /// <summary>
    /// Default response for every process.
    /// </summary>
    public class Response
    {
        public Response()
        {
            ErrorCode = ErrorCodes.GeneralError;
        }

        private ErrorCodes _errorCode;
        
        [JsonProperty("error_code")]
        public ErrorCodes ErrorCode
        {
            get => _errorCode;
            set
            {
                _errorCode = value;
                ErrorMessage = GetDescription(value);
            }
        }

        [JsonProperty("message")]
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Get description from enum description text.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetDescription(Enum value)
        {
            if (value == null) return string.Empty;
            FieldInfo fi = value.GetType().GetField(value.ToString());
            DescriptionAttribute attribute = fi?.GetCustomAttribute<DescriptionAttribute>();
            return !string.IsNullOrEmpty(attribute?.Description) ? attribute.Description : value.ToString();
        }
    }
}
