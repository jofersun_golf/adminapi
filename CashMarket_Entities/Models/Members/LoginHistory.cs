﻿using System;

#nullable disable

namespace CashMarket_Entities.Models.Members
{
    public partial class LoginHistory
    {
        public long Id { get; set; }
        public int MemberId { get; set; }
        public string WhiteLabelCode { get; set; }
        public string UserName { get; set; }
        public string PrefixUserName { get; set; }
        public DateTime? LoginTime { get; set; }
        public DateTime? LogoutTime { get; set; }
        public string SessionCode { get; set; }
        public string IpAddress { get; set; }

        public virtual Member Member { get; set; }
    }
}
