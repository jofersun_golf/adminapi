﻿using System;
using System.Collections.Generic;
using System.Linq;

#nullable disable

namespace CashMarket_Entities.Models.Members
{
    public partial class MemberBank
    {
        public int Id { get; set; }
        public int MemberId { get; set; }
        public string CurrencyId { get; set; }
        public string BankName { get; set; }
        public string AccountName { get; set; }
        public string AccountNumber { get; set; }
        public bool PrefferedDeposit { get; set; }
        public bool PrefferedWithdrawal { get; set; }
        public DateTime? CreateTime { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateTime { get; set; }
        public string UpdateBy { get; set; }
        public short Status { get; set; }
        public short BankTypeId { get; set; }

        public virtual Member Member { get; set; }

        public static List<MemberBank> GetMemberBanks(int? memberId = null)
        {
            List<MemberBank> memberBanks = new List<MemberBank>();
            cashmarket_membersContext context = new cashmarket_membersContext();
            memberBanks.AddRange(memberId.HasValue
                ? context.MemberBanks.Where(a => a.MemberId.Equals(memberId))
                : context.MemberBanks);
            return memberBanks;
        }
    }
}
