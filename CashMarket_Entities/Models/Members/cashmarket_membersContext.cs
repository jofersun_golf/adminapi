﻿using CashMarket_Entities.Context;
using CashMarket_Entities.Enums;
using Microsoft.EntityFrameworkCore;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace CashMarket_Entities.Models.Members
{
    public partial class cashmarket_membersContext : DbContext
    {
        public cashmarket_membersContext()
        {
            var connection = DbContextFactory.GetCashMarketConnectionString(CashMarketDb.Members);
            base.Database.SetConnectionString(connection);
        }

        public cashmarket_membersContext(DbContextOptions<cashmarket_membersContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Member> Members { get; set; }
        public virtual DbSet<MemberBank> MemberBanks { get; set; }
        public virtual DbSet<MemberResetHistory> MemberResetHistories { get; set; }
        public virtual DbSet<MemberWallet> MemberWallets { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseNpgsql("Host=13.212.37.122;Database=cashmarket_members;Username=postgres;Password=tristan123");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasPostgresExtension("uuid-ossp")
                .HasAnnotation("Relational:Collation", "C.UTF-8");

            modelBuilder.Entity<Member>(entity =>
            {
                entity.ToTable("members");

                entity.HasIndex(e => e.CurrencyCode, "fki_fk_currencycode_members");

                entity.HasIndex(e => e.WhiteLabelCode, "fki_fk_white_label_members");

                entity.HasIndex(e => e.WhiteLabelCode, "idx_prefix_user_name_members");

                entity.HasIndex(e => new { e.WhiteLabelCode, e.Email }, "uix_email_prefix_members")
                    .IsUnique()
                    .HasFilter("verified");

                entity.HasIndex(e => new { e.WhiteLabelCode, e.UserName }, "uix_uname_prefix_members")
                    .IsUnique()
                    .HasFilter("verified");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Contact)
                    .HasMaxLength(100)
                    .HasColumnName("contact");

                entity.Property(e => e.Contact2)
                    .HasMaxLength(100)
                    .HasColumnName("contact2");

                entity.Property(e => e.CreateBy)
                    .HasMaxLength(50)
                    .HasColumnName("create_by");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("create_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.CurrencyCode)
                    .HasMaxLength(5)
                    .HasColumnName("currency_code");

                entity.Property(e => e.CustomIdentifier)
                    .HasMaxLength(450)
                    .HasColumnName("custom_identifier");

                entity.Property(e => e.DailyWithdrawalLimit).HasColumnName("daily_withdrawal_limit");

                entity.Property(e => e.DateOfBirth)
                    .HasColumnType("date")
                    .HasColumnName("date_of_birth");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(150)
                    .HasColumnName("email");

                entity.Property(e => e.EncryptedPassword)
                    .IsRequired()
                    .HasMaxLength(150)
                    .HasColumnName("encrypted_password");

                entity.Property(e => e.EncryptedPaymentPin)
                    .HasMaxLength(50)
                    .HasColumnName("encrypted_payment_pin");

                entity.Property(e => e.FullName)
                    .HasMaxLength(150)
                    .HasColumnName("full_name");

                entity.Property(e => e.Gender).HasColumnName("gender");

                entity.Property(e => e.IsNotValidWelcomeBonus).HasColumnName("is_not_valid_welcome_bonus");

                entity.Property(e => e.LastIp)
                    .HasMaxLength(50)
                    .HasColumnName("last_ip");

                entity.Property(e => e.LastLogin)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("last_login");

                entity.Property(e => e.LineUserId)
                    .HasMaxLength(100)
                    .HasColumnName("line_user_id");

                entity.Property(e => e.MemberStatus)
                    .HasColumnName("member_status")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.PreferredLanguageCode)
                    .HasMaxLength(10)
                    .HasColumnName("preferred_language_code");

                entity.Property(e => e.PreferredLanguageName)
                    .HasMaxLength(50)
                    .HasColumnName("preferred_language_name");

                entity.Property(e => e.PrefixUserName)
                    .IsRequired()
                    .HasMaxLength(65)
                    .HasColumnName("prefix_user_name");

                entity.Property(e => e.Referral)
                    .HasMaxLength(50)
                    .HasColumnName("referral");

                entity.Property(e => e.ReferralReference)
                    .HasMaxLength(50)
                    .HasColumnName("referral_reference");

                entity.Property(e => e.ReferralTypeId).HasColumnName("referral_type_id");

                entity.Property(e => e.RegisterIp)
                    .HasMaxLength(50)
                    .HasColumnName("register_ip");

                entity.Property(e => e.Remarks)
                    .HasMaxLength(150)
                    .HasColumnName("remarks");

                entity.Property(e => e.Salt)
                    .IsRequired()
                    .HasMaxLength(150)
                    .HasColumnName("salt");

                entity.Property(e => e.SessionCode)
                    .HasMaxLength(150)
                    .HasColumnName("session_code");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.UpdateBy)
                    .HasMaxLength(50)
                    .HasColumnName("update_by");

                entity.Property(e => e.UpdateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("update_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("user_name");

                entity.Property(e => e.ValidationExpiredDate)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("validation_expired_date");

                entity.Property(e => e.Verified).HasColumnName("verified");

                entity.Property(e => e.VerifiedAt)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("verified_at");

                entity.Property(e => e.WechatUserId)
                    .HasMaxLength(100)
                    .HasColumnName("wechat_user_id");

                entity.Property(e => e.WhiteLabelCode)
                    .IsRequired()
                    .HasMaxLength(10)
                    .HasColumnName("white_label_code");

                entity.Property(e => e.WrongCount).HasColumnName("wrong_count");
            });

            modelBuilder.Entity<MemberBank>(entity =>
            {
                entity.ToTable("member_banks");

                entity.HasIndex(e => e.CurrencyId, "fki_fk_currency_member_banks");

                entity.HasIndex(e => e.MemberId, "fki_fk_member_member_banks");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AccountName)
                    .IsRequired()
                    .HasMaxLength(150)
                    .HasColumnName("account_name");

                entity.Property(e => e.AccountNumber)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("account_number");

                entity.Property(e => e.BankName)
                    .IsRequired()
                    .HasMaxLength(150)
                    .HasColumnName("bank_name");

                entity.Property(e => e.BankTypeId).HasColumnName("bank_type_id");

                entity.Property(e => e.CreateBy)
                    .HasMaxLength(50)
                    .HasColumnName("create_by");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("create_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.CurrencyId)
                    .IsRequired()
                    .HasMaxLength(5)
                    .HasColumnName("currency_id");

                entity.Property(e => e.MemberId).HasColumnName("member_id");

                entity.Property(e => e.PrefferedDeposit).HasColumnName("preffered_deposit");

                entity.Property(e => e.PrefferedWithdrawal).HasColumnName("preffered_withdrawal");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.UpdateBy)
                    .HasMaxLength(50)
                    .HasColumnName("update_by");

                entity.Property(e => e.UpdateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("update_time")
                    .HasDefaultValueSql("now()");

                entity.HasOne(d => d.Member)
                    .WithMany(p => p.MemberBanks)
                    .HasForeignKey(d => d.MemberId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_member_member_banks");
            });

            modelBuilder.Entity<MemberResetHistory>(entity =>
            {
                entity.ToTable("member_reset_histories");

                entity.HasIndex(e => e.MemberId, "fki_fk_member_member_reset_histories");

                entity.HasIndex(e => e.PrefixUserName, "fki_prefix_user_name_member_reset_histories");

                entity.HasIndex(e => new { e.WhiteLabelCode, e.UserName }, "fki_wluser_member_reset_histories");

                entity.HasIndex(e => e.CreateTime, "idx_create_time_member_reset_histories")
                    .HasSortOrder(new[] { SortOrder.Descending });

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasDefaultValueSql("uuid_generate_v4()");

                entity.Property(e => e.CreateBy)
                    .HasMaxLength(50)
                    .HasColumnName("create_by");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("create_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.LinkAddress)
                    .IsRequired()
                    .HasMaxLength(250)
                    .HasColumnName("link_address");

                entity.Property(e => e.MemberId).HasColumnName("member_id");

                entity.Property(e => e.PrefixUserName)
                    .IsRequired()
                    .HasMaxLength(65)
                    .HasColumnName("prefix_user_name");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.StatusSent)
                    .HasColumnName("status_sent")
                    .HasDefaultValueSql("false");

                entity.Property(e => e.StatusUsed)
                    .HasColumnName("status_used")
                    .HasDefaultValueSql("false");

                entity.Property(e => e.UpdateBy)
                    .HasMaxLength(50)
                    .HasColumnName("update_by");

                entity.Property(e => e.UpdateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("update_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("user_name");

                entity.Property(e => e.WhiteLabelCode)
                    .IsRequired()
                    .HasMaxLength(10)
                    .HasColumnName("white_label_code");

                entity.HasOne(d => d.Member)
                    .WithMany(p => p.MemberResetHistories)
                    .HasForeignKey(d => d.MemberId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_member_member_reset_histories");
            });

            modelBuilder.Entity<MemberWallet>(entity =>
            {
                entity.ToTable("member_wallets");

                entity.HasIndex(e => e.MemberId, "fki_fk_member_member_wallets");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Balance)
                    .HasPrecision(22, 4)
                    .HasColumnName("balance");

                entity.Property(e => e.CreateBy)
                    .HasMaxLength(50)
                    .HasColumnName("create_by");

                entity.Property(e => e.CreateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("create_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.CurrencyCode)
                    .IsRequired()
                    .HasMaxLength(5)
                    .HasColumnName("currency_code");

                entity.Property(e => e.MemberId).HasColumnName("member_id");

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.UpdateBy)
                    .HasMaxLength(50)
                    .HasColumnName("update_by");

                entity.Property(e => e.UpdateTime)
                    .HasColumnType("timestamp(6) with time zone")
                    .HasColumnName("update_time")
                    .HasDefaultValueSql("now()");

                entity.Property(e => e.WalletType).HasColumnName("wallet_type");

                entity.HasOne(d => d.Member)
                    .WithMany(p => p.MemberWallets)
                    .HasForeignKey(d => d.MemberId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_member_member_wallets");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
