﻿using System;
using System.Collections.Generic;
using System.Linq;
using CashMarket_Entities.Enums;
using X.PagedList;

#nullable disable

namespace CashMarket_Entities.Models.Members
{
    public partial class Member
    {
        public Member()
        {
            MemberBanks = new HashSet<MemberBank>();
            MemberResetHistories = new HashSet<MemberResetHistory>();
            MemberWallets = new HashSet<MemberWallet>();
        }

        public int Id { get; set; }
        public string WhiteLabelCode { get; set; }
        public string PrefixUserName { get; set; }
        public string UserName { get; set; }
        public string EncryptedPassword { get; set; }
        public string Salt { get; set; }
        public string Email { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string FullName { get; set; }
        public short? Gender { get; set; }
        public string Contact { get; set; }
        public string Contact2 { get; set; }
        public string LastIp { get; set; }
        public DateTime? LastLogin { get; set; }
        public string RegisterIp { get; set; }
        public string Remarks { get; set; }
        public string SessionCode { get; set; }
        public bool Verified { get; set; }
        public DateTime? VerifiedAt { get; set; }
        public int DailyWithdrawalLimit { get; set; }
        public short WrongCount { get; set; }
        public string Referral { get; set; }
        public string CustomIdentifier { get; set; }
        public string WechatUserId { get; set; }
        public string LineUserId { get; set; }
        public bool IsNotValidWelcomeBonus { get; set; }
        public string EncryptedPaymentPin { get; set; }
        public string PreferredLanguageCode { get; set; }
        public string PreferredLanguageName { get; set; }
        public short MemberStatus { get; set; }
        public DateTime? CreateTime { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateTime { get; set; }
        public string UpdateBy { get; set; }
        public short Status { get; set; }
        public string CurrencyCode { get; set; }
        public string ReferralReference { get; set; }
        public short ReferralTypeId { get; set; }
        public DateTime ValidationExpiredDate { get; set; }

        public virtual ICollection<MemberBank> MemberBanks { get; set; }
        public virtual ICollection<MemberResetHistory> MemberResetHistories { get; set; }
        public virtual ICollection<MemberWallet> MemberWallets { get; set; }

        public static Member GetById(int id)
        {
            var context = new cashmarket_membersContext();
            return context.Members.Find(id);
        }

        public static Member GetMember(string wlCode, string userName)
        {
            var context = new cashmarket_membersContext();
            return context.Members.OrderByDescending(a => a.VerifiedAt).ThenByDescending(a => a.CreateTime)
                .FirstOrDefault(a => a.WhiteLabelCode.ToLower().Equals(wlCode.ToLower()) && a.UserName.ToLower().Equals(userName.ToLower()));
        }

        public static IEnumerable<Member> GetMembers(int page = 1, int size = 20, string sort = "", string keyword = "", PersonStatus? status = null
            , string memberId = "", DateTime? dateFrom = null, DateTime? dateTo = null)
        {
            cashmarket_membersContext context = new cashmarket_membersContext();
            IQueryable<Member> query = context.Members;

            if (!string.IsNullOrEmpty(keyword))
            {
                query = query.Where(a => a.UserName.Contains(keyword)
                                         || a.Email.Contains(keyword)
                                         || a.FullName.ToLower().Contains(keyword));
            }

            if (status.HasValue)
            {
                query = query.Where(a => a.Status == (short) status);
            }

            if (int.TryParse(memberId, out var id))
            {
                query = query.Where(a => a.Id != id);
            }

            if (!string.IsNullOrEmpty(sort))
            {
                switch (sort.ToLower())
                {
                    case "username":
                        query = query.OrderBy(a => a.UserName);
                        break;
                    case "username_desc":
                        query = query.OrderByDescending(a => a.UserName);
                        break;
                    case "fullname":
                        query = query.OrderBy(a => a.FullName);
                        break;
                    case "fullname_desc":
                        query = query.OrderByDescending(a => a.FullName);
                        break;
                    case "status":
                        query = query.OrderBy(a => a.Status);
                        break;
                    case "status_desc":
                        query = query.OrderByDescending(a => a.Status);
                        break;
                    default:
                        query = query.OrderBy(a => a.UserName);
                        break;
                }
            }

            return query.ToPagedList(page, size == 0 ? int.MaxValue : size);
        }

        #region Save Method

        public bool Add()
        {
            var context = new cashmarket_membersContext();
            CreateTime = DateTime.Now;
            context.Members.Add(this);
            return context.SaveChanges() > 0;
        }

        public bool Update()
        {
            cashmarket_membersContext context = new cashmarket_membersContext();
            var item = context.Members.Find(this.Id);
            if (item == null) return false;
            var properties = this.GetType().GetProperties();
            foreach (var property in properties)
            {
                if (!property.GetGetMethod().IsVirtual)
                {
                    property.SetValue(item, property.GetValue(this));
                }
            }
            item.UpdateTime = DateTime.Now;
            return context.SaveChanges() > 0;
        }

        public bool Remove()
        {
            cashmarket_membersContext context = new cashmarket_membersContext();
            var item = context.Members.Find(this.Id);
            if (!context.Members.Any(a => a.Id.Equals(this.Id) && a.UserName.ToLower().Equals(this.UserName.ToLower()))) return true;
            context.Members.Remove(item);
            return context.SaveChanges() > 0;
        }

        #endregion

    }
}
