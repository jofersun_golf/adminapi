﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace CashMarket_Entities.Models.Members
{
    public partial class MemberWallet
    {
        public int Id { get; set; }
        public int MemberId { get; set; }
        public decimal Balance { get; set; }
        public DateTime? CreateTime { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateTime { get; set; }
        public string UpdateBy { get; set; }
        public short Status { get; set; }
        public string CurrencyCode { get; set; }
        public short WalletType { get; set; }

        public virtual Member Member { get; set; }

        public static IEnumerable<MemberWallet> GetMemberWallets(string[] wlCodes = null)
        {
            cashmarket_membersContext context = new cashmarket_membersContext();
            IQueryable<MemberWallet> query = context.MemberWallets;
            if (wlCodes != null && wlCodes.Any())
            {
                query = query.Where(a => wlCodes.Contains(a.Member.WhiteLabelCode.ToLower()));
            }
            return query.Include(a => a.Member).ToList();
        }

        #region Save Method

        public bool Add()
        {
            var context = new cashmarket_membersContext();
            CreateTime = DateTime.Now;
            context.MemberWallets.Add(this);
            return context.SaveChanges() > 0;
        }

        public bool Update()
        {
            cashmarket_membersContext context = new cashmarket_membersContext();
            var item = context.MemberWallets.Find(this.Id);
            if (item == null) return false;
            var properties = this.GetType().GetProperties();
            foreach (var property in properties)
            {
                if (!property.GetGetMethod().IsVirtual)
                {
                    property.SetValue(item, property.GetValue(this));
                }
            }
            item.UpdateTime = DateTime.Now;
            return context.SaveChanges() > 0;
        }

        public bool Remove()
        {
            cashmarket_membersContext context = new cashmarket_membersContext();
            var item = context.MemberWallets.Find(this.Id);
            if (!context.MemberWallets.Any(a => a.MemberId.Equals(this.MemberId))) return true;
            context.MemberWallets.Remove(item);
            return context.SaveChanges() > 0;
        }

        #endregion
    }
}
