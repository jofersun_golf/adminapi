﻿using System;

#nullable disable

namespace CashMarket_Entities.Models.Members
{
    public partial class MemberResetHistory
    {
        public Guid Id { get; set; }
        public int MemberId { get; set; }
        public string WhiteLabelCode { get; set; }
        public string PrefixUserName { get; set; }
        public string UserName { get; set; }
        public string LinkAddress { get; set; }
        public bool? StatusSent { get; set; }
        public bool? StatusUsed { get; set; }
        public DateTime? CreateTime { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateTime { get; set; }
        public string UpdateBy { get; set; }
        public short Status { get; set; }

        public virtual Member Member { get; set; }
    }
}
