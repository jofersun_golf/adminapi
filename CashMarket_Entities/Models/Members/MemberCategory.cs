﻿using System;

#nullable disable

namespace CashMarket_Entities.Models.Members
{
    public partial class MemberCategory
    {
        public int Id { get; set; }
        public int MemberId { get; set; }
        public string CategoryCode { get; set; }
        public string CategoryName { get; set; }
        public int[] FriendListIds { get; set; }
        public DateTime? CreateTime { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateTime { get; set; }
        public string UpdateBy { get; set; }
        public short Status { get; set; }

        public virtual Member Member { get; set; }
    }
}
