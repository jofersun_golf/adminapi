﻿using System;
using System.Collections.Generic;
using System.Linq;
using CashMarket_Entities.Enums;
using CashMarket_Entities.Models.Admin;
using CashMarket_Entities.Models.Members;
using CashMarket_Entities.Models.Providers;
using Microsoft.EntityFrameworkCore;
using X.PagedList;

namespace CashMarket_Entities.Models.Reports
{
    public static class ReportHelper
    {
        public static DashboardReport GetDashboardReport(DateTime from, DateTime to, string[] providerConnStrings)
        {
            DashboardReport report = new DashboardReport();

            cashmarket_adminContext adminContext = new cashmarket_adminContext();
            var requestsQuery = adminContext.TransactionRequests.Where(a =>
                a.CreateTime.HasValue && a.CreateTime.Value.Date >= from && a.CreateTime.Value.Date <= to
                && a.ApprovalStatus == (short) Approval.Approve);
            report.TotalDeposit = requestsQuery.Where(a => a.RequestType == (short) Request.Deposit).Sum(a => a.Amount);
            report.TotalWithdrawal = requestsQuery.Where(a => a.RequestType == (short) Request.Withdraw).Sum(a => a.Amount);

            cashmarket_membersContext membersContext = new cashmarket_membersContext();
            var membersQuery = membersContext.Members.Where(a =>
                a.CreateTime.HasValue && a.CreateTime.Value.Date >= from && a.CreateTime.Value.Date <= to);
            report.TotalRegistered = membersQuery.Count();

            foreach (string connString in providerConnStrings)
            {
                if (string.IsNullOrEmpty(connString)) continue;
                cashmarket_provider_aContext providerAContext = new cashmarket_provider_aContext();
                providerAContext.Database.SetConnectionString(connString);
                var logsQuery = providerAContext.CreditLogs.Where(a =>
                    a.CreateTime.HasValue && a.CreateTime.Value.Date >= from && a.CreateTime.Value.Date <= to);
                report.TotalActive += logsQuery.Select(a => a.PrefixUserName).Distinct().Count();
                report.TotalTurnover += logsQuery.Sum(a => a.Amount);


                var wagersQuery = providerAContext.Wagers.Where(a =>
                    a.CreateTime.HasValue && a.CreateTime.Value.Date >= from && a.CreateTime.Value.Date <= to && a.UpdateTime != null);
                report.TotalWinLose += wagersQuery.Sum(a => a.PayoutAmount + a.ActualRealBet);
                //providerAContexts.Add(providerAContext);
            }

            report.Balance = report.TotalDeposit - report.TotalWithdrawal;

            return report;
        }

        public static CreditLog[] GetCreditLogs(string[] providerConnStrings, int page = 1, int size = 20,
            DateTime? from = null, DateTime? to = null, string[] prefixUserNames = null, string[] providerCodes = null, string[] productCodes = null)
        {
            List<CreditLog> creditLogs = new List<CreditLog>();
            foreach (string connString in providerConnStrings)
            {
                if (string.IsNullOrEmpty(connString)) continue;
                cashmarket_provider_aContext providerAContext = new cashmarket_provider_aContext();
                providerAContext.Database.SetConnectionString(connString);
                var logsQuery = providerAContext.CreditLogs.AsQueryable();

                if (from.HasValue)
                {
                    logsQuery = logsQuery.Where(a => (a.CreateTime.HasValue && a.CreateTime.Value.Date >= from.Value)
                    || (a.UpdateTime.HasValue && a.UpdateTime.Value.Date >= from.Value));
                }
                if (to.HasValue)
                {
                    logsQuery = logsQuery.Where(a => (a.CreateTime.HasValue && a.CreateTime.Value.Date <= to.Value)
                                                     || (a.UpdateTime.HasValue && a.UpdateTime.Value.Date <= to.Value));
                }
                if (prefixUserNames != null && prefixUserNames.Any())
                {
                    var prefixUserNames2 = prefixUserNames.Select(a => a.ToLower()).ToList();
                    logsQuery = logsQuery.Where(a => prefixUserNames2.Contains(a.PrefixUserName.ToLower()));
                }
                if (providerCodes != null && providerCodes.Any())
                {
                    var providerCodes2 = providerCodes.Select(a => a.ToLower()).ToList();
                    logsQuery = logsQuery.Where(a => providerCodes2.Contains(a.ProviderCode.ToLower()));
                }
                if (productCodes != null && productCodes.Any())
                {
                    var productCodes2 = productCodes.Select(a => a.ToLower()).ToList();
                    logsQuery = logsQuery.Where(a => productCodes2.Contains(a.ProductCode.ToLower()));
                }
                creditLogs.AddRange(logsQuery.ToList());
            }
            return creditLogs.OrderByDescending(a=>a.UpdateTime).ToPagedList(page, size == 0 ? int.MaxValue : size).ToArray();
        }

        public static List<Wager> GetWagers(string[] providerConnStrings, int page = 1, int size = 20,
            DateTime? from = null, DateTime? to = null, string[] prefixUserNames = null, string[] providerCodes = null, string[] productCodes = null)
        {
            List<Wager> wagers = new List<Wager>();
            foreach (string connString in providerConnStrings)
            {
                if (string.IsNullOrEmpty(connString)) continue;
                cashmarket_provider_aContext providerAContext = new cashmarket_provider_aContext();
                providerAContext.Database.SetConnectionString(connString);
                var wagerQuery = providerAContext.Wagers.AsQueryable();

                if (from.HasValue)
                {
                    wagerQuery = wagerQuery.Where(a => (a.CreateTime.HasValue && a.CreateTime.Value.Date >= from.Value)
                    || (a.UpdateTime.HasValue && a.UpdateTime.Value.Date >= from.Value));
                }
                if (to.HasValue)
                {
                    wagerQuery = wagerQuery.Where(a => (a.CreateTime.HasValue && a.CreateTime.Value.Date <= to.Value)
                                                     || (a.UpdateTime.HasValue && a.UpdateTime.Value.Date <= to.Value));
                }
                if (prefixUserNames != null && prefixUserNames.Any())
                {
                    var prefixUserNames2 = prefixUserNames.Select(a => a.ToLower()).ToList();
                    wagerQuery = wagerQuery.Where(a => prefixUserNames2.Contains(a.PrefixUserName.ToLower()));
                }
                if (providerCodes != null && providerCodes.Any())
                {
                    var providerCodes2 = providerCodes.Select(a => a.ToLower()).ToList();
                    wagerQuery = wagerQuery.Where(a => providerCodes2.Contains(a.ProviderCode.ToLower()));
                }
                if (productCodes != null && productCodes.Any())
                {
                    var productCodes2 = productCodes.Select(a => a.ToLower()).ToList();
                    wagerQuery = wagerQuery.Where(a => productCodes2.Contains(a.ProductCode.ToLower()));
                }

                wagers.AddRange(wagerQuery.ToList());
            }
            return wagers.OrderByDescending(a => a.UpdateTime).ToPagedList(page, size == 0 ? int.MaxValue : size).ToList();
        }

        public static List<Wager> GetWagers(string[] providerConnStrings,
            out decimal grandTotalStake, out decimal grandTotalWinLose, out decimal grandTotalRealBet, int page = 1, int size = 20,
            DateTime? from = null, DateTime? to = null, string keyWord = "", string[] providerCodes = null, string[] categoryCodes = null, string[] productCodes = null)
        {
            List<Wager> wagers = new List<Wager>();
            grandTotalStake = 0;
            grandTotalWinLose = 0;
            grandTotalRealBet = 0;

            List<ProductCategory> productCategories = new List<ProductCategory>();
            List<string> productCategories2 = new List<string>();
            if (categoryCodes != null && categoryCodes.Any())
            {
                var categoryCodes2 = categoryCodes.Select(a => a.ToLower()).ToList();
                cashmarket_adminContext adminContext = new cashmarket_adminContext();
                productCategories.AddRange(adminContext.ProductCategories.Where(a => categoryCodes2.Contains(a.CategoryCode.ToLower())));
                productCategories2.AddRange(productCategories.Select(a=>a.ProductCode.ToLower()));
            }

            foreach (string connString in providerConnStrings)
            {
                if (string.IsNullOrEmpty(connString)) continue;
                cashmarket_provider_aContext providerAContext = new cashmarket_provider_aContext();
                providerAContext.Database.SetConnectionString(connString);
                var wagerQuery = providerAContext.Wagers.AsQueryable();

                if (from.HasValue)
                {
                    wagerQuery = wagerQuery.Where(a =>
                        (a.CreateTime.HasValue && a.CreateTime.Value.Date >= from.Value) ||
                        (a.UpdateTime.HasValue && a.UpdateTime.Value.Date >= from.Value));
                }
                if (to.HasValue)
                {
                    wagerQuery = wagerQuery.Where(a =>
                        (a.CreateTime.HasValue && a.CreateTime.Value.Date <= to.Value) ||
                        (a.UpdateTime.HasValue && a.UpdateTime.Value.Date <= to.Value));
                }

                if (!string.IsNullOrEmpty(keyWord))
                {
                    wagerQuery = wagerQuery.Where(a => a.PrefixUserName.Contains(keyWord));
                }

                if (providerCodes != null && providerCodes.Any())
                {
                    var providerCodes2 = providerCodes.Select(a => a.ToLower()).ToList();
                    wagerQuery = wagerQuery.Where(a => providerCodes2.Contains(a.ProviderCode.ToLower()));
                }
                if (productCodes != null && productCodes.Any())
                {
                    var productCodes2 = productCodes.Select(a => a.ToLower()).ToList();
                    wagerQuery = wagerQuery.Where(a => productCodes2.Contains(a.ProductCode.ToLower()));
                }

                if (productCategories2.Any())
                {
                    wagerQuery = wagerQuery.Where(a => productCategories2.Contains(a.ProductCode.ToLower()));
                }

                wagers.AddRange(wagerQuery.ToList());
            }

            grandTotalStake = wagers.Sum(a=>a.StakeAmount);
            grandTotalWinLose = wagers.Sum(a => a.PayoutAmount - a.StakeAmount);
            grandTotalRealBet = wagers.Sum(a => a.ActualRealBet);

            return wagers.OrderByDescending(a => a.UpdateTime).ToPagedList(page, size == 0 ? int.MaxValue : size).ToList();
        }
    }
}
