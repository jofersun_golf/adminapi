﻿namespace CashMarket_Entities.Models.Reports
{
    public class DashboardReport
    {
        public decimal TotalDeposit { get; set; }
        public decimal TotalWithdrawal { get; set; }
        public decimal Balance { get; set; }
        public int TotalRegistered { get; set; }
        public int TotalActive { get; set; }
        public decimal TotalWinLose { get; set; }
        public decimal TotalTurnover { get; set; }
    }
}
