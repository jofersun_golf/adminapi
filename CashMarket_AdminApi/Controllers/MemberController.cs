﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CashMarket_AdminApi.Models;
using CashMarket_AdminApi.Models.Admin;
using CashMarket_AdminApi.Models.Exception;
using CashMarket_AdminApi.Models.Member;
using CashMarket_Entities.Enums;
using CashMarket_Entities.Models;
using CashMarket_Entities.Models.Admin;
using CashMarket_Entities.Models.Base;
using CashMarket_Entities.Utilities;
using Newtonsoft.Json;

namespace CashMarket_AdminApi.Controllers
{
    [Route("api/Admin/Member")]
    [ApiController]
    public class MemberController : BaseController
    {
        [HttpPost("GetMemberList")]
        public async Task<IActionResult> GetMemberList()
        {
            var jsonInput = Request.HasFormContentType ? GetJson(Request.Form) : await new StreamReader(Request.Body).ReadToEndAsync();
            MemberListOutput output = new MemberListOutput();
            try
            {
                var input = JsonConvert.DeserializeObject<MemberListInput>(jsonInput);
                if (input != null)
                {
                    var whiteLabels = CashMarket_Entities.Models.WhiteLabels.WhiteLabel.GetWhiteLabels();
                    var members = CashMarket_Entities.Models.Members.Member.GetMembers(input.Page, input.Size,
                        input.Sort, input.KeyWord, input.Status, string.Empty, input.DateFrom, input.DateTo);
                    var transactions = TransactionRequest.GetTransactionRequests(input.Page,
                            input.Size, input.Sort, null, null,
                            new Request[]
                            {
                                CashMarket_Entities.Enums.Request.Deposit, CashMarket_Entities.Enums.Request.Withdraw
                            }, new Approval[]
                            {
                                Approval.Approve
                            },
                            input.DateFrom, input.DateTo).ToList();

                    var memberTags = MemberStatusTag.GetMemberStatusTags().ToList();

                    foreach (CashMarket_Entities.Models.Members.Member member in members)
                    {
                        var query = transactions.Where(a=> a.MemberUserName.Equals(member.UserName,StringComparison.InvariantCultureIgnoreCase)).ToList();
                        var statusTags = memberTags.Where(a => a.UserName.Equals(member.UserName, StringComparison.CurrentCultureIgnoreCase)).ToList();

                        output.Data.Add(new MemberListData()
                        {
                            Id = member.Id,
                            Name = member.UserName,
                            Code = member.WhiteLabelCode,
                            MemberTags = string.Join(", ", statusTags.Select(a => a.StatusTag?.TagName).ToList()),
                            FullName = member.FullName,
                            Channel = "belom tahu",
                            MainWallet = member.MemberWallets.Sum(a=>a.Balance),
                            Deposit = query.Count(a => a.RequestType == (short) CashMarket_Entities.Enums.Request.Deposit).ToString() + " / " +
                                      query.Where(a => a.RequestType == (short) CashMarket_Entities.Enums.Request.Deposit).Sum(a=>a.Amount),
                            Contact = member.Contact,
                            DateOfBirth = member.DateOfBirth,
                            Email = member.Email,
                            RegisteredAt = member.CreateTime?.Date ?? member.VerifiedAt?.Date,
                            LastDeposit = query.OrderByDescending(a=>a.CreateTime).FirstOrDefault()?.CreateTime,
                            Status = ((PersonStatus)member.MemberStatus).ToString(),
                            IP = member.RegisterIp
                        });
                    }

                    output.Response.ErrorCode = ErrorCodes.Success;
                }
                else output.Response.ErrorCode = ErrorCodes.InvalidParameter;
            }
            catch (Exception e)
            {
                SetException(e, output.Response);
            }

            return Result(output);
        }

        [HttpPost("SaveMember")]
        public async Task<IActionResult> SaveMember()
        {
            var jsonInput = Request.HasFormContentType ? GetJson(Request.Form) : await new StreamReader(Request.Body).ReadToEndAsync();
            BaseOutput output = new BaseOutput();
            try
            {
                var input = JsonConvert.DeserializeObject<MemberInput>(jsonInput);
                if (input != null)
                {
                    if (!input.Validate()) throw new InvalidParameterException(input.ErrorMessage);

                    CashMarket_Entities.Models.Members.Member member;
                    switch (input.Action)
                    {
                        case Constants.Add:
                            string salt = Guid.NewGuid().ToString();
                            member = new CashMarket_Entities.Models.Members.Member
                            {
                                Id = 0,
                                WhiteLabelCode = input.Member.WhiteLabelCode,
                                PrefixUserName = input.Member.WhiteLabelCode + "_" + input.Member.UserName,
                                UserName = input.Member.UserName,
                                Salt = salt,
                                EncryptedPassword = HashMethod.HmacSha256Digest(input.Member.PassWord, salt),
                                Email = input.Member.Email,
                                DateOfBirth = input.Member.DateOfBirth,
                                FullName = input.Member.FullName,
                                Gender = input.Member.Gender,
                                Contact = input.Member.Contact,
                                Contact2 = input.Member.Contact2,
                                RegisterIp = GetIpAddress(),
                                Verified = false,
                                WrongCount = 0,
                                MemberStatus = (short) PersonStatus.Activated,
                                CreateBy = input.UserLogin,
                                CurrencyCode = input.Member.CurrencyCode,
                                ReferralReference = input.Member.ReferralReference,
                                ValidationExpiredDate = DateTime.Now.AddDays(3)
                            };
                            if (member.Add()) output.Response.ErrorCode = ErrorCodes.Success;
                            break;
                        case Constants.Update:
                            member = CashMarket_Entities.Models.Members.Member.GetById(input.Member.Id);
                            if (member != null)
                            {
                                member.WhiteLabelCode = input.Member.WhiteLabelCode;
                                member.PrefixUserName = input.Member.WhiteLabelCode + "_" + input.Member.UserName;
                                member.UserName = input.Member.UserName;
                                member.EncryptedPassword = HashMethod.HmacSha256Digest(input.Member.PassWord, member.Salt);
                                member.Email = input.Member.Email;
                                member.DateOfBirth = input.Member.DateOfBirth;
                                member.FullName = input.Member.FullName;
                                member.Gender = input.Member.Gender;
                                member.Contact = input.Member.Contact;
                                member.Contact2 = input.Member.Contact2;
                                member.MemberStatus = (short) PersonStatus.Activated;
                                member.UpdateBy = input.UserLogin;
                                member.CurrencyCode = input.Member.CurrencyCode;
                                member.ReferralReference = input.Member.ReferralReference;
                                if (member.Update()) output.Response.ErrorCode = ErrorCodes.Success;
                            }
                            else output.Response.ErrorCode = ErrorCodes.NotFound;
                            break;
                        case Constants.Remove:
                            member = CashMarket_Entities.Models.Members.Member.GetById(input.Member.Id);
                            if (member != null)
                            {
                                if (member.Remove()) output.Response.ErrorCode = ErrorCodes.Success;
                            }
                            else output.Response.ErrorCode = ErrorCodes.NotFound;
                            break;
                        default:
                            output.Response.ErrorCode = ErrorCodes.InvalidParameter;
                            break;
                    }
                }
                else output.Response.ErrorCode = ErrorCodes.InvalidParameter;
            }
            catch (Exception e)
            {
                SetException(e, output.Response);
            }

            return Result(output);
        }

        [HttpPost("GetAccountDetails")]
        public async Task<IActionResult> GetAccountDetails()
        {
            var jsonInput = Request.HasFormContentType ? GetJson(Request.Form) : await new StreamReader(Request.Body).ReadToEndAsync();
            var output = new BaseOutput<AccountDetailsOutput>();
            try
            {
                var input = JsonConvert.DeserializeObject<AccountDetailsInput>(jsonInput);
                if (input != null)
                {
                    if (!input.Validate()) throw new InvalidParameterException(input.ErrorMessage);

                    var member = CashMarket_Entities.Models.Members.Member.GetById(input.MemberAccount.Id);
                    if (member == null || !member.WhiteLabelCode.Equals(input.MemberAccount.WhiteLabelCode, StringComparison.CurrentCultureIgnoreCase) ||
                        !member.UserName.Equals(input.MemberAccount.UserName, StringComparison.CurrentCultureIgnoreCase))
                    {
                        output.Response.ErrorCode = ErrorCodes.InvalidData;
            
                        return new ContentResult {Content = JsonConvert.SerializeObject(output)};
                    }

                    output.Data.AccountInformation = new AccountInformationData()
                    {
                        UserName = member.UserName,
                        WhiteLabelCode = member.WhiteLabelCode,
                        FullName = member.FullName,
                        DateOfBirth = member.DateOfBirth,
                        Gender = member.Gender,
                        Status = (PersonStatus) member.MemberStatus,
                        Email = member.Email,
                        Contact = member.Contact,
                        Contact2 = member.Contact2
                    };

                    var statusTags = StatusTag.GetMemberStatusTags(input.MemberAccount.WhiteLabelCode, input.MemberAccount.UserName);
                    foreach (StatusTag tag in statusTags)
                    {
                        output.Data.StatusTags.Add(new StatusTagData()
                        {
                            Id = tag.Id,
                            WhiteLabelCode = tag.WhiteLabelCode,
                            ColorCode = tag.ColorCode,
                            TagCode = tag.TagCode,
                            TagName = tag.TagName
                        });
                    }
                    output.Response.ErrorCode = ErrorCodes.Success;
                }
                else output.Response.ErrorCode = ErrorCodes.InvalidParameter;
            }
            catch (Exception e)
            {
                SetException(e, output.Response);
            }

            return Result(output);
        }

        [HttpPost("VerifyMember")]
        public async Task<IActionResult> VerifyMember()
        {
            var jsonInput = Request.HasFormContentType ? GetJson(Request.Form) : await new StreamReader(Request.Body).ReadToEndAsync();
            BaseOutput output = new BaseOutput();
            try
            {
                var input = JsonConvert.DeserializeObject<VerifyMemberInput>(jsonInput);
                if (input != null)
                {
                    if (!input.Validate()) throw new InvalidParameterException(input.ErrorMessage);
                    CashMarket_Entities.Models.Members.Member member = CashMarket_Entities.Models.Members.Member.GetMember(input.Member.WhiteLabelCode, input.Member.UserName);
                    if (member != null &&
                        member.Email.Equals(input.Member.Email, StringComparison.CurrentCultureIgnoreCase) &&
                        member.Contact.Equals(input.Member.Contact, StringComparison.CurrentCultureIgnoreCase))
                    {
                        if (!member.Verified || member.VerifiedAt == null)
                        {
                            member.Verified = true;
                            member.VerifiedAt = DateTime.Now;
                            member.Status = (short) PersonStatus.Activated;
                            member.UpdateBy = input.UserLogin;
                            if (!member.Update())
                            {
                                output.Response.ErrorCode = ErrorCodes.UnableSave;
                                return Result(output);
                            }

                            var wallet = new CashMarket_Entities.Models.Members.MemberWallet()
                            {
                                MemberId = member.Id,
                                Balance = 0,
                                CreateBy = input.UserLogin,
                                CurrencyCode = member.CurrencyCode,
                                WalletType = (short) Wallet.Main,
                                Status = 1
                            };
                            if (wallet.Add()) output.Response.ErrorCode = ErrorCodes.Success;
                        }
                        else output.Response.ErrorCode = ErrorCodes.AlreadyProcessed;
                    }
                    else output.Response.ErrorCode = ErrorCodes.NotFound;
                }
                else output.Response.ErrorCode = ErrorCodes.InvalidParameter;
            }
            catch (Exception e)
            {
                SetException(e, output.Response);
            }

            return Result(output);
        }
    }
}
