﻿using CashMarket_AdminApi.Service;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CashMarket_Entities.Models.WhiteLabels;

namespace CashMarket_AdminApi.Controllers
{
    //[Route("api/[controller]")]
    [ApiController]
    public class WhitelabelsController : ControllerBase
    {
        private readonly IWhitelabelsService iWhitelabelsService;
        public WhitelabelsController(IWhitelabelsService _IWhitelabelsService)
        {
            iWhitelabelsService = _IWhitelabelsService;
        }

        [HttpGet]
        [Route("/GetAllWhiteLabelLanguage")]
        public async Task<IEnumerable<WhiteLabelLanguage>> GetAllWhiteLabelLanguage()
        {
            return iWhitelabelsService.GetAllWhiteLabelLanguage();
        }

        [HttpGet]
        [Route("/GetAllWhiteLabelProvider")]
        public async Task<IEnumerable<WhiteLabelProvider>> GetAllWhiteLabelProvider()
        {
            return iWhitelabelsService.GetAllWhiteLabelProvider();
        }

        [HttpGet]
        [Route("/GetAllWhiteLabel")]
        public async Task<IEnumerable<WhiteLabel>> GetAllWhiteLabel()
        {
            return iWhitelabelsService.GetAllWhiteLabel();
        }

        [HttpGet]
        [Route("/GetAllWhiteLabelProduct")]
        public async Task<IEnumerable<WhiteLabelProduct>> GetAllWhiteLabelProduct()
        {
            return iWhitelabelsService.GetAllWhiteLabelProduct();
        }

        [HttpPost]
        [Route("/AddWhiteLabel")]
        public async Task<IActionResult> AddWhiteLabel(WhiteLabel whitelabels)
        {
            iWhitelabelsService.AddWhiteLabel(whitelabels);
            return Ok();
        }

        [HttpPost]
        // [Route("[action]")]//api/Whitelabels
        [Route("/UpdateWhiteLabel")]
        public async Task<IActionResult> UpdateWhiteLabel(WhiteLabel whitelabels)
        {
            iWhitelabelsService.UpdateWhiteLabel(whitelabels);
            return Ok();
        }

        [HttpDelete]
        [Route("/DeleteWhiteLabel")]
        public async Task<IActionResult> DeleteWhiteLabel(string wl)
        {
            var _wl = iWhitelabelsService.GetWhiteLabel(wl);
            if (_wl != null)
            {
                iWhitelabelsService.DeleteWhiteLabel(_wl.WhiteLabelCode);
                return Ok();
            }
            return NotFound($"Not Found : {_wl.WhiteLabelCode}");
        }

        [HttpGet]
        [Route("GetWhiteLabel")]
        public async Task<WhiteLabel> GetWhiteLabel(string wl)
        {
            return iWhitelabelsService.GetWhiteLabel(wl);
        }

        [HttpGet]
        [Route("GetWhiteLabelProduct")]
        public async Task<WhiteLabelProduct> GetWhiteLabelProduct(int WhiteLabelId)
        {
            return iWhitelabelsService.GetWhiteLabelProduct(WhiteLabelId);
        }

        [HttpGet]
        [Route("GetWhiteLabelLanguage")]
        public async Task<WhiteLabelLanguage> GetWhiteLabelLanguage(int WhiteLabelId)
        {
            return iWhitelabelsService.GetWhiteLabelLanguage(WhiteLabelId);
        }

        [HttpGet]
        [Route("GetWhiteLabelProvider")]
        public async Task<WhiteLabelProvider> GetWhiteLabelProvider(string Brandname)
        {
            return iWhitelabelsService.GetWhiteLabelProvider(Brandname);
        }
    }
}
