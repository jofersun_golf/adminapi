﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CashMarket_AdminApi.Models.Report;
using CashMarket_Entities.Enums;
using CashMarket_Entities.Models;
using CashMarket_Entities.Models.Base;
using Newtonsoft.Json;
using X.PagedList;
using CashMarket_Entities.Models.Report;

namespace CashMarket_AdminApi.Controllers
{
    [Route("api/Admin/Report")]
    public class ReportController : BaseController
    {
        [HttpPost("GetBetInquiries")]
        public async Task<IActionResult> GetBetInquiries()
        {
            var jsonInput = Request.HasFormContentType ? GetJson(Request.Form) : await new StreamReader(Request.Body).ReadToEndAsync();
            var output = new BaseOutput<BetInquiriesOutput>();
            try
            {
                var input = JsonConvert.DeserializeObject<BetInquiriesInput>(jsonInput);
                if (input != null)
                {
                    var wagers = Wager.GetWagers(out var grandTotalStake, out var grandTotalWinLose, out var grandTotalRealBet,
                        input.Page, input.Size, input.DateFrom, input.DateTo, input.Keyword, input.ProviderCode, input.CategoryCode, input.ProductCode, input.Sort).ToList();
                    foreach (Wager wager in wagers)
                    {
                        string result;
                        if (wager.PayoutAmount > wager.StakeAmount) result = BetStatus.Win.ToString();
                        else if (wager.PayoutAmount < wager.StakeAmount) result = BetStatus.Lose.ToString();
                        else result = BetStatus.Draw.ToString();

                        dynamic betData = JsonConvert.DeserializeObject(wager.BetData);

                        output.Data.BetInquiries.Add(new BetInquiryData()
                        {
                            DateTime = wager.GameLastUpdateTime ?? wager.UpdateTime ?? wager.PlaceTime ?? wager.CreateTime,
                            Member = wager.PrefixUserName,
                            ProviderCode = wager.ProviderCode,
                            CategoryName = wager.CategoryName,
                            Game = wager.ProductName,
                            ReferenceNo = wager.RefKey,
                            RoundId = GetDynamicValue(betData, "round_id")?.ToString() ?? string.Empty,
                            Selection = wager.BetData,
                            Stake = wager.StakeAmount,
                            WinLose = wager.PayoutAmount - wager.StakeAmount,
                            RealBet = wager.ActualRealBet,
                            Result = result
                        });
                    }

                    output.Data.SubTotalStake = wagers.Sum(a => a.StakeAmount);
                    output.Data.SubTotalWinLose = wagers.Sum(a => a.PayoutAmount - a.StakeAmount);
                    output.Data.SubTotalRealBet = wagers.Sum(a => a.ActualRealBet);
                    output.Data.GrandTotalStake = grandTotalStake;
                    output.Data.GrandTotalWinLose = grandTotalWinLose;
                    output.Data.GrandTotalRealBet = grandTotalRealBet;

                    output.Response.ErrorCode = ErrorCodes.Success;
                }
                else output.Response.ErrorCode = ErrorCodes.InvalidParameter;
            }
            catch (Exception e)
            {
                SetException(e, output.Response);
            }

            return Result(output);
        }

        [HttpPost("GetProfitLost")]
        public async Task<IActionResult> GetProfitLost()
        {
            var jsonInput = Request.HasFormContentType ? GetJson(Request.Form) : await new StreamReader(Request.Body).ReadToEndAsync();
            var output = new BaseOutput<ProfitLostOutput>();
            try
            {
                var input = JsonConvert.DeserializeObject<ProfitLostInput>(jsonInput);
                if (input != null)
                {
                    var wagers = Wager.GetWagers(out var grandTotalStake, out var grandTotalWinLose, out var grandTotalRealBet,
                        input.Page, input.Size, input.DateFrom, input.DateTo, null, input.ProviderCode, input.CategoryCode, input.ProductCode, input.Sort).ToList();

                    var groupWagers2 = new List<string>();
                    if (input.GroupBy != null && input.GroupBy.Any()) groupWagers2.AddRange(input.GroupBy);
                    else groupWagers2.Add("PrefixUserName");

                    var groupWagers = wagers.GroupBy(a => new
                    {
                        PrefixUserName = groupWagers2.Contains("PrefixUserName", StringComparer.CurrentCultureIgnoreCase) ? a.PrefixUserName : null,
                        CategoryCode = groupWagers2.Contains("CategoryCode", StringComparer.CurrentCultureIgnoreCase) ? a.CategoryCode : null,
                        ProviderCode = groupWagers2.Contains("ProviderCode", StringComparer.CurrentCultureIgnoreCase) ? a.ProviderCode : null,
                        ProductCode = groupWagers2.Contains("ProductCode", StringComparer.CurrentCultureIgnoreCase) ? a.ProductCode : null,
                    }).ToPagedList(input.Page, input.Size).ToList();

                    foreach (var wager in groupWagers)
                    {
                        Console.WriteLine(wager.Key.PrefixUserName);
                        output.Data.ProfitLost.Add(new ProfitLostData()
                        {
                            UserName = wager.Key.PrefixUserName ?? (input.PrefixUserName == null || !input.PrefixUserName.Any() ? "All" : string.Join("; ", input.PrefixUserName)),
                            Provider = wager.Key.ProviderCode ?? (input.ProviderCode == null || !input.ProviderCode.Any() ? "All" : string.Join("; ", input.ProviderCode)),
                            Type = wager.Key.CategoryCode ?? (input.CategoryCode == null || !input.CategoryCode.Any() ? "All" : string.Join("; ", input.CategoryCode)),
                            Game = wager.Key.ProductCode ?? (input.ProductCode == null || !input.ProductCode.Any() ? "All" : string.Join("; ", input.ProductCode)),
                            Count = wager.Count(),
                            TurnOver = wager.Sum(a => a.StakeAmount),
                            RealBet = wager.Sum(a => a.ActualRealBet),
                            WinLose = wager.Sum(a => a.PayoutAmount - a.StakeAmount)
                        });
                    }

                    output.Data.TotalCount = wagers.Count;
                    output.Data.TotalTurnOver = grandTotalStake;
                    output.Data.TotalRealBet = grandTotalRealBet;
                    output.Data.TotalWinLose = grandTotalWinLose;

                    output.Response.ErrorCode = ErrorCodes.Success;
                }
                else output.Response.ErrorCode = ErrorCodes.InvalidParameter;
            }
            catch (Exception e)
            {
                SetException(e, output.Response);
            }

            return Result(output);
        }

        [HttpPost("GetCreditLog")]
        public async Task<IActionResult> GetCreditLog()
        {
            var jsonInput = Request.HasFormContentType ? GetJson(Request.Form) : await new StreamReader(Request.Body).ReadToEndAsync();
            CreditLogOutput output = new CreditLogOutput();
            try
            {
                var input = JsonConvert.DeserializeObject<ReportInput>(jsonInput);
                if (input != null)
                {
                    var creditLogs = CreditLog.GetCreditLogs(out var grandTotalAmount,
                        input.Page, input.Size, input.DateFrom, input.DateTo,null, input.ProviderCode, input.CategoryCode, input.ProductCode, input.Sort);

                    foreach (CreditLog creditLog in creditLogs)
                    {
                        string fromWallet = string.Empty;
                        string toWallet = string.Empty;
                        decimal fromStart = 0;
                        decimal fromEnd = 0;
                        decimal toStart = 0;
                        decimal toEnd = 0;

                        if (creditLog.ActionType == (short)ActionType.Na)
                        {
                            fromWallet = GetDescription((Wallet) creditLog.WalletTypeId);
                            toWallet = GetDescription((Wallet) creditLog.WalletTypeId);
                        }
                        else if (creditLog.ActionType == (short)ActionType.Bet)
                        {
                            fromWallet = GetDescription((Wallet) creditLog.WalletTypeId);
                            toWallet = creditLog.ProductName;
                            fromStart = creditLog.StartBalance;
                            fromEnd = creditLog.EndBalance;
                            toEnd = creditLog.Amount;
                        }
                        else if (creditLog.ActionType == (short) ActionType.Set)
                        {
                            fromWallet = creditLog.ProductName;
                            toWallet = GetDescription((Wallet) creditLog.WalletTypeId);
                            fromStart = creditLog.Amount;
                            toStart = creditLog.StartBalance;
                            toEnd = creditLog.EndBalance;
                        }

                        output.Data.Add(new CreditLogData()
                        {
                            DateTime = creditLog.UpdateTime ?? creditLog.CreateTime ?? DateTime.MinValue,
                            FromWallet = fromWallet,
                            FromStatus = creditLog.Status == 1 ? "Successful" : "Failed",
                            ToWallet = toWallet,
                            ToStatus = "Successful",
                            Amount = creditLog.Amount,
                            Status = creditLog.Status == 1 ? "Successful" : "Failed",
                            FromStart = fromStart,
                            FromEnd = fromEnd,
                            ToStart = toStart,
                            ToEnd = toEnd,
                            Action = !string.IsNullOrEmpty(creditLog.ActionTypeDesc) ? creditLog.ActionTypeDesc : GetDescription((ActionType) creditLog.ActionType)
                        });
                    }
                    
                    output.Response.ErrorCode = ErrorCodes.Success;
                }
                else output.Response.ErrorCode = ErrorCodes.InvalidParameter;
            }
            catch (Exception e)
            {
                SetException(e, output.Response);
            }

            return Result(output);
        }
    }
}
