﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CashMarket_AdminApi.Models;
using CashMarket_AdminApi.Models.Bank;
using CashMarket_AdminApi.Models.Exception;
using CashMarket_Entities.Enums;
using CashMarket_Entities.Models;
using CashMarket_Entities.Models.Admin;
using CashMarket_Entities.Models.Base;
using CashMarket_Entities.Models.WhiteLabels;
using Newtonsoft.Json;

namespace CashMarket_AdminApi.Controllers
{
    [Route("api/Admin/Bank")]
    public class BankController : BaseController
    {
        [HttpPost("GetBankList")]
        public async Task<IActionResult> GetBankList()
        {
            var jsonInput = Request.HasFormContentType ? GetJson(Request.Form) : await new StreamReader(Request.Body).ReadToEndAsync();
            BankListOutput output = new BankListOutput();
            try
            {
                var input = JsonConvert.DeserializeObject<BankListInput>(jsonInput);
                if (input != null)
                {
                    List<int> wlIds = new List<int>();
                    if (!string.IsNullOrEmpty(input.WhiteLabelCode))
                    {
                        var whiteLabel = WhiteLabel.GetByCode(input.WhiteLabelCode);
                        if (whiteLabel == null) throw new ArgumentException("WhiteLabelCode");
                        wlIds.Add(whiteLabel.Id);
                    }
                    var banks = WhiteLabelBank.GetBanks(input.Page, 0, input.Sort, input.KeyWord, null, wlIds.ToArray());
                    foreach (WhiteLabelBank bank in banks)
                    {
                        output.Data.Add(new WhiteLabelBankData()
                        {
                            BankCode = bank.BankTypeCode,
                            BankName = bank.BankName,
                            BankAccountName = bank.BankAccountName,
                            BankNumber = bank.BankNumber,
                            MaxDeposit = bank.MaxDeposit,
                            MaxWithdraw = bank.MaxWithdraw,
                            MinDeposit = bank.MinDeposit,
                            MinWithdraw = bank.MinWithdraw,
                            Deposit = bank.Deposit,
                            Withdraw = bank.Withdraw
                        });
                    }
                    output.Response.ErrorCode = ErrorCodes.Success;
                }
                else output.Response.ErrorCode = ErrorCodes.InvalidParameter;
            }
            catch (Exception e)
            {
                SetException(e, output.Response);
            }
            
            return Result(output);
        }

        [HttpPost("GetSummary")]
        public async Task<IActionResult> GetSummary()
        {
            var jsonInput = Request.HasFormContentType ? GetJson(Request.Form) : await new StreamReader(Request.Body).ReadToEndAsync();
            BankSummaryOutput output = new BankSummaryOutput();
            try
            {
                var input = JsonConvert.DeserializeObject<BankListInput>(jsonInput);
                if (input != null)
                {
                    var banks = WhiteLabelBank.GetBanks(input.Page, input.Size, input.Sort, input.KeyWord);
                    var bankAccounts = BankAccountBalance.GetBankAccounts(1, 0, string.Empty, string.Empty).ToList();
                    foreach (WhiteLabelBank bank in banks)
                    {
                        BankAccountBalance bankAcc = bankAccounts.FirstOrDefault(a =>
                            a.BankName.Equals(bank.BankName, StringComparison.CurrentCultureIgnoreCase) &&
                            a.BankNumber.Equals(bank.BankNumber, StringComparison.CurrentCultureIgnoreCase));

                        output.Data.Add(new BankSummaryData()
                        {
                            BankCode = bank.BankTypeCode,
                            BankName = bank.BankName,
                            BankAccountName = bank.BankAccountName,
                            BankNumber = bank.BankNumber,
                            Balance = bankAcc?.Balance ?? 0,
                            Status = GetDescription((RowStatus)bank.Status)
                        });
                    }
                    output.Response.ErrorCode = ErrorCodes.Success;
                }
                else output.Response.ErrorCode = ErrorCodes.InvalidParameter;
            }
            catch (Exception e)
            {
                SetException(e, output.Response);
            }

            return Result(output);
        }


        [HttpPost("SaveBank")]
        public async Task<IActionResult> SaveBank()
        {
            var jsonInput = Request.HasFormContentType ? GetJson(Request.Form) : await new StreamReader(Request.Body).ReadToEndAsync();
            BaseOutput output = new BaseOutput();
            try
            {
                var input = JsonConvert.DeserializeObject<BankInput>(jsonInput);
                if (input != null)
                {
                    if (!input.Validate()) throw new InvalidParameterException(input.ErrorMessage);

                    WhiteLabelBank wlBank;
                    WhiteLabel whiteLabel;
                    switch (input.Action)
                    {
                        case Constants.Add:
                            whiteLabel = WhiteLabel.GetByCode(input.Bank.WhiteLabelCode);
                            if (whiteLabel == null) throw new ArgumentException("WhiteLabelCode");
                            wlBank = new WhiteLabelBank()
                            {
                                Id = 0,
                                WhiteLabelId = whiteLabel.Id,
                                BankTypeCode = input.Bank.BankTypeCode,
                                BankName = input.Bank.BankName,
                                BankAccountName = input.Bank.BankAccountName,
                                BankNumber = input.Bank.BankNumber,
                                MaxDeposit = input.Bank.MaxDeposit,
                                MaxWithdraw = input.Bank.MaxWithdraw,
                                MinDeposit = input.Bank.MinDeposit,
                                MinWithdraw = input.Bank.MinWithdraw,
                                BankSerial = input.Bank.BankSerial,
                                Deposit = input.Bank.Deposit,
                                Withdraw = input.Bank.Withdraw,
                                CreateBy = input.UserLogin,
                                SortNumber = input.Bank.SortNumber,
                                Status = input.Bank.Status,
                            };
                            if (wlBank.Add()) output.Response.ErrorCode = ErrorCodes.Success;
                            break;
                        case Constants.Update:
                            if (string.IsNullOrEmpty(input.Bank.BankName)) throw new ArgumentNullException(Constants.InvalidName);
                            whiteLabel = WhiteLabel.GetByCode(input.Bank.WhiteLabelCode);
                            if (whiteLabel == null) throw new ArgumentException("WhiteLabelCode");
                            wlBank = WhiteLabelBank.GetById(input.Bank.Id);
                            if (wlBank != null)
                            {
                                wlBank.WhiteLabelId = whiteLabel.Id;
                                wlBank.BankTypeCode = input.Bank.BankTypeCode;
                                wlBank.BankName = input.Bank.BankName;
                                wlBank.BankAccountName = input.Bank.BankAccountName;
                                wlBank.BankNumber = input.Bank.BankNumber;
                                wlBank.MaxDeposit = input.Bank.MaxDeposit;
                                wlBank.MaxWithdraw = input.Bank.MaxWithdraw;
                                wlBank.MinDeposit = input.Bank.MinDeposit;
                                wlBank.MinWithdraw = input.Bank.MinWithdraw;
                                wlBank.BankSerial = input.Bank.BankSerial;
                                wlBank.Deposit = input.Bank.Deposit;
                                wlBank.Withdraw = input.Bank.Withdraw;
                                wlBank.UpdateBy = input.UserLogin;
                                wlBank.SortNumber = input.Bank.SortNumber;
                                wlBank.Status = input.Bank.Status;
                                if (wlBank.Update()) output.Response.ErrorCode = ErrorCodes.Success;
                            }
                            else output.Response.ErrorCode = ErrorCodes.NotFound;
                            break;
                        case Constants.Remove:
                            wlBank = WhiteLabelBank.GetById(input.Bank.Id);
                            if (wlBank != null)
                            {
                                if (wlBank.Remove()) output.Response.ErrorCode = ErrorCodes.Success;
                            }
                            else output.Response.ErrorCode = ErrorCodes.NotFound;
                            break;
                        default:
                            output.Response.ErrorCode = ErrorCodes.InvalidParameter;
                            break;
                    }
                }
                else output.Response.ErrorCode = ErrorCodes.InvalidParameter;
            }
            catch (Exception e)
            {
                SetException(e, output.Response);
            }

            return Result(output);
        }
    }
}
