﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;
using System.Threading.Tasks;
using CashMarket_AdminApi.Models.Dashboard;
using CashMarket_Entities.Context;
using CashMarket_Entities.Enums;
using CashMarket_Entities.Models.Admin;
using Newtonsoft.Json;

namespace CashMarket_AdminApi.Controllers
{
    [Route("api/Admin/Dashboard")]
    public class DashboardController : BaseController
    {
        [HttpPost("GetDashboard")]
        public async Task<IActionResult> GetDashboard()
        {
            var jsonInput = Request.HasFormContentType ? GetJson(Request.Form) : await new StreamReader(Request.Body).ReadToEndAsync();
            DashboardOutput output = new DashboardOutput();
            try
            {
                var input = JsonConvert.DeserializeObject<DashboardInput>(jsonInput);
                if (input != null)
                {
                    var result = CashMarket_Entities.Models.Reports.ReportHelper.GetDashboardReport(
                            input.DateFrom ?? DateTime.Today, input.DateTo ?? DateTime.Today, DbContextFactory.GetProviderConnectionStrings());
                    output.Data.TotalDeposit = result.TotalDeposit;
                    output.Data.TotalWithdrawal = result.TotalWithdrawal;
                    output.Data.Balance = result.Balance;
                    output.Data.TotalRegistered = result.TotalRegistered;
                    output.Data.TotalActive = result.TotalActive;
                    output.Data.TotalWinLose = result.TotalWinLose;
                    output.Data.TotalTurnover = result.TotalTurnover;

                    var banks = BankAccountBalance.GetBankAccounts();
                    foreach (BankAccountBalance bankAccount in banks)
                    {
                        output.Data.Banks.Add(new BankData()
                        {
                            Balance = bankAccount.Balance,
                            AccountName = bankAccount.BankName,
                            AccountNumber = bankAccount.BankNumber,
                            BankCode = bankAccount.BankName
                        });
                    }
                    output.Response.ErrorCode = ErrorCodes.Success;
                }
                else output.Response.ErrorCode = ErrorCodes.InvalidParameter;
            }
            catch (Exception e)
            {
                SetException(e, output.Response);
            }

            return Result(output);
        }
    }
}
