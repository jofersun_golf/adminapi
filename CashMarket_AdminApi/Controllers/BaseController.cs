﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;
using System.Net;
using System.Reflection;
using CashMarket_AdminApi.Models.Exception;
using CashMarket_Entities.Enums;
using CashMarket_Entities.Models;
using CashMarket_Entities.Models.Base;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CashMarket_AdminApi.Controllers
{
    [EnableCors("MyPolicy")]
    public class BaseController : ControllerBase
    {
        /// <summary>
        /// Get http status code from error code.
        /// </summary>
        /// <param name="errorCode"></param>
        /// <returns></returns>
        private static int GetStatusCode(ErrorCodes errorCode)
        {
            switch (errorCode)
            {
                case ErrorCodes.Success:
                    return (int) HttpStatusCode.OK;

                case ErrorCodes.InvalidParameter:
                case ErrorCodes.InvalidData:
                case ErrorCodes.AlreadyExist:
                case ErrorCodes.AlreadyProcessed:
                case ErrorCodes.InsufficientFund:
                    return (int) HttpStatusCode.BadRequest;

                case ErrorCodes.InvalidUserNameOrPassWord:
                case ErrorCodes.InvalidSession:
                    return (int) HttpStatusCode.Unauthorized;

                case ErrorCodes.InvalidWlCodeOrUserName:
                case ErrorCodes.NotFound:
                    return (int) HttpStatusCode.NotFound;
            }
            return (int) HttpStatusCode.InternalServerError;
        }

        internal string GetIpAddress()
        {
            string ip = Request.Headers["X-Forwarded-For"].ToString();
            if (string.IsNullOrEmpty(ip)) ip = Request.Headers["CF_CONNECTING_IP"].ToString();
            if (string.IsNullOrEmpty(ip)) ip = Request.HttpContext.Connection.RemoteIpAddress?.ToString();
            return ip;
        }

        /// <summary>
        /// Get json string from object without carriage/backslash char.
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>
        internal static string GetJson(IFormCollection collection)
        {
            if (collection == null) return string.Empty;
            List<string> dataList = new List<string>();
            foreach (KeyValuePair<string, StringValues> valuePair in collection)
            {
                if (!string.IsNullOrEmpty(valuePair.Value))
                    dataList.Add('"' + valuePair.Key + "\":\"" + valuePair.Value + '"');
            }
            string jsonResult = '{' + string.Join(",", dataList) + '}';
            return jsonResult;
        }

        /// <summary>
        /// Get description from enum description text.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        internal static string GetDescription(Enum value)
        {
            if (value == null) return string.Empty;
            FieldInfo fi = value.GetType().GetField(value.ToString());
            DescriptionAttribute attribute = fi?.GetCustomAttribute<DescriptionAttribute>();
            return !string.IsNullOrEmpty(attribute?.Description) ? attribute.Description : value.ToString();
        }

        internal static object GetDynamicValue(dynamic obj, string propertyName)
        {
            if (obj == null) return null;
            Type objType = obj.GetType();

            if (objType == typeof(ExpandoObject))
            {
                return !((IDictionary<string, object>)obj).TryGetValue(propertyName, out var result) ? null : result;
            }
            if (objType == typeof(JArray))
            {
                if (obj.Count > 0)
                {
                    var propValue = ((JObject)obj[0]).GetValue(propertyName, StringComparison.CurrentCultureIgnoreCase);
                    return propValue?.Value<JValue>()?.Value;
                }
            }
            if (objType == typeof(JObject))
            {
                    var propValue = ((JObject) obj).GetValue(propertyName, StringComparison.CurrentCultureIgnoreCase);
                    return propValue?.Value<JValue>()?.Value;
            }

            var property = objType.GetProperty(propertyName);
            if (property == null) return null;
            return property.GetValue(obj);
        }

        internal ContentResult Result(BaseOutput output)
        {
            return new ContentResult
            {
                Content = JsonConvert.SerializeObject(output),
                ContentType = "application/json",
                StatusCode = GetStatusCode(output.Response.ErrorCode)
            };
        }

        internal void SetException(Exception e, Response response)
        {
            response.ErrorCode = ErrorCodes.InternalError;
            if (e.InnerException is Npgsql.NpgsqlException npgsqlException)
            {
                if (!string.IsNullOrEmpty(npgsqlException.SqlState) && npgsqlException.SqlState.Equals("23505")) response.ErrorCode = ErrorCodes.AlreadyExist;
            }
            else if (e is InvalidParameterException) response.ErrorCode = ErrorCodes.InvalidParameter;

            response.ErrorMessage = e.InnerException?.Message ?? e.Message;
        }
    }
}