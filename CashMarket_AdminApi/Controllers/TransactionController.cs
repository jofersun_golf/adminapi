﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CashMarket_AdminApi.Models.Admin;
using CashMarket_AdminApi.Models.Exception;
using CashMarket_AdminApi.Models.Transaction;
using CashMarket_Entities.Enums;
using CashMarket_Entities.Models;
using CashMarket_Entities.Models.Admin;
using CashMarket_Entities.Models.Base;
using CashMarket_Entities.Models.Members;
using Newtonsoft.Json;

namespace CashMarket_AdminApi.Controllers
{
    [Route("api/Admin/Transaction")]
    public class TransactionController : BaseController
    {
        [HttpPost("GetTransactionList")]
        public async Task<IActionResult> GetTransactionList()
        {
            var jsonInput = Request.HasFormContentType ? GetJson(Request.Form) : await new StreamReader(Request.Body).ReadToEndAsync();
            var output = new BaseOutput<TransactionListOutput>();
            try
            {
                var input = JsonConvert.DeserializeObject<TransactionListInput>(jsonInput);
                if (input != null)
                {
                    string[] wlCodes = null;
                    string[] userNames = null;
                    if (input.Member != null)
                    {
                        wlCodes = input.Member.GroupBy(a => a.WhiteLabelCode).Select(a => a.Key).ToArray();
                        userNames = input.Member.GroupBy(a => a.UserName).Select(a => a.Key).ToArray();
                    }
                    var transactionRequests = TransactionRequest.GetTransactionRequests(input.Page, input.Size, input.Sort, wlCodes, userNames,
                        input.Request, input.Approval).ToList();
                    var wlGroup = transactionRequests.GroupBy(a => a.WhiteLabelCode).Select(a => a.Key.ToLower()).ToArray();
                    var statusTags = MemberStatusTag.GetMemberStatusTags().ToList();
                    var wallets = MemberWallet.GetMemberWallets(wlGroup).ToList();
                    var wlBanks = WhiteLabelBank.GetBanks(1, 0).ToList();
                    var memberBanks = MemberBank.GetMemberBanks();

                    foreach (TransactionRequest request in transactionRequests)
                    {
                        var method = ((Request) request.RequestType).ToString();
                        switch (request.RequestType)
                        {
                            case (short) CashMarket_Entities.Enums.Request.Deposit:
                            case (short) CashMarket_Entities.Enums.Request.Addition:
                            {
                                var wlBank = wlBanks.FirstOrDefault(a => a.BankNumber.Equals(request.RecipientAccountNumber, StringComparison.CurrentCultureIgnoreCase) &&
                                                                         a.BankAccountName.Equals(request.RecipientAccountHolderName, StringComparison.CurrentCultureIgnoreCase));
                                if (wlBank != null)
                                {
                                    string number = wlBank.BankNumber?.Length > 4
                                        ? wlBank.BankNumber.Substring(wlBank.BankNumber.Length - 4)
                                        : wlBank.BankNumber ?? string.Empty;
                                    method += " [" + wlBank.BankName + " - " + number + "]";
                                }
                                break;
                            }
                            case (short) CashMarket_Entities.Enums.Request.Withdraw:
                            case (short) CashMarket_Entities.Enums.Request.Subs:
                            {
                                var mBank = memberBanks.FirstOrDefault(a => a.AccountNumber.Equals(request.RecipientAccountNumber, StringComparison.CurrentCultureIgnoreCase) &&
                                                                            a.AccountNumber.Equals(request.RecipientAccountNumber, StringComparison.CurrentCultureIgnoreCase));
                                if (mBank != null)
                                {
                                    string number = mBank.AccountNumber?.Length > 4
                                        ? mBank.AccountNumber.Substring(mBank.AccountNumber.Length - 4)
                                        : mBank.AccountNumber ?? string.Empty;
                                    method += " [" + mBank.BankName + " - " + number + "]";
                                }
                                break;
                            }
                        }

                        Models.Transaction.Transaction transaction = new Models.Transaction.Transaction()
                        {
                            Id = request.Id,
                            Date = request.UpdateTime ?? request.CreateTime ?? DateTime.MinValue,
                            Serial = "masih belom tahu, harus tanya",
                            Member = request.MemberUserName,
                            Name = request.MemberUserName,
                            Method = method,
                            Status = ((Approval) request.ApprovalStatus).ToString(),
                            Credit = (request.RequestType == (short) CashMarket_Entities.Enums.Request.Addition ||
                                      request.RequestType == (short) CashMarket_Entities.Enums.Request.Deposit)
                                ? request.Amount
                                : 0,
                            Debit = (request.RequestType == (short) CashMarket_Entities.Enums.Request.Subs ||
                                     request.RequestType == (short) CashMarket_Entities.Enums.Request.Withdraw)
                                ? request.Amount
                                : 0,
                            Balance = wallets.FirstOrDefault(a =>
                                a.Member.UserName.Equals(request.MemberUserName,
                                    StringComparison.CurrentCultureIgnoreCase))?.Balance ?? 0,
                            Processing = request.Transactions.FirstOrDefault()?.CreateBy
                        };
                        var memberStatusTags = statusTags.Where(a =>
                                a.WhiteLabelCode.Equals(request.WhiteLabelCode, StringComparison.CurrentCultureIgnoreCase)
                                && a.UserName.Equals(request.MemberUserName, StringComparison.CurrentCultureIgnoreCase)).ToList();
                        foreach (MemberStatusTag msTag in memberStatusTags)
                        {
                            transaction.StatusTags.Add(new StatusTagData()
                            {
                                WhiteLabelCode = msTag.WhiteLabelCode,
                                ColorCode = msTag.StatusTag.ColorCode,
                                TagCode = msTag.StatusTag.TagCode,
                                TagName = msTag.StatusTag.TagName,
                            });
                        }
                        output.Data.Transactions.Add(transaction);
                    }

                    output.Data.TotalCredit = transactionRequests
                        .Where(a => a.RequestType == (short) CashMarket_Entities.Enums.Request.Addition ||
                                    a.RequestType == (short) CashMarket_Entities.Enums.Request.Deposit)
                        .Sum(a => a.Amount);
                    output.Data.TotalDebit = transactionRequests
                        .Where(a => a.RequestType == (short)CashMarket_Entities.Enums.Request.Subs ||
                                    a.RequestType == (short)CashMarket_Entities.Enums.Request.Withdraw)
                        .Sum(a => a.Amount);
                    output.Response.ErrorCode = ErrorCodes.Success;
                }
                else output.Response.ErrorCode = ErrorCodes.InvalidParameter;
            }
            catch (Exception e)
            {
                SetException(e, output.Response);
            }

            return Result(output);
        }

        [HttpPost("RequestTransaction")]
        public async Task<IActionResult> RequestTransaction()
        {
            var jsonInput = Request.HasFormContentType ? GetJson(Request.Form) : await new StreamReader(Request.Body).ReadToEndAsync();
            BaseOutput output = new BaseOutput();
            try
            {
                var input = JsonConvert.DeserializeObject<RequestTransactionInput>(jsonInput);
                if (input != null)
                {
                    if (!input.Validate()) throw new InvalidParameterException(input.ErrorMessage);
                    if (!input.Validate(output)) return Result(output);

                    var request = new TransactionRequest()
                    {
                        WhiteLabelCode = input.Transaction.WhiteLabelCode,
                        MemberUserName = input.Transaction.UserName,
                        PrefixUserName = input.Transaction.WhiteLabelCode,
                        Amount = input.Transaction.Amount,
                        RequestData = jsonInput,
                        SenderAccountHolderName = input.Transaction.SenderAccountHolder,
                        SenderAccountNumber = input.Transaction.SenderAccountNumber,
                        RecipientAccountHolderName = input.Transaction.RecipientAccountHolder,
                        RecipientAccountNumber = input.Transaction.RecipientAccountNumber,
                        RequestType = (short) input.Transaction.RequestType,
                        ApprovalStatus = (short) Approval.Pending,
                        CreateBy = input.UserLogin
                    };
                    if (request.Add()) output.Response.ErrorCode = ErrorCodes.Success;
                }
                else output.Response.ErrorCode = ErrorCodes.InvalidParameter;
            }
            catch (Exception e)
            {
                SetException(e, output.Response);
            }

            return Result(output);
        }

        [HttpPost("ProcessTransaction")]
        public async Task<IActionResult> ProcessTransaction()
        {
            var jsonInput = Request.HasFormContentType ? GetJson(Request.Form) : await new StreamReader(Request.Body).ReadToEndAsync();
            BaseOutput output = new BaseOutput();
            try
            {
                var input = JsonConvert.DeserializeObject<ProcessTransactionInput>(jsonInput);
                if (input != null)
                {
                    if (!input.Validate()) throw new InvalidParameterException(input.ErrorMessage);

                    var adminContext = new cashmarket_adminContext();
                    var request = await adminContext.TransactionRequests.FindAsync(input.Transaction.Id);
                    if (request == null || !request.WhiteLabelCode.Equals(input.Transaction.WhiteLabelCode, StringComparison.CurrentCultureIgnoreCase)
                                        || !request.MemberUserName.Equals(input.Transaction.UserName, StringComparison.CurrentCultureIgnoreCase)
                                        || !request.Amount.Equals(input.Transaction.Amount))
                    {
                        output.Response.ErrorCode = ErrorCodes.InvalidData;
                        output.Response.ErrorMessage = "Invalid data transaction";
                        return Result(output);
                    }
                    if (request.ApprovalStatus != (short) Approval.Pending)
                    {
                        output.Response.ErrorCode = ErrorCodes.AlreadyProcessed;
                        return Result(output);
                    }

                    var membersContext = new cashmarket_membersContext();
                    var wallet = membersContext.MemberWallets.FirstOrDefault(a =>
                        a.Member.WhiteLabelCode.ToLower().Equals(request.WhiteLabelCode.ToLower()) &&
                        a.Member.UserName.ToLower().Equals(request.MemberUserName.ToLower()));
                    if (wallet == null)
                    {
                        output.Response.ErrorCode = ErrorCodes.InvalidWlCodeOrUserName;
                        return Result(output);
                    }

                    decimal startBalance = wallet.Balance;
                    decimal endBalance;
                    string accountNumber;
                    string accountName;
                    TransactionType transactionType;
                    switch (request.RequestType)
                    {
                        case (short) CashMarket_Entities.Enums.Request.Withdraw:
                        case (short) CashMarket_Entities.Enums.Request.Subs:
                            if (wallet.Balance - request.Amount < 0)
                            {
                                output.Response.ErrorCode = ErrorCodes.InsufficientFund;
                    
                            }
                            endBalance = wallet.Balance - request.Amount;
                            wallet.Balance -= request.Amount;
                            accountNumber = request.SenderAccountNumber;
                            accountName = request.SenderAccountHolderName;
                            transactionType = TransactionType.Credit;
                            break;
                        case (short) CashMarket_Entities.Enums.Request.Deposit:
                        case (short) CashMarket_Entities.Enums.Request.Addition:
                            endBalance = wallet.Balance + request.Amount;
                            wallet.Balance += request.Amount;
                            accountNumber = request.RecipientAccountNumber;
                            accountName = request.RecipientAccountHolderName;
                            transactionType = TransactionType.Debit;
                            break;
                        default:
                            output.Response.ErrorCode = ErrorCodes.InvalidData;
                            output.Response.ErrorMessage = input.ErrorMessage;
                            return Result(output);
                    }

                    var wlBank = adminContext.WhiteLabelBanks.FirstOrDefault(a =>
                        a.BankNumber.ToLower().Equals(accountNumber.ToLower()) &&
                        a.BankAccountName.ToLower().Equals(accountName.ToLower()));
                    if (wlBank == null)
                    {
                        output.Response.ErrorCode = ErrorCodes.InvalidData;
                        return Result(output);
                    }

                    var transaction = new CashMarket_Entities.Models.Admin.Transaction()
                    {
                        TransactionRequestId = request.Id,
                        Amount = request.Amount,
                        StartBalance = startBalance,
                        EndBalance = endBalance,
                        Remark = input.Transaction.Remark,
                        CreateTime = DateTime.Now,
                        CreateBy = input.UserLogin,
                        Status = 1
                    };
                    await adminContext.Transactions.AddAsync(transaction);

                    request.ApprovalStatus = (short) input.Transaction.ApprovalStatus;
                    request.UpdateBy = input.UserLogin;

                    if (await adminContext.SaveChangesAsync() < 1 || await membersContext.SaveChangesAsync() < 1) throw new Exception(GetDescription(ErrorCodes.UnableSave));

                    string transactionString = JsonConvert.SerializeObject(transaction, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore});
                    var baLog = new BankActionLog()
                    {
                        WhiteLabelCode = input.Transaction.WhiteLabelCode,
                        BankNumber = accountNumber,
                        ActionType = (short) transactionType,
                        ReffType = (short) RefTable.Transaction,
                        ReffData = transactionString,
                        ReffId = transaction.Id,
                        Amount = request.Amount,
                        BankName = wlBank.BankName ?? accountName,
                        CreateBy = input.UserLogin
                    };
                    await adminContext.BankActionLogs.AddAsync(baLog);

                    if (await adminContext.SaveChangesAsync() < 1) throw new Exception(GetDescription(ErrorCodes.UnableSave));

                    output.Response.ErrorCode = ErrorCodes.Success;
                }
                else output.Response.ErrorCode = ErrorCodes.InvalidParameter;
            }
            catch (Exception e)
            {
                SetException(e, output.Response);
            }

            return Result(output);
        }
    }
}
