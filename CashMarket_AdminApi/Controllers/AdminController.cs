﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CashMarket_AdminApi.Models;
using CashMarket_AdminApi.Models.Admin;
using CashMarket_AdminApi.Models.Base;
using CashMarket_AdminApi.Models.Category;
using CashMarket_AdminApi.Models.Exception;
using CashMarket_AdminApi.Models.Role;
using CashMarket_Entities.Enums;
using CashMarket_Entities.Models;
using CashMarket_Entities.Models.Admin;
using CashMarket_Entities.Models.Base;
using Newtonsoft.Json;
using CashMarket_Entities.Utilities;
using User = CashMarket_Entities.Models.Admin.User;

namespace CashMarket_AdminApi.Controllers
{
    [Route("api/Admin/Admin")]
    public class AdminController : BaseController
    {
        [HttpPost("GetUserList")]
        public async Task<IActionResult> GetUserList()
        {
            var jsonInput = Request.HasFormContentType ? GetJson(Request.Form) : await new StreamReader(Request.Body).ReadToEndAsync();
            UserListOutput output = new UserListOutput();
            try
            {
                var input = JsonConvert.DeserializeObject<UserListInput>(jsonInput);
                if (input != null)
                {
                    var users = CashMarket_Entities.Models.Admin.User.GetUsers(input.Page, input.Size, input.Sort, input.KeyWord, input.Status);
                    foreach (User user in users)
                    {
                        output.Data.Add(new Models.Admin.User()
                        {
                            Name = user.UserName,
                            FullName = user.FullName,
                            Role = user.UserRoles.FirstOrDefault()?.Role.RoleDesc,
                            Status = GetDescription((PersonStatus) user.Status)
                        });
                    }
                    output.Response.ErrorCode = ErrorCodes.Success;
                }
                else output.Response.ErrorCode = ErrorCodes.InvalidParameter;
            }
            catch (Exception e)
            {
                SetException(e, output.Response);
            }

            return Result(output);
        }

        [HttpPost("GetRoleList")]
        public async Task<IActionResult> GetRoleList()
        {
            var jsonInput = Request.HasFormContentType ? GetJson(Request.Form) : await new StreamReader(Request.Body).ReadToEndAsync();
            RoleListOutput output = new RoleListOutput();
            try
            {
                var input = JsonConvert.DeserializeObject<RoleListInput>(jsonInput);
                if (input != null)
                {
                    var roles = Role.GetRoles(input.Page, input.Size, input.Sort, input.KeyWord, input.Status, input.RoleId);
                    if (roles != null) output.Data.AddRange(roles);
                    output.Response.ErrorCode = ErrorCodes.Success;
                }
                else output.Response.ErrorCode = ErrorCodes.InvalidParameter;
            }
            catch (Exception e)
            {
                SetException(e, output.Response);
            }

            return Result(output);
        }

        [HttpPost("GetStatusTagList")]
        public async Task<IActionResult> GetStatusTagList()
        {
            var jsonInput = Request.HasFormContentType ? GetJson(Request.Form) : await new StreamReader(Request.Body).ReadToEndAsync();
            StatusTagOutput output = new StatusTagOutput();
            try
            {
                var input = JsonConvert.DeserializeObject<ListInput>(jsonInput);
                if (input != null)
                {
                    var statusTags = StatusTag.GetStatusTags(input.Page, input.Size, input.Sort, input.KeyWord);
                    foreach (StatusTag tag in statusTags)
                    {
                        output.Data.Add(new StatusTagData()
                        {
                            Id = tag.Id,
                            WhiteLabelCode = tag.WhiteLabelCode,
                            ColorCode = tag.ColorCode,
                            TagCode = tag.TagCode,
                            TagName = tag.TagName
                        });
                    }
                    output.Response.ErrorCode = ErrorCodes.Success;
                }
                else output.Response.ErrorCode = ErrorCodes.InvalidParameter;
            }
            catch (Exception e)
            {
                SetException(e, output.Response);
            }

            return Result(output);
        }

        [HttpPost("SaveMemberStatusTag")]
        public async Task<IActionResult> SaveMemberStatusTag()
        {
            var jsonInput = Request.HasFormContentType ? GetJson(Request.Form) : await new StreamReader(Request.Body).ReadToEndAsync();
            BaseOutput output = new BaseOutput();
            try
            {
                var input = JsonConvert.DeserializeObject<MemberStatusTagInput>(jsonInput);
                if (input != null)
                {
                    if (!input.Validate()) throw new InvalidParameterException(input.ErrorMessage);

                    MemberStatusTag memberTag;
                    switch (input.Action)
                    {
                        case Constants.Add:
                            memberTag = new MemberStatusTag
                            {
                                Id = 0,
                                WhiteLabelCode = input.MemberStatusTag.WhiteLabelCode,
                                PrefixUserName = input.MemberStatusTag.PrefixUserName,
                                UserName = input.MemberStatusTag.UserName,
                                StatusTagId = input.MemberStatusTag.StatusTagId,
                                Status = 1,
                                CreateBy = input.UserLogin
                            };
                            if (memberTag.Add()) output.Response.ErrorCode = ErrorCodes.Success;
                            break;
                        case Constants.Update:
                            memberTag = MemberStatusTag.GetById(input.MemberStatusTag.Id);
                            if (memberTag != null)
                            {
                                memberTag.WhiteLabelCode = input.MemberStatusTag.WhiteLabelCode;
                                memberTag.PrefixUserName = input.MemberStatusTag.PrefixUserName;
                                memberTag.UserName = input.MemberStatusTag.UserName;
                                memberTag.StatusTagId = input.MemberStatusTag.StatusTagId;
                                memberTag.UpdateBy = input.UserLogin;
                                if (memberTag.Update()) output.Response.ErrorCode = ErrorCodes.Success;
                            }
                            else output.Response.ErrorCode = ErrorCodes.NotFound;
                            break;
                        case Constants.Remove:
                            memberTag = MemberStatusTag.GetById(input.MemberStatusTag.Id);
                            if (memberTag != null)
                            {
                                if (memberTag.Remove()) output.Response.ErrorCode = ErrorCodes.Success;
                            }
                            else output.Response.ErrorCode = ErrorCodes.NotFound;
                            break;
                        default:
                            output.Response.ErrorCode = ErrorCodes.InvalidParameter;
                            break;
                    }
                }
                else output.Response.ErrorCode = ErrorCodes.InvalidParameter;
            }
            catch (Exception e)
            {
                SetException(e, output.Response);
            }
            
            return Result(output);
        }

        [HttpPost("SaveUser")]
        public async Task<IActionResult> SaveUser()
        {
            var jsonInput = Request.HasFormContentType ? GetJson(Request.Form) : await new StreamReader(Request.Body).ReadToEndAsync();
            BaseOutput output = new BaseOutput();
            try
            {
                var input = JsonConvert.DeserializeObject<UserInput>(jsonInput);
                if (input != null)
                {
                    if (!input.Validate()) throw new InvalidParameterException(input.ErrorMessage);

                    User user;
                    switch (input.Action)
                    {
                        case Constants.Add:
                            string salt = Guid.NewGuid().ToString();
                            user = new User
                            {
                                UserName = input.User.UserName,
                                FirstName = input.User.FirstName,
                                LastName = input.User.LastName,
                                FullName = input.User.FullName,
                                Salt = salt,
                                PasswordHash = HashMethod.HmacSha256Digest(input.User.PassWord, salt),
                                EmailAddress = input.User.Email,
                                Verified = false,
                                WrongCount = 0,
                                Status = (short)PersonStatus.Activated,
                                CreateBy = input.UserLogin
                            };
                            if (user.Add()) output.Response.ErrorCode = ErrorCodes.Success;
                            break;
                        case Constants.Update:
                            user = CashMarket_Entities.Models.Admin.User.GetById(input.User.Id);
                            if (user != null)
                            {
                                user.UserName = input.User.UserName;
                                user.FirstName = input.User.FirstName;
                                user.LastName = input.User.LastName;
                                user.FullName = input.User.FullName;
                                user.PasswordHash = HashMethod.HmacSha256Digest(input.User.PassWord, user.Salt);
                                user.EmailAddress = input.User.Email;
                                user.Verified = false;
                                user.WrongCount = 0;
                                if (user.Update()) output.Response.ErrorCode = ErrorCodes.Success;
                            }
                            else output.Response.ErrorCode = ErrorCodes.NotFound;
                            break;
                        case Constants.Remove:
                            user = CashMarket_Entities.Models.Admin.User.GetById(input.User.Id);
                            if (user != null)
                            {
                                if (user.Remove()) output.Response.ErrorCode = ErrorCodes.Success;
                            }
                            else output.Response.ErrorCode = ErrorCodes.NotFound;
                            break;
                        default:
                            output.Response.ErrorCode = ErrorCodes.InvalidParameter;
                            break;
                    }
                }
                else output.Response.ErrorCode = ErrorCodes.InvalidParameter;
            }
            catch (Exception e)
            {
                SetException(e, output.Response);
            }

            return Result(output);
        }

        [HttpPost("SaveRole")]
        public async Task<IActionResult> SaveRole()
        {
            var jsonInput = Request.HasFormContentType ? GetJson(Request.Form) : await new StreamReader(Request.Body).ReadToEndAsync();
            RoleListOutput output = new RoleListOutput();
            try
            {
                var input = JsonConvert.DeserializeObject<Role>(jsonInput);
                if (input != null)
                {
                    //var roles = CashMarket_Entities.Models.Admin.Role.GetRoles(input.Page, input.Size, input.Sort, input.KeyWord, input.Status, input.RoleId);
                    //if (roles != null) output.Response.Roles.AddRange(roles);
                    output.Response.ErrorCode = ErrorCodes.Success;
                }
                else output.Response.ErrorCode = ErrorCodes.InvalidParameter;
            }
            catch (Exception e)
            {
                SetException(e, output.Response);
            }

            return Result(output);
        }

        [HttpPost("SaveStatusTag")]
        public async Task<IActionResult> SaveStatusTag()
        {
            var jsonInput = Request.HasFormContentType ? GetJson(Request.Form) : await new StreamReader(Request.Body).ReadToEndAsync();
            BaseOutput output = new BaseOutput();
            try
            {
                var input = JsonConvert.DeserializeObject<StatusTagInput>(jsonInput);
                if (input != null)
                {
                    if (!input.Validate()) throw new InvalidParameterException(input.ErrorMessage);

                    StatusTag tag;
                    switch (input.Action)
                    {
                        case Constants.Add:
                            tag = new StatusTag
                            {
                                Id = 0,
                                WhiteLabelCode = input.StatusTag.WhiteLabelCode,
                                ColorCode = input.StatusTag.ColorCode,
                                TagCode = input.StatusTag.TagCode,
                                TagName = input.StatusTag.TagName,
                                Status = 1,
                                CreateBy = input.UserLogin
                            };
                            if (tag.Add()) output.Response.ErrorCode = ErrorCodes.Success;
                            break;
                        case Constants.Update:
                            tag = StatusTag.GetById(input.StatusTag.Id);
                            if (tag != null)
                            {
                                tag.WhiteLabelCode = input.StatusTag.WhiteLabelCode;
                                tag.ColorCode = input.StatusTag.ColorCode;
                                tag.TagCode = input.StatusTag.ColorCode;
                                tag.TagName = input.StatusTag.TagName;
                                tag.UpdateBy = input.UserLogin;
                                if (tag.Update()) output.Response.ErrorCode = ErrorCodes.Success;
                            }
                            else output.Response.ErrorCode = ErrorCodes.NotFound;
                            break;
                        case Constants.Remove:
                            tag = StatusTag.GetById(input.StatusTag.Id);
                            if (tag != null)
                            {
                                if (tag.Remove()) output.Response.ErrorCode = ErrorCodes.Success;
                            }
                            else output.Response.ErrorCode = ErrorCodes.NotFound;
                            break;
                        default:
                            output.Response.ErrorCode = ErrorCodes.InvalidParameter;
                            break;
                    }
                }
                else output.Response.ErrorCode = ErrorCodes.InvalidParameter;
            }
            catch (Exception e)
            {
                SetException(e, output.Response);
            }

            return Result(output);
        }

        [HttpPost("SaveCategory")]
        public async Task<IActionResult> SaveCategory()
        {
            var jsonInput = Request.HasFormContentType ? GetJson(Request.Form) : await new StreamReader(Request.Body).ReadToEndAsync();
            BaseOutput output = new BaseOutput();
            try
            {
                var input = JsonConvert.DeserializeObject<ProductCategoryInput>(jsonInput);
                if (input != null)
                {
                    if (!input.Validate()) throw new InvalidParameterException(input.ErrorMessage);
                    if (!input.Validate(output)) return Result(output);

                    ProductCategory prodCategory;
                    switch (input.Action)
                    {
                        case Constants.Add:
                            prodCategory = new ProductCategory
                            {
                                WhiteLabelCode = input.ProductCategory.WhiteLabelCode,
                                ProviderCode = input.ProductCategory.ProviderCode,
                                ProductCode = input.ProductCategory.ProductCode,
                                ProductName = input.ProductCategory.ProductName,
                                CategoryCode = input.ProductCategory.CategoryCode,
                                CategoryName = input.ProductCategory.CategoryName,
                                ItemSort = input.ProductCategory.ItemSort,
                                Status = 1,
                                CreateBy = input.UserLogin
                            };
                            if (prodCategory.Add()) output.Response.ErrorCode = ErrorCodes.Success;
                            break;
                        case Constants.Update:
                            prodCategory = ProductCategory.GetById(input.ProductCategory.Id);
                            if (prodCategory != null)
                            {
                                prodCategory.WhiteLabelCode = input.ProductCategory.WhiteLabelCode;
                                prodCategory.ProviderCode = input.ProductCategory.ProviderCode;
                                prodCategory.ProductCode = input.ProductCategory.ProductCode;
                                prodCategory.ProductName = input.ProductCategory.ProductName;
                                prodCategory.CategoryCode = input.ProductCategory.CategoryCode;
                                prodCategory.CategoryName = input.ProductCategory.CategoryName;
                                prodCategory.ItemSort = input.ProductCategory.ItemSort;
                                if (prodCategory.Update()) output.Response.ErrorCode = ErrorCodes.Success;
                            }
                            else output.Response.ErrorCode = ErrorCodes.NotFound;
                            break;
                        case Constants.Remove:
                            prodCategory = ProductCategory.GetById(input.ProductCategory.Id);
                            if (prodCategory != null)
                            {
                                if (prodCategory.Remove()) output.Response.ErrorCode = ErrorCodes.Success;
                            }
                            else output.Response.ErrorCode = ErrorCodes.NotFound;
                            break;
                        default:
                            output.Response.ErrorCode = ErrorCodes.InvalidParameter;
                            break;
                    }
                }
                else output.Response.ErrorCode = ErrorCodes.InvalidParameter;
            }
            catch (Exception e)
            {
                SetException(e, output.Response);
            }

            return Result(output);
        }

        [HttpPost("GetAdminLogList")]
        public async Task<IActionResult> GetAdminLogList()
        {
            var jsonInput = Request.HasFormContentType ? GetJson(Request.Form) : await new StreamReader(Request.Body).ReadToEndAsync();
            AdminLogOutput output = new AdminLogOutput();
            try
            {
                var input = JsonConvert.DeserializeObject<AdminLogInput>(jsonInput);
                if (input != null)
                {
                    var tableLogs = TableLogs.GetTableLogs(input.Page, input.Size, null, input.TableName,
                        input.DateFrom, input.DateTo);
                    var users = CashMarket_Entities.Models.Admin.User.GetUsers().ToList();
                    foreach (TableLogs tableLog in tableLogs)
                    {
                        dynamic logDatas = JsonConvert.DeserializeObject(tableLog.NewData);
                        if (logDatas?.Count > 0)
                        {
                            string administrator;
                            DateTime dateTime;
                            object dateTemp;
                            dynamic logData = JsonConvert.DeserializeObject(tableLog.NewData);
                            switch (tableLog.TableName)
                            {
                                case "login_histories":
                                    administrator = GetDynamicValue(logData, "user_name")?.ToString();
                                    dateTemp = GetDynamicValue(logData, "login_time") ?? GetDynamicValue(logData, "logout_time");
                                    if (dateTemp == null || !DateTime.TryParse(dateTemp.ToString(), out dateTime))
                                    {
                                        dateTime = tableLog.CreateTime ?? DateTime.MinValue;
                                    }
                                    break;
                                case "users":
                                    administrator = GetDynamicValue(logData, "update_by") ?? GetDynamicValue(logData, "create_by") ?? GetDynamicValue(logData, "user_name");
                                    dateTemp = GetDynamicValue(logData, "update_time") ?? GetDynamicValue(logData, "create_time");
                                    if (dateTemp == null || !DateTime.TryParse(dateTemp.ToString(), out dateTime))
                                    {
                                        dateTime = tableLog.CreateTime ?? DateTime.MinValue;
                                    }
                                    break;
                                case "product_categories":
                                case "transaction_request":
                                default:
                                    administrator = GetDynamicValue(logData, "update_by") ?? GetDynamicValue(logData, "create_by");
                                    dateTemp = GetDynamicValue(logData, "update_time") ?? GetDynamicValue(logData, "create_time");
                                    if (dateTemp == null || !DateTime.TryParse(dateTemp.ToString(), out dateTime))
                                    {
                                        dateTime = tableLog.CreateTime ?? DateTime.MinValue;
                                    }
                                    break;
                            }
                            output.Data.Add(new AdminLogData()
                            {
                                DateTime = dateTime,
                                UserType = users.FirstOrDefault(a=>a.UserName.Equals(administrator, StringComparison.InvariantCultureIgnoreCase))?.UserRoles.FirstOrDefault()?.Role.RoleCode,
                                Administrator = administrator,
                                ActionName = tableLog.ActionName + " " + tableLog.TableName,
                                TableName = tableLog.TableName
                            });
                        }
                    }

                    output.Response.ErrorCode = ErrorCodes.Success;
                }
                else output.Response.ErrorCode = ErrorCodes.InvalidParameter;
            }
            catch (Exception e)
            {
                SetException(e, output.Response);
            }
            
            return Result(output);
        }
    }
}
