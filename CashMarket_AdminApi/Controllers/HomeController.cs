﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;
using System.Threading.Tasks;
using CashMarket_AdminApi.Models.Exception;
using CashMarket_AdminApi.Models.Home;
using CashMarket_AdminApi.Models.Home.LogIn;
using CashMarket_AdminApi.Models.Home.LogOut;
using CashMarket_Entities.Enums;
using CashMarket_Entities.Models;
using CashMarket_Entities.Models.Base;
using Newtonsoft.Json;

namespace CashMarket_AdminApi.Controllers
{
    [Route("api/Admin")]
    public class HomeController : BaseController
    {
        [HttpPost("Login")]
        public async Task<IActionResult> LogIn()
        {
            var jsonInput = Request.HasFormContentType ? GetJson(Request.Form) : await new StreamReader(Request.Body).ReadToEndAsync();
            var output = new BaseOutput<LogInOutput>();
            try
            {
                var input = JsonConvert.DeserializeObject<LogInInput>(jsonInput);
                if (input != null)
                {
                    if (!input.Validate()) throw new InvalidParameterException(input.ErrorMessage);
                    CashMarket_Entities.Models.Admin.User user = CashMarket_Entities.Models.Admin.User.LogIn(input.UserName, input.PassWord, GetIpAddress(), out var session);
                    if (user != null)
                    {
                        if (!string.IsNullOrEmpty(session))
                        {
                            output.Response.ErrorCode = ErrorCodes.Success;
                            output.Data.FullName = user.FullName;
                            output.Data.LastName = user.LastName;
                            output.Data.Session = session;
                            output.Data.UserName = user.UserName;
                        }
                        else if (user.Status != (int) PersonStatus.Activated) output.Response.ErrorMessage = "User has been " + GetDescription((PersonStatus) user.Status);
                        else output.Response.ErrorCode = ErrorCodes.InvalidUserNameOrPassWord;
                    }
                    else output.Response.ErrorCode = ErrorCodes.InvalidUserNameOrPassWord;
                }
                else output.Response.ErrorCode = ErrorCodes.InvalidParameter;
            }
            catch (Exception e)
            {
                SetException(e, output.Response);
            }

            return Result(output);
        }

        [HttpPost("Logout")]
        public async Task<IActionResult> LogOut()
        {
            var jsonInput = Request.HasFormContentType ? GetJson(Request.Form) : await new StreamReader(Request.Body).ReadToEndAsync();
            BaseOutput output = new BaseOutput();
            try
            {
                var input = JsonConvert.DeserializeObject<LogOutInput>(jsonInput);
                if (input != null)
                {
                    if (CashMarket_Entities.Models.Admin.User.LogOut(input.UserName)) output.Response.ErrorCode = ErrorCodes.Success;
                }
                else output.Response.ErrorCode = ErrorCodes.InvalidParameter;
            }
            catch (Exception e)
            {
                SetException(e, output.Response);
            }

            return Result(output);
        }

        [HttpPost("ValidateUserSession")]
        public async Task<IActionResult> ValidateUserSession()
        {
            var jsonInput = Request.HasFormContentType ? GetJson(Request.Form) : await new StreamReader(Request.Body).ReadToEndAsync();
            BaseOutput output = new BaseOutput();
            try
            {
                var input = JsonConvert.DeserializeObject<UserSessionInput>(jsonInput);
                if (input != null)
                {
                    if (!input.Validate()) throw new InvalidParameterException(input.ErrorMessage);
                    bool result = CashMarket_Entities.Models.Admin.User.ValidateUserSession(input.UserName, input.SessionCode);
                    output.Response.ErrorCode = result ? ErrorCodes.Success : ErrorCodes.InvalidSession;
                }
                else output.Response.ErrorCode = ErrorCodes.InvalidParameter;
            }
            catch (Exception e)
            {
                SetException(e, output.Response);
            }

            return Result(output);
        }
    }
}
