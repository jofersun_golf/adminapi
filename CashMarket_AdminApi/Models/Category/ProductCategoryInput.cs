﻿using System.ComponentModel.DataAnnotations;
using System.Linq;
using CashMarket_AdminApi.Models.Base;
using CashMarket_Entities.Enums;
using CashMarket_Entities.Models;
using CashMarket_Entities.Models.Base;
using CashMarket_Entities.Models.WhiteLabels;

namespace CashMarket_AdminApi.Models.Category
{
    public class ProductCategoryInput : SaveInput
    {
        public ProductCategoryData ProductCategory { get; set; }
        public override bool Validate() => base.Validate(ProductCategory);

        public bool Validate(BaseOutput output)
        {
            cashmarket_whitelabelsContext wlContext = new cashmarket_whitelabelsContext();
            var product = wlContext.WhiteLabelProducts.FirstOrDefault(a =>
                a.WhiteLabel.WhiteLabelCode.ToLower().Equals(this.ProductCategory.WhiteLabelCode.ToLower())
                && a.ProviderCode.ToLower().Equals(this.ProductCategory.ProviderCode.ToLower())
                && a.ProductCode.ToLower().Equals(this.ProductCategory.ProductCode.ToLower()));
            if (product != null && product.Status == 1) return true;
            output.Response.ErrorCode = ErrorCodes.InvalidData;
            output.Response.ErrorMessage = "Invalid data product";
            return false;
        }
    }
    
    public class ProductCategoryData
    {
        public int Id { get; set; }
        [RequiredAttribute]
        public string WhiteLabelCode { get; set; }
        [RequiredAttribute]
        public string ProviderCode { get; set; }
        [RequiredAttribute]
        public string ProductCode { get; set; }
        [RequiredAttribute]
        public string ProductName { get; set; }
        [RequiredAttribute]
        public string CategoryCode { get; set; }
        [RequiredAttribute]
        public string CategoryName { get; set; }
        public int? ItemSort { get; set; }
    }
}
