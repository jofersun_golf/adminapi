﻿namespace CashMarket_AdminApi.Models.Base
{
    public class ListInput
    {
        public ListInput()
        {
            Page = 1;
            Size = int.MaxValue;
        }
        public int Page { get; set; }
        public int Size { get; set; }
        public string Sort { get; set; }
        public string KeyWord { get; set; }
    }
}
