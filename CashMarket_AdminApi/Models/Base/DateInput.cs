﻿using System;

namespace CashMarket_AdminApi.Models.Base
{
    public abstract class DateListInput : ListInput
    {
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
    }
}
