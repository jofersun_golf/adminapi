﻿using System;

namespace CashMarket_AdminApi.Models.Base
{
    public abstract class RequestInput : ValidateInput
    {
        /// <summary>
        /// User who is currently logged in.
        /// </summary>
        public string UserLogin { get; set; }

        protected bool IsAddOrUpdate(string action)
        {
            return action.Equals(Constants.Add, StringComparison.CurrentCultureIgnoreCase) ||
                   action.Equals(Constants.Update, StringComparison.CurrentCultureIgnoreCase);
        }
        protected bool IsUpdateOrRemove(string action)
        {
            return action.Equals(Constants.Update, StringComparison.CurrentCultureIgnoreCase) ||
                   action.Equals(Constants.Remove, StringComparison.CurrentCultureIgnoreCase);
        }
    }
}
