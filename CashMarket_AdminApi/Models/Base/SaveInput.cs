﻿namespace CashMarket_AdminApi.Models.Base
{
    public abstract class SaveInput : RequestInput
    {
        public virtual string Action { get; set; }
    }
}
