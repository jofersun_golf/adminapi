﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace CashMarket_AdminApi.Models.Base
{
    public abstract class ValidateInput
    {
        protected ValidateInput()
        {
            ErrorList = new List<string>();
        }
        /// <summary>
        /// Error message after validation.
        /// </summary>
        public string ErrorMessage { get; protected set; }

        protected List<string> ErrorList { get; }

        /// <summary>
        /// Clear error message and error list.
        /// </summary>
        protected void ClearError()
        {
            ErrorMessage = string.Empty;
            ErrorList.Clear();
        }

        /// <summary>
        /// Validate for this object class.
        /// </summary>
        /// <returns></returns>
        public virtual bool Validate()
        {
            return Validate(this);
        }

        /// <summary>
        /// Validate for data object.
        /// </summary>
        /// <param name="data">Data which to be validated.</param>
        /// <returns></returns>
        public virtual bool Validate(object data)
        {
            ClearError();
            if (data != null)
            {
                foreach (PropertyInfo property in data.GetType().GetProperties())
                {
                    var attribute = property.GetCustomAttributes<RequiredAttribute>().FirstOrDefault();
                    if (attribute == null) continue;
                    object value = property.GetValue(data);
                    if ((value == null || string.IsNullOrEmpty(value.ToString())) ||
                        (value is Enum && !Enum.IsDefined(property.PropertyType, value)) ||
                        (value is DateTime dateValue && dateValue.Equals(DateTime.MinValue)) ||
                        (value is decimal and <= 0) ||
                        (value is int and 0))
                    {
                        ErrorList.Add("Invalid " + property.Name);
                    }
                }
            }
            else ErrorList.Add("Invalid data");

            ErrorMessage = string.Join("; ", ErrorList);
            return !ErrorList.Any();
        }
    }
}
