﻿using System.Collections.Generic;
using CashMarket_Entities.Models;
using CashMarket_Entities.Models.Base;

namespace CashMarket_AdminApi.Models.Role
{
    public class RoleListOutput : BaseOutput<List<CashMarket_Entities.Models.Admin.Role>>
    {
        public RoleListOutput()
        {
            Data = new List<CashMarket_Entities.Models.Admin.Role>();
        }
    }
}
