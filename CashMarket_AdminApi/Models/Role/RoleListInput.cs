﻿namespace CashMarket_AdminApi.Models.Role
{
    public class RoleListInput : Base.ListInput
    {
        public string RoleId { get; set; }
        public short Status { get; set; }
    }
}
