﻿using System.ComponentModel.DataAnnotations;
using CashMarket_AdminApi.Models.Base;

namespace CashMarket_AdminApi.Models.Home
{
    public class UserSessionInput : ValidateInput
    {
        [RequiredAttribute]
        public string UserName { get; set; }
        [RequiredAttribute]
        public string SessionCode { get; set; }
    }
}
