﻿namespace CashMarket_AdminApi.Models.Home.LogOut
{
    public class LogOutInput
    {
        public string UserName { get; set; }
    }
}
