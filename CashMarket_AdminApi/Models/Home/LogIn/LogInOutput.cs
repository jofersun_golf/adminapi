﻿using CashMarket_Entities.Models;

namespace CashMarket_AdminApi.Models.Home.LogIn
{
    public class LogInOutput
    {
        public string UserName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
        public string Session { get; set; }
    }
}
