﻿using System.ComponentModel.DataAnnotations;
using CashMarket_AdminApi.Models.Base;

namespace CashMarket_AdminApi.Models.Home.LogIn
{
    public class LogInInput : ValidateInput
    {
        [RequiredAttribute]
        public string UserName { get; set; }
        [RequiredAttribute]
        public string PassWord { get; set; }
    }
}
