﻿using System;
using System.ComponentModel.DataAnnotations;
using CashMarket_AdminApi.Models.Base;

namespace CashMarket_AdminApi.Models.Member
{
    public class MemberInput : SaveInput
    {
        public MemberInputData Member { get; set; }
        public override bool Validate() => base.Validate(Member);
    }

    public class MemberInputData
    {
        public int Id { get; set; }
        [RequiredAttribute]
        public string WhiteLabelCode { get; set; }
        [RequiredAttribute]
        public string UserName { get; set; }
        /// <summary>
        /// Raw password before encrypted.
        /// </summary>
        [RequiredAttribute]
        public string PassWord { get; set; }
        [RequiredAttribute]
        public string Email { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string FullName { get; set; }
        public short? Gender { get; set; }
        public string Contact { get; set; }
        public string Contact2 { get; set; }
        public string LastIp { get; set; }
        public string Referral { get; set; }
        public string CurrencyCode { get; set; }
        public string ReferralReference { get; set; }
    }
}
