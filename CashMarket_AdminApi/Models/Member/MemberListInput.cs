﻿using CashMarket_AdminApi.Models.Base;
using CashMarket_Entities.Enums;

namespace CashMarket_AdminApi.Models.Member
{
    public class MemberListInput : DateListInput
    {
        public PersonStatus? Status { get; set; }
    }
}
