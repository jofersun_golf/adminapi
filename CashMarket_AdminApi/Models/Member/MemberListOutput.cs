﻿using System;
using System.Collections.Generic;
using CashMarket_Entities.Models;
using CashMarket_Entities.Models.Base;

namespace CashMarket_AdminApi.Models.Member
{
    public class MemberListOutput : BaseOutput<List<MemberListData>>
    {
        public MemberListOutput()
        {
            Data = new List<MemberListData>();
        }
    }

    public class MemberListData
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string MemberTags { get; set; }
        public string FullName { get; set; }
        public string Channel { get; set; }
        public decimal MainWallet { get; set; }
        /// <summary>
        /// Number and sum of deposit approved.
        /// </summary>
        public string Deposit { get; set; }
        /// <summary>
        /// Number and sum of withdrawal approved.
        /// </summary>
        public string Withdrawal { get; set; }
        public string Contact { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string Email { get; set; }
        public DateTime? RegisteredAt { get; set; }
        public DateTime? LastDeposit { get; set; }
        public string Status { get; set; }
        public string IP { get; set; }
    }
}
