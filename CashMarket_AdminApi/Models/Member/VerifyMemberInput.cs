﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using CashMarket_AdminApi.Models.Base;

namespace CashMarket_AdminApi.Models.Member
{
    public class VerifyMemberInput : RequestInput
    {
        public VerifyMemberData Member { get; set; }
        public override bool Validate() => base.Validate(Member);
    }

    public class VerifyMemberData
    {
        [RequiredAttribute]
        public string WhiteLabelCode { get; set; }
        [RequiredAttribute]
        public string UserName { get; set; }
        [RequiredAttribute]
        public string Email { get; set; }
        [RequiredAttribute]
        public string Contact { get; set; }
    }
}
