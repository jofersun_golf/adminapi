﻿using System.ComponentModel.DataAnnotations;
using CashMarket_AdminApi.Models.Base;

namespace CashMarket_AdminApi.Models.Member
{
    public class AccountDetailsInput : RequestInput
    {
        public MemberAccountData MemberAccount { get; set; }
        public override bool Validate() => Validate(MemberAccount);
        }

    public class MemberAccountData
    {
        [RequiredAttribute]
        public int Id { get; set; }
        [RequiredAttribute]
        public string WhiteLabelCode { get; set; }
        [RequiredAttribute]
        public string UserName { get; set; }
    }
}
