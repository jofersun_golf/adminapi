﻿using System;
using System.Collections.Generic;
using CashMarket_AdminApi.Models.Admin;
using CashMarket_Entities.Enums;
using CashMarket_Entities.Models;

namespace CashMarket_AdminApi.Models.Member
{
    public class AccountDetailsOutput
    {
        public AccountDetailsOutput()
        {
            StatusTags = new List<StatusTagData>();
        }
        public AccountInformationData AccountInformation { get; set; }
        public List<StatusTagData> StatusTags { get; }
    }
    public class AccountInformationData
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string WhiteLabelCode { get; set; }
        public string FullName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public short? Gender { get; set; }
        public PersonStatus Status { get; set; }
        public string Email { get; set; }
        public string Contact { get; set; }
        public string Contact2 { get; set; }
    }
}
