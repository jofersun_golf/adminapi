﻿using System;

namespace CashMarket_AdminApi.Models.Dashboard
{
    public class DashboardInput
    {
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
    }
}
