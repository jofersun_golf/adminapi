﻿using System.Collections.Generic;
using CashMarket_Entities.Models;
using CashMarket_Entities.Models.Base;

namespace CashMarket_AdminApi.Models.Dashboard
{
    public class DashboardOutput : BaseOutput<DashboardDataOutput>
    {
    }

    public class DashboardDataOutput
    {
        public DashboardDataOutput()
        {
            Banks = new List<BankData>();
        }
        public decimal TotalDeposit { get; set; }
        public decimal TotalWithdrawal { get; set; }
        public decimal Balance { get; set; }
        public int TotalRegistered { get; set; }
        public int TotalActive { get; set; }
        public decimal TotalWinLose { get; set; }
        public decimal TotalTurnover { get; set; }

        public List<BankData> Banks { get; }
    }

    public class BankData
    {
        public string BankCode { get; set; }
        public string AccountName { get; set; }
        public string AccountNumber { get; set; }
        public decimal Balance { get; set; }
    }
}
