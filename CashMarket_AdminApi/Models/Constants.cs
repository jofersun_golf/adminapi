﻿namespace CashMarket_AdminApi.Models
{
    public class Constants
    {
        internal const string Add = "Add";
        internal const string Update = "Update";
        internal const string Remove = "Remove";

        internal const string InvalidName = "Invalid name";
    }
}
