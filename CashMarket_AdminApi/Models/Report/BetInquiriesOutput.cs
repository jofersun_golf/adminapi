﻿using System;
using System.Collections.Generic;
using CashMarket_Entities.Models;

namespace CashMarket_AdminApi.Models.Report
{
    public class BetInquiriesOutput
    {
        public BetInquiriesOutput()
        {
            BetInquiries = new List<BetInquiryData>();
        }
        public List<BetInquiryData> BetInquiries { get; }
        public decimal SubTotalStake { get; set; }
        public decimal SubTotalWinLose { get; set; }
        public decimal SubTotalRealBet { get; set; }
        public decimal GrandTotalStake { get; set; }
        public decimal GrandTotalWinLose { get; set; }
        public decimal GrandTotalRealBet { get; set; }
    }

    public class BetInquiryData
    {
        public DateTime? DateTime { get; set; }
        public string Member { get; set; }
        public string ProviderCode { get; set; }
        public string CategoryName { get; set; }
        public string Game { get; set; }
        public string ReferenceNo { get; set; }
        public string RoundId { get; set; }
        public string Selection { get; set; }
        public decimal Stake { get; set; }
        public decimal WinLose { get; set; }
        public decimal RealBet { get; set; }
        public string Result { get; set; }
    }
}
