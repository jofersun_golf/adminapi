﻿using System;
using System.Collections.Generic;
using CashMarket_Entities.Models;
using CashMarket_Entities.Models.Base;

namespace CashMarket_AdminApi.Models.Report
{
    public class CreditLogOutput : BaseOutput<List<CreditLogData>>
    {
        public CreditLogOutput()
        {
            Data = new List<CreditLogData>();
        }

    }

    public class CreditLogData
    {
        public DateTime DateTime { get; set; }
        public string FromWallet { get; set; }
        public string FromStatus { get; set; }
        public string ToWallet { get; set; }
        public string ToStatus { get; set; }
        public decimal Amount { get; set; }
        public string Status { get; set; }
        public decimal FromStart { get; set; }
        public decimal FromEnd { get; set; }
        public decimal ToStart { get; set; }
        public decimal ToEnd { get; set; }
        public string Action { get; set; }
    }
}
