﻿using CashMarket_AdminApi.Models.Base;

namespace CashMarket_AdminApi.Models.Report
{
    public class ReportInput : DateListInput
    {
        /// <summary>
        /// Array of combined of white label code and user name. Example: btdl_username
        /// </summary>
        public string[] PrefixUserName { get; set; }

        /// <summary>
        /// Array of provider code.
        /// </summary>
        public string[] ProviderCode { get; set; }

        /// <summary>
        /// Array of category code.
        /// </summary>
        public string[] CategoryCode { get; set; }

        /// <summary>
        /// Array of product code.
        /// </summary>
        public string[] ProductCode { get; set; }
    }
}
