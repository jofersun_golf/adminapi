﻿namespace CashMarket_AdminApi.Models.Report
{
    public class BetInquiriesInput : ReportInput
    {
        public string Keyword { get; set; }
    }
}
