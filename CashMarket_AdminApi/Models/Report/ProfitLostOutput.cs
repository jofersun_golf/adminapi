﻿using System.Collections.Generic;
using CashMarket_Entities.Models;

namespace CashMarket_AdminApi.Models.Report
{
    public class ProfitLostOutput
    {
        public ProfitLostOutput()
        {
            ProfitLost = new List<ProfitLostData>();
        }
        public List<ProfitLostData> ProfitLost { get; }
        public int TotalCount { get; set; }
        public decimal TotalTurnOver { get; set; }
        public decimal TotalRealBet { get; set; }
        public decimal TotalWinLose { get; set; }
    }

    public class ProfitLostData
    {
        public string UserName { get; set; }
        public string Provider { get; set; }
        public string Type { get; set; }
        public string Game { get; set; }
        public int Count { get; set; }
        public decimal TurnOver { get; set; }
        public decimal RealBet { get; set; }
        public decimal WinLose { get; set; }
    }
}
