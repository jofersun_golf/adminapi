﻿namespace CashMarket_AdminApi.Models.Report
{
    public class ProfitLostInput : ReportInput
    {
        public string[] GroupBy { get; set; }
    }
}
