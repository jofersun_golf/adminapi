﻿namespace CashMarket_AdminApi.Models.Exception
{
    public class InvalidParameterException : System.Exception
    {
        public InvalidParameterException(string message) : base(message)
        {
            HResult = (int) CashMarket_Entities.Enums.ErrorCodes.InvalidParameter;
        }
        public InvalidParameterException(string message, System.Exception inner) : base(message, inner)
        {
            HResult = (int) CashMarket_Entities.Enums.ErrorCodes.InvalidParameter;
        }
    }
}
