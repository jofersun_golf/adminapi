﻿using CashMarket_AdminApi.Models.Base;
using CashMarket_Entities.Enums;

namespace CashMarket_AdminApi.Models.Admin
{
    public class UserListInput : ListInput
    {
        public string UserId { get; set; }
        public PersonStatus? Status { get; set; }
    }
}
