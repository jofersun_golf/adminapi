﻿using System;
using System.Collections.Generic;
using CashMarket_Entities.Models;
using CashMarket_Entities.Models.Base;

namespace CashMarket_AdminApi.Models.Admin
{
    public class AdminLogOutput : BaseOutput<List<AdminLogData>>
    {
        public AdminLogOutput()
        {
            Data = new List<AdminLogData>();
        }
    }

    public class AdminLogData
    {
        public DateTime DateTime { get; set; }
        public string UserType { get; set; }
        public string Administrator { get; set; }
        public string ActionName { get; set; }
        public string TableName { get; set; }
    }
}
