﻿using System.ComponentModel.DataAnnotations;
using CashMarket_AdminApi.Models.Base;

namespace CashMarket_AdminApi.Models.Admin
{
    public class UserInput : SaveInput
    {
        public UserData User { get; set; }

        public override bool Validate() => Validate(User);
    }

    public class UserData
    {
        public int Id { get; set; }
        [RequiredAttribute]
        public string UserName { get; set; }
        /// <summary>
        /// Raw password before encrypted.
        /// </summary>
        [RequiredAttribute]
        public string PassWord { get; set; }
        [RequiredAttribute]
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get; set; }
    }
}
