﻿using System.Collections.Generic;
using CashMarket_Entities.Models;
using CashMarket_Entities.Models.Base;

namespace CashMarket_AdminApi.Models.Admin
{
    public class UserListOutput : BaseOutput<List<User>>
    {
        public UserListOutput()
        {
            Data = new List<User>();
        }
    }

    public class User
    {
        public string Name { get; set; }
        public string FullName { get; set; }
        public string Role { get; set; }
        public string Status { get; set; }
    }
}
