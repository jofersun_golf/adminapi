﻿using System.ComponentModel.DataAnnotations;
using CashMarket_AdminApi.Models.Base;

namespace CashMarket_AdminApi.Models.Admin
{
    public class StatusTagInput : SaveInput
    {
        public StatusTagData StatusTag { get; set; }
        public override bool Validate() => Validate(StatusTag);
    }

    public class StatusTagData
    {
        public int Id { get; set; }
        [RequiredAttribute]
        public string WhiteLabelCode { get; set; }
        [RequiredAttribute]
        public string ColorCode { get; set; }
        public string TagCode { get; set; }
        [RequiredAttribute]
        public string TagName { get; set; }
        public string Description { get; set; }
    }
}
