﻿using CashMarket_AdminApi.Models.Base;

namespace CashMarket_AdminApi.Models.Admin
{
    public class AdminLogInput : DateListInput
    {
        public string TableName { get; set; }
    }
}
