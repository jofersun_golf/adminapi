﻿using System.Collections.Generic;
using CashMarket_Entities.Models;
using CashMarket_Entities.Models.Base;

namespace CashMarket_AdminApi.Models.Admin
{
    public class StatusTagOutput : BaseOutput<List<StatusTagData>>
    {
        public StatusTagOutput()
        {
            Data = new List<StatusTagData>();
        }
    }
}
