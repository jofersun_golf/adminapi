﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using CashMarket_AdminApi.Models.Base;
using CashMarket_Entities.Models.Members;

namespace CashMarket_AdminApi.Models.Admin
{
    public class MemberStatusTagInput : SaveInput
    {
        public MemberStatusTagData MemberStatusTag { get; set; }
        public override bool Validate()
        {
            if (!Validate(MemberStatusTag)) return false;
            var context = new cashmarket_membersContext();
            var member = context.Members.FirstOrDefault(a =>
                a.PrefixUserName.ToLower().Equals(MemberStatusTag.PrefixUserName.ToLower()) &&
                a.UserName.ToLower().Equals(MemberStatusTag.UserName.ToLower()));
            if (member == null) ErrorList.Add("Invalid data user");
            return base.Validate();
        }
    }

    public class MemberStatusTagData
    {
        public int Id { get; set; }
        [RequiredAttribute]
        public string WhiteLabelCode { get; set; }
        [RequiredAttribute]
        public string PrefixUserName { get; set; }
        [RequiredAttribute]
        public string UserName { get; set; }
        [RequiredAttribute]
        public int StatusTagId { get; set; }
    }
}
