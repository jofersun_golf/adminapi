﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using CashMarket_AdminApi.Models.Base;
using CashMarket_Entities.Enums;
using CashMarket_Entities.Models;
using CashMarket_Entities.Models.Admin;
using CashMarket_Entities.Models.Base;
using CashMarket_Entities.Models.Members;
using Microsoft.EntityFrameworkCore;

namespace CashMarket_AdminApi.Models.Transaction
{
    public class RequestTransactionInput : RequestInput
    {
        public RequestData Transaction { get; set; }

        public override bool Validate()
        {
            if (!Validate(Transaction)) return false;
            if (Transaction.RequestType == Request.Withdraw ||
                Transaction.RequestType == Request.Subs)
            {
                var membersContext = new cashmarket_membersContext();
                var wallet = membersContext.MemberWallets.FirstOrDefault(a =>
                    a.Member.WhiteLabelCode.ToLower().Equals(Transaction.WhiteLabelCode.ToLower()) &&
                    a.Member.UserName.ToLower().Equals(Transaction.UserName.ToLower()));
                if (wallet == null) ErrorList.Add(Response.GetDescription(ErrorCodes.InvalidWlCodeOrUserName));
                else if (wallet.Balance - Transaction.Amount < 0) ErrorList.Add(Response.GetDescription(ErrorCodes.InsufficientFund));
            }
            ErrorMessage = string.Join("; ", ErrorList);
            return !ErrorList.Any();
        }

        public bool Validate(BaseOutput output)
        {
            var membersContext = new cashmarket_membersContext();
            var wallet = membersContext.MemberWallets.Include(a=>a.Member).FirstOrDefault(a =>
                a.Member.WhiteLabelCode.ToLower().Equals(Transaction.WhiteLabelCode.ToLower()) &&
                a.Member.UserName.ToLower().Equals(Transaction.UserName.ToLower()));
            if (wallet == null)
            {
                output.Response.ErrorCode = ErrorCodes.InvalidWlCodeOrUserName;
                return false;
            }

            switch (Transaction.RequestType)
            {
                case Request.Withdraw:
                case Request.Subs:
                {
                    var banks = MemberBank.GetMemberBanks(wallet.Member.Id);
                    if (!banks.Any(a =>
                        a.BankName.Equals(Transaction.BankName, StringComparison.CurrentCultureIgnoreCase) &&
                        a.AccountNumber.Equals(Transaction.RecipientAccountNumber) &&
                        a.AccountName.Equals(Transaction.RecipientAccountHolder)))
                    {
                        output.Response.ErrorCode = ErrorCodes.InvalidData;
                        output.Response.ErrorMessage = "Invalid data recipient";
                        return false;
                    }
                    if (wallet.Balance - Transaction.Amount < 0)
                    {
                        output.Response.ErrorCode = ErrorCodes.InsufficientFund;
                        return false;
                    }
                    break;
                }
                case Request.Deposit:
                case Request.Addition:
                {
                    var adminContext = new cashmarket_adminContext();
                    var wlBank = adminContext.WhiteLabelBanks.FirstOrDefault(a =>
                        a.BankAccountName.ToLower().Equals(Transaction.RecipientAccountHolder.ToLower()) &&
                        a.BankNumber.ToLower().Equals(Transaction.RecipientAccountNumber.ToLower()));
                    if (wlBank == null)
                    {
                        output.Response.ErrorCode = ErrorCodes.InvalidData;
                        output.Response.ErrorMessage = "Invalid data recipient";
                        return false;
                    }
                    break;
                }
            }
            return true;
        }
    }

    public class RequestData
    {
        [RequiredAttribute]
        public string WhiteLabelCode { get; set; }
        [RequiredAttribute]
        public string UserName { get; set; }
        public string Channel { get; set; }
        public string Reference { get; set; }
        [RequiredAttribute]
        public decimal Amount { get; set; }
        public string BankName { get; set; }
        [RequiredAttribute]
        public Request RequestType { get; set; }
        [RequiredAttribute]
        public string SenderAccountHolder { get; set; }
        [RequiredAttribute]
        public string SenderAccountNumber { get; set; }
        [RequiredAttribute]
        public string RecipientAccountHolder { get; set; }
        [RequiredAttribute]
        public string RecipientAccountNumber { get; set; }


    }
}
