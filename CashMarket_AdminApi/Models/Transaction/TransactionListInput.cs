﻿using CashMarket_AdminApi.Models.Base;
using CashMarket_AdminApi.Models.Member;
using CashMarket_Entities.Enums;

namespace CashMarket_AdminApi.Models.Transaction
{
    public class TransactionListInput : DateListInput
    {
        public Approval[] Approval { get; set; }
        public Request[] Request { get; set; }
        public MemberInputData[] Member { get; set; }
    }
}
