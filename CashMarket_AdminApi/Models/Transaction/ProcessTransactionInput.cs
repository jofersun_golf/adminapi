﻿using System.ComponentModel.DataAnnotations;
using CashMarket_AdminApi.Models.Base;
using CashMarket_Entities.Enums;

namespace CashMarket_AdminApi.Models.Transaction
{
    public class ProcessTransactionInput : RequestInput
    {
        public ProcessData Transaction { get; set; }
        public override bool Validate() => Validate(Transaction);
    }

    public class ProcessData
    {
        [RequiredAttribute]
        public long Id { get; set; }
        [RequiredAttribute]
        public string WhiteLabelCode { get; set; }
        [RequiredAttribute]
        public string UserName { get; set; }
        [RequiredAttribute]
        public decimal Amount { get; set; }
        [RequiredAttribute]
        public Approval ApprovalStatus { get; set; }
        public string Remark { get; set; }
    }
}
