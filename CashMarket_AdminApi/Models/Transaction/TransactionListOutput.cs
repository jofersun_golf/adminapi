﻿using System;
using System.Collections.Generic;
using CashMarket_AdminApi.Models.Admin;
using CashMarket_Entities.Models;

namespace CashMarket_AdminApi.Models.Transaction
{
    public class TransactionListOutput
    {
        public TransactionListOutput()
        {
            Transactions = new List<Transaction>();
        }
        public decimal TotalCredit { get; set; }
        public decimal TotalDebit { get; set; }
        public List<Transaction> Transactions { get; }
    }

    public class Transaction
    {
        public Transaction()
        {
            StatusTags = new List<StatusTagData>();
        }
        public long Id { get; set; }
        public DateTime Date { get; set; }
        public string Serial { get; set; }
        public string Member { get; set; }
        public string Name { get; set; }
        public List<StatusTagData> StatusTags { get; }
        public string Method { get; set; }
        public string Status { get; set; }
        public decimal? Credit { get; set; }
        public decimal? Debit { get; set; }
        public decimal Balance { get; set; }
        public string Processing { get; set; }

    }
}
