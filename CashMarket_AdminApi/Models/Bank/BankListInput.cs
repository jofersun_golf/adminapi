﻿using CashMarket_AdminApi.Models.Base;

namespace CashMarket_AdminApi.Models.Bank
{
    public class BankListInput : ListInput
    {
        //public short? Status { get; set; }
        public string WhiteLabelCode { get; set; }
    }
}
