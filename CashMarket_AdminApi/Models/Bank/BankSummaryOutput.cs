﻿using System.Collections.Generic;
using CashMarket_Entities.Models;
using CashMarket_Entities.Models.Base;

namespace CashMarket_AdminApi.Models.Bank
{
    public class BankSummaryOutput : BaseOutput<List<BankSummaryData>>
    {
        public BankSummaryOutput()
        {
            Data = new List<BankSummaryData>();
        }
    }

    public class BankSummaryData
    {
        public string BankCode { get; set; }
        public string BankName { get; set; }
        public string BankAccountName { get; set; }
        public string BankNumber { get; set; }
        public decimal Balance { get; set; }
        public string Status { get; set; }
    }
}
