﻿using System.ComponentModel.DataAnnotations;
using CashMarket_AdminApi.Models.Base;

namespace CashMarket_AdminApi.Models.Bank
{
    public class BankInput : SaveInput
    {
        public BankData Bank { get; set; }
        public override bool Validate() => base.Validate(Bank);
    }

    public class BankData
    {
        public int Id { get; set; }
        [RequiredAttribute]
        public string WhiteLabelCode { get; set; }
        public string BankTypeCode { get; set; }
        [RequiredAttribute]
        public string BankAccountName { get; set; }
        [RequiredAttribute]
        public string BankName { get; set; }
        [RequiredAttribute]
        public string BankNumber { get; set; }
        public string BankSerial { get; set; }
        public decimal MaxDeposit { get; set; }
        public decimal MaxWithdraw { get; set; }
        [RequiredAttribute]
        public decimal MinDeposit { get; set; }
        [RequiredAttribute]
        public decimal MinWithdraw { get; set; }
        public bool Deposit { get; set; }
        public bool Withdraw { get; set; }
        public int? SortNumber { get; set; }
        public short Status { get; set; }
    }
}
