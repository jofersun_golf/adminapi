﻿using System.Collections.Generic;
using CashMarket_Entities.Models;
using CashMarket_Entities.Models.Base;

namespace CashMarket_AdminApi.Models.Bank
{
    public class BankListOutput : BaseOutput<List<WhiteLabelBankData>>
    {
        public BankListOutput()
        {
            Data = new List<WhiteLabelBankData>();
        }
    }

    public class WhiteLabelBankData
    {
        public string BankCode { get; set; }
        public string BankName { get; set; }
        public string BankAccountName { get; set; }
        public string BankNumber { get; set; }
        public decimal MaxDeposit { get; set; }
        public decimal MaxWithdraw { get; set; }
        public decimal MinDeposit { get; set; }
        public decimal MinWithdraw { get; set; }
        public bool Deposit { get; set; }
        public bool Withdraw { get; set; }
    }
}
