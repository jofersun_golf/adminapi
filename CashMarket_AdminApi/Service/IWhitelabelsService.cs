﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CashMarket_Entities.Models.WhiteLabels;

namespace CashMarket_AdminApi.Service
{
    public interface IWhitelabelsService
    {
        WhiteLabel AddWhiteLabel(WhiteLabel whiteLabel);

        List<WhiteLabel> GetAllWhiteLabel();

        void UpdateWhiteLabel(WhiteLabel whiteLabel);

        void DeleteWhiteLabel(string wl);

        WhiteLabel GetWhiteLabel(string wl);
        List<WhiteLabelProduct> GetAllWhiteLabelProduct();
        WhiteLabelProduct GetWhiteLabelProduct(int WhiteLabelId);
        List<WhiteLabelProvider> GetAllWhiteLabelProvider();
        WhiteLabelProvider GetWhiteLabelProvider(string Brandname);
        List<WhiteLabelLanguage> GetAllWhiteLabelLanguage();
        WhiteLabelLanguage GetWhiteLabelLanguage(int WhiteLabelId);
    }
}
