﻿using CashMarket_AdminApi.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CashMarket_Entities.Models.WhiteLabels;

namespace CashMarket_AdminApi.Service
{
    public class WhitelabelsService : IWhitelabelsService
    {
        public WhitelabelsContext whitelabelsContext;
        public WhitelabelsService(WhitelabelsContext _WhitelabelsContext)
        {
            whitelabelsContext = _WhitelabelsContext;
        }
        public WhiteLabel AddWhiteLabel(WhiteLabel whiteLabel)
        {
            whitelabelsContext.Whitelabels.Add(whiteLabel);
            whitelabelsContext.SaveChanges();
            return whiteLabel;
        }

        public void DeleteWhiteLabel(string wl)
        {
            var _wl = whitelabelsContext.Whitelabels.FirstOrDefault(x => x.WhiteLabelCode == wl);
            if (_wl != null)
            {
                whitelabelsContext.Remove(_wl);
                whitelabelsContext.SaveChanges();
            }
        }
        public List<WhiteLabelLanguage> GetAllWhiteLabelLanguage()
        {
            return whitelabelsContext.WhiteLabelLanguages.ToList();
        }
        public List<WhiteLabelProvider> GetAllWhiteLabelProvider()
        {
            return whitelabelsContext.WhiteLabelProviders.ToList();
        }

        public List<WhiteLabel> GetAllWhiteLabel()
        {
            return whitelabelsContext.Whitelabels.ToList();
        }

        public List<WhiteLabelProduct> GetAllWhiteLabelProduct()
        {
            //  throw new NotImplementedException();
            return whitelabelsContext.WhiteLabelProducts.ToList();
        }

        public WhiteLabel GetWhiteLabel(string wl)
        {
            return whitelabelsContext.Whitelabels.FirstOrDefault(x => x.WhiteLabelCode == wl);
        }
        public WhiteLabelLanguage GetWhiteLabelLanguage(int WhiteLabelId)
        {
            return whitelabelsContext.WhiteLabelLanguages.FirstOrDefault(x => x.WhiteLabelId == WhiteLabelId);
        }
        public WhiteLabelProduct GetWhiteLabelProduct(int WhiteLabelId)
        {
            return whitelabelsContext.WhiteLabelProducts.FirstOrDefault(x => x.WhiteLabelId == WhiteLabelId);
        }
        public WhiteLabelProvider GetWhiteLabelProvider(string Brandname)
        {
            return whitelabelsContext.WhiteLabelProviders.FirstOrDefault(x => x.Brandname == Brandname);
        }
        public void UpdateWhiteLabel(WhiteLabel whiteLabel)
        {
            whitelabelsContext.Whitelabels.Update(whiteLabel);
            whitelabelsContext.SaveChanges();
        }
    }
}
