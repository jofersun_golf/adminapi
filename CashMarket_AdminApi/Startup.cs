using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System.Collections.Generic;
using CashMarket_Entities.Context;
using CashMarket_AdminApi.Service;
using CashMarket_AdminApi.Context;
using Microsoft.EntityFrameworkCore;
using CashMarket_Entities.Enums;
using CashMarket_Entities.Models.WhiteLabels;

namespace CashMarket_AdminApi
{
    public class Startup
    {
        List<DataBaseConnection> connections = new List<DataBaseConnection>();
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            string sqlConnectionString = Configuration["ConnectionStringWl"];
            services.AddDbContextPool<WhitelabelsContext>(options => options.UseNpgsql(sqlConnectionString));
            services.AddScoped<IWhitelabelsService, WhitelabelsService>();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "CashMarket_AdminApi", Version = "v1" });
            });

            services.AddCors(o => o.AddPolicy("MyPolicy", builder =>
            {
                builder
                    .WithMethods("PUT", "GET", "POST", "DELETE", "OPTIONS")
                    .WithHeaders("access-control-allow-headers", "access-control-allow-methods",
                        "access-control-allow-origin", "access-control-allow-credentials", "content-type")
                    .WithOrigins("*");
            }));
            services.AddCors();

            DataBaseInitialize();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "CashMarket_AdminApi v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

        }

        private void DataBaseInitialize()
        {
            var cashMarkets = Configuration.GetSection("CashMarket").Get<List<DataBaseConnection>>();
            connections.AddRange(cashMarkets);
            var providers = Configuration.GetSection("Provider").Get<List<DataBaseConnection>>();
            providers.ForEach(a=>a.IsProvider = true);
            connections.AddRange(providers);
            DbContextFactory.Initialize(connections);
        }
    }
}
